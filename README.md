# GOAL #

* Primary goal is to build repo of solved coding interview questions. So that solutions and thoughts can be put together in one place. I have used Java for solving problems. 

* Second goal is to solve same problems in other languages as well.

* Third goal is to write test cases for these problems assuming I am writing test cases for production application.