
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Stack;

/**
 * Running this program from command line:
 * Compile: javac Thumbtack.java
 * Run: java Thumbtack
 * @author arnavawasthi
 *
 */
public class InMemoryTxnDB {
	
	/*
	 * Map for keeping final cache key-value pairs. Using map for cache gives
	 * O(1) for set, get, unset operations from committed cache.
	 * 
	 * Space: O(N)
	 */
	private Map<String, String> cache;

	/*
	 * Map for keeping counts of each value in the cache. This map takes
	 * additional O(N) space. But with this map NUMEQUALTO operation takes O(1)
	 * time, when read from committed cache
	 * 
	 * Space: O(N)
	 */
	private Map<String, Integer> valuesCountMap;

	/*
	 * Map for keeping transaction data for each transaction id (txnId). It
	 * keeps old and new value for each modified key in the transaction. "null"
	 * denotes that key is not present. If a key is deleted, its new value
	 * becomes "null". Similarly when a new key is added, its old value is
	 * "null". Having both old and new values is useful, when a transaction is
	 * rollbacked. For example: In txnId = 1, A is changed from 12 to 15. In
	 * this case it will have {1: {A: [12, 15]}} Now If A is deleted, it will
	 * change to: {1: {A: [15, null]}}
	 * 
	 * Space: Per transaction it take 2M values, where M is modified keys in one
	 * transaction. Hence O(M) per transaction
	 */
	private Map<Integer, Map<String, List<String>>> txnsCache;
	
	
	/*
	 * Map for keeping transactions key-value pairs. Since it keeps key-value
	 * pairs for all transactions, it is used when commit command is fired. When
	 * a transaction is rollbacked, values in this map are also rollbacked for
	 * that transaction. This property is useful in GET operation. (commitCache
	 * + cache) together gives O(1) time for GET operation.
	 * 
	 * Space: Since it keeps changes done in all transactions, per txn space
	 * would be O(N/T). Assuming all the cache keys are modified in the union of
	 * all transactions. Given T >> N, N/T will be negligible.
	 */
	private Map<String, String> commitCache;
	
	/*
	 * Map for keeping counts for each value modified in transactions.
	 * (commitValuesCountMap + valuesCountMap) together gives O(1) time for
	 * NUMEQUALTO operation.
	 * 
	 * Space: Since it is the applies value count changes done in all
	 * transactions in this Map, per txn space would be O(N/T). Assuming all the
	 * cache keys are modified in the union of all transactions. Given T >> N,
	 * N/T will be negligible.
	 */
	private Map<String, Integer> commitValuesCountMap;
	
	/*
	 * Stack for active transactions. By using stack, I get most recent transaction in O(1) time. 
	 * All SET, UNSET, ROLLBACK uses latest from txns Stack. 
	 */
	private Stack<Integer> txns;
	
	public InMemoryTxnDB(){
		
		cache = new HashMap<String, String>();
		
		valuesCountMap = new HashMap<String, Integer>();
		
		txnsCache = new HashMap<Integer, Map<String, List<String>>>();
		
		commitCache = new HashMap<String, String>();
		
		commitValuesCountMap = new HashMap<String, Integer>();
		
		txns = new Stack<Integer>();
	}
	
	/**
	 * Sets value for the given key in either committed cache or in the
	 * transaction cache (txnsCache + commitCache), if there is an active
	 * transaction. In this operation, counts for values are also updated in
	 * respective maps. Since it does not depend of number of transactions or
	 * number of keys saved in cache, it gives O(1) run time.
	 * 
	 * @param key
	 * @param value
	 */
	public void set(String key, String value){

		//current value
		String oldValue = get(key);
		//Since new value is same as existing value no need to update.
		if(value.equals(oldValue)) return;
 		
		//if there is an active transaction, put value in the transaction cache
		if(!txns.isEmpty()){
			//Get the most recent transaction for stack
			Integer txnId = txns.peek();
			
			putInTxnCache(key, oldValue, value, txnId);
			putInCommitCache(key, oldValue, value);
			
		}else{
			//value count for the given value
			Integer newValueCount = valueCount(value);
			//current value count. We will need to decrement the current value count by one.
			Integer oldValueCount = valueCount(oldValue);
			//if there is no transaction, directly set value and value count in the cache.
			cache.put(key, value);
			valuesCountMap.put(value, newValueCount+1);
			if(oldValueCount > 0){
				valuesCountMap.put(oldValue, oldValueCount-1);
			}
		}
	}

	/**
	 * Returns value for given key either from committed cache or from the transaction cache, 
	 * if there is an active transaction. Since it does at most 2-3 HashMap lookups, it gives O(1) 
	 * run time.
	 * @param key
	 * @return value for the given key
	 */
	public String get(String key){

		//
		if(!txns.isEmpty() && commitCache.containsKey(key)){
			return commitCache.get(key);
		}
		
		//if no active txn and key is not in transactions, return value from committed cache
		return cache.get(key);
	}
	
	/**
	 * Removes value from the cache for given key. If there is an active
	 * transaction, it removes from transaction cache. Since it does not depend
	 * of number of transactions or number of keys saved in cache, it gives O(1)
	 * run time.
	 * 
	 * @param key
	 */
	public void unset(String key){
		
		String oldValue = get(key);
		//key not present in cache
		if(oldValue == null) return;
		
		//Checking if there is an active transaction
		if(!txns.isEmpty()){
			//txnId of most recent transaction
			Integer txnId = txns.peek();
			//Putting NULL in the txnCache. NULL value indicates that this key is deleted in the transaction.
			//This information is used while committing txns.
			putInTxnCache(key, oldValue, null, txnId);
			putInCommitCache(key, oldValue, null);
			return;
		}
		
		//if no transaction, directly remove key from the cache and decrement count for value
		String value = cache.get(key);
		Integer curValueCount = valueCount(value);
		cache.remove(key);
		valuesCountMap.put(value, curValueCount-1);
	}
	
	/**
	 * Returns count of given value in the cache. Since it does at most 2
	 * HashMap lookups, it gives O(1) run time.
	 * 
	 * @param value
	 * @return
	 */
	public Integer valueCount(String value){
		Integer count = null;
		//first check for count in the transaction, if there is an active txn.
		if(!txns.isEmpty()){
			count = commitValuesCountMap.get(value); 
		}
		
		//if no txn or value is null in txn (ie. it is not yet updated in txn), 
		// get it from committed cache.
		if(count == null){
			count = valuesCountMap.get(value);
		}
		
		//return 0, if count is null.
		return (count == null)?0:count;
	}
	
	
	/**
	 * Begins transaction. 
	 * 1. Creates a txnId, puts it into stack
	 * 2. Adds a blank map to txnsCache for created txnId.
	 * It does constant amount of work, giving O(1) run time.
	 */
	public void beginTransaction(){
		Integer txnId = 1;
		if(!txns.isEmpty()){
			txnId = txns.peek() + 1;
		}
		txns.push(txnId);
		
		Map<String, List<String>> txnCache = new HashMap<String, List<String>>();
		txnsCache.put(txnId, txnCache);
		
	}
	
	/**
	 * Commits all transactions into final cache. 
	 * @throws Exception If there is no active transaction
	 */
	public void commit() throws Exception{
		
		//Throw exception, if there is no transaction
		checkIfNoTxn();
		//Remove all txnIds from the stack.
		txns.clear();
		
		//commit each key-value
		for(Entry<String, String> keyValue: commitCache.entrySet()){
			String key = keyValue.getKey();
			String value = keyValue.getValue();
			
			//null value means, this key is deleted in transaction
			if(value == null){
				unset(key);
			}else{
				set(key, value);
			}
		}
		
		//Clear all transaction related data.
		txnsCache.clear();
		commitCache.clear();
		commitValuesCountMap.clear();
		
	}
	
	/**
	 * Rollback most recent transaction
	 * @throws Exception
	 */
	public void rollback() throws Exception{

		checkIfNoTxn();
			
		Integer txnId = txns.pop();
		Map<String, List<String>> txnCache = txnsCache.get(txnId);
		
		for(Entry<String, List<String>> entry: txnCache.entrySet()){
			String key = entry.getKey();
			List<String> oldNewValue = entry.getValue();
			String oldValue = oldNewValue.get(0);
			String newValue = oldNewValue.get(1);
			
			//during rollback, oldValue from transaction (txnCache) becomes newValue in commitCache
			putInCommitCache(key, newValue, oldValue);
		}
		
		txnsCache.remove(txnId);
		
		//if it was the last rollbacked transaction, clear commitCache and commitValuesCountMap. Because 
		//they may still have some keys with NULL or 0 values.
		if(txns.isEmpty()){
			commitCache.clear();
			commitValuesCountMap.clear();
		}
	
		
	}
	
	/**
	 * Helper method for validating no transaction scenario in Rollback and Commit.
	 * @throws Exception 
	 * 
	 */
	private void checkIfNoTxn() throws Exception {
		if(txns.isEmpty()){
			throw new Exception("NO TRANSACTION");
		}
		
	}
	
	/**
	 * Helper method for updating txnCache with old and new values for a given
	 * key and transaction.
	 * 
	 * @param key
	 * @param oldValue
	 * @param newValue
	 * @param txnId
	 */
	private void putInTxnCache(String key, String oldValue, String newValue,
			Integer txnId) {
		List<String> oldNewValues = new ArrayList<String>();
		oldNewValues.add(oldValue);
		oldNewValues.add(newValue);
		txnsCache.get(txnId).put(key, oldNewValues);
	}
	

	/**
	 * Helper method for updating commitCache (to be committed) with key and new
	 * value (will be null for deleted key). It also updated counts for values
	 * in the to be committed cache.
	 * 
	 * @param key
	 * @param oldValue
	 * @param newValue
	 */
	private void putInCommitCache(String key, String oldValue, String newValue) {
		commitCache.put(key, newValue);
		
		Integer oldValueCount = valueCount(oldValue);
		Integer newValueCount = valueCount(newValue);
		
		commitValuesCountMap.put(oldValue, oldValueCount-1);
		commitValuesCountMap.put(newValue, newValueCount+1);
	}
	
	
	public static void main(String[] args) {

		InMemoryTxnDB solution = new InMemoryTxnDB();
		
		Scanner scanner = new Scanner(System.in);
		
		while(scanner.hasNextLine()){
			String cmd = scanner.nextLine();
			
			if(cmd.equals("")) continue;
			
			//Split cmd by space
			String[] cmdParams = cmd.split(" ");
			
			//Begins transaction block
			if(cmdParams[0].equals("BEGIN")){
				solution.beginTransaction();
			}
			//SET block
			else if(cmdParams[0].equals("SET")){
				//validates min params for SET
				if(cmdParams.length < 3){
					System.out.println("Invalid input format! Correct Example: SET a 30");
					continue;
				}
				solution.set(cmdParams[1], cmdParams[2]);
			}
			//UNSET block
			else if(cmdParams[0].equals("UNSET")){
				//validates min params for UNSET
				if(cmdParams.length < 2){
					System.out.println("Invalid input format! Correct Example: UNSET a");
					continue;
				}
				solution.unset(cmdParams[1]);
			}
			//GET block
			else if(cmdParams[0].equals("GET")){
				//validates min params for GET
				if(cmdParams.length < 2){
					System.out.println("Invalid input format! Correct Example: GET a");
					continue;
				}
				String output = solution.get(cmdParams[1]);
				System.out.println((output == null)?"NULL":output);
			}
			//NUMEQUALTO block
			else if(cmdParams[0].equals("NUMEQUALTO")){
				//validates min params for NUMEQUALTO
				if(cmdParams.length < 2){
					System.out.println("Invalid input format! Correct Example: NUMEQUALTO 10");
					continue;
				}
				
				Integer output = solution.valueCount(cmdParams[1]);
				System.out.println((output == null)?"NULL":output);
			}
			//ROLLBACK block
			else if(cmdParams[0].equals("ROLLBACK")){
				try{
					solution.rollback();
				}
				//Exception is thrown, when there is no transaction
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
			//COMMIT block
			else if(cmdParams[0].equals("COMMIT")){
				try{
					solution.commit();
				}
				//Exception is thrown, when there is no transaction
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
			//END block
			else if(cmdParams[0].equals("END")){
				//Exits program
				System.exit(0);
			}
			//Block for command not supported.
			else{
				System.out.println("Command not supported!!");
			}
		}
		scanner.close();
		
		
	}
	
}