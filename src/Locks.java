import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/**
 * 
 */

/**
 * @author arnavawasthi
 *
 */
public class Locks {
	/*
	 * Complete the function below.
	 */

	    static int check_log_history(String[] events) {
	        Stack<Integer> locks = new Stack<Integer>();
	        Set<Integer> set = new HashSet<Integer>();
	        int len = events.length;
	        
	        int relIndex = -1;
	        for(int i = 0; i < len; i++){
	            String event = events[i];
	            String[] temp = event.split(" ");
	            int id = Integer.parseInt(temp[1]);
	            if("ACQUIRE".equals(temp[0])){
	                
	                //already acquired
	                if(set.contains(id)) return i+1;
	                locks.add(id);
	                set.add(id);
	            }else if("RELEASE".equals(temp[0])){
	                
	                
	                if(locks.pop() != id){
	                    return i+1;
	                }
	                set.remove(id);
	            }else{
	             //handling for incorrect command here.   
	            }
	        }
	        
	        if(!locks.isEmpty()){
	            return len+1;
	        }
	        
	        return 0;

	    }
}
