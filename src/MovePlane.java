/**
 * 
 */

/**
 * @author arnavawasthi
 *
 */
public class MovePlane {
    static String movePlane(String command) {

    	String incorrect = "(999, 999)";
        StringBuilder cur = new StringBuilder();
        int x = 0;
        int y = 0;
        for(int i = 0; i <= command.length(); i++){
            char c = '0';
            if( i < command.length()) c = command.charAt(i);
            boolean isInt = isInteger(cur.toString());
            
            if((cur.length() == 0 || isInt) && c == 'X') return incorrect;
            
            if(cur.length() == 0 || isInt){
                cur.append(c);
            }else{
                if(c != 'X'){
                    String temp = cur.toString();
                    int units = parseInt(temp);
                    char cmd = temp.charAt(temp.length()-1);
                    
                    switch(cmd){
                        case 'U':
                        y += units;
                        break;
                        
                        case 'D':
                        y -= units;
                        break;
                        
                        case 'L':
                        x -= units;
                        break;
                        
                        case 'R':
                        x += units;
                        break;
                        
                        default:
                        return incorrect;
                        
                    }
                    
                    
                    i--;
                    //cur.append(c);
                }
                //else{
                    //cur = new StringBuilder();
                //}
                cur = new StringBuilder();
            }
            
            
        }
        StringBuilder output = new StringBuilder();
        output.append("(").append(String.valueOf(x));
        output.append(", ").append(String.valueOf(y));
        output.append(")");
        
//        System.out.println(output);
        return output.toString();
    }

    
    static String movePlaneNupur(String command) {
		int i=0;
        int x=0;
        int y=0;
		int u=0;
		int d=0;
		int l=0;
		int r=0;
        int len=command.length();
        String incorrect="(999, 999)";
        
		
        /*
        Check each character in the string and evaluate as per the conditions
        */
		for(i=0;i<len;i++){
            if(len==0 || Character.isDigit(0)){
                return incorrect;
            }
            /*
            If the character found is a digit then continue
            */
			if(Character.isDigit(command.charAt(i))){
				//System.out.println("character at "+i +" is: "+str.charAt(i));
				continue;
			}
			else{
				if((i<len-2 && (command.charAt(i+1)=='X'|| command.charAt(i+2)=='X')) || ((i==len-1) && command.charAt(len-1)=='X')){
					continue;
				}
				else{
					int j=i;
					String strNum="";
					while(j>0 && Character.isDigit(command.charAt(--j))){
						strNum+=command.charAt(j);
					}
					
					if(strNum.equals("")){
						strNum="1";
					}
					switch(command.charAt(i)){
						case 'U':
                            y+=Integer.parseInt(strNum);
							
							break;
						case 'D':	
                            y-=Integer.parseInt(strNum);
							
							break;
						case 'L':
                            x-=Integer.parseInt(strNum);
				    		
							break;
						case 'R':	
                            x+=Integer.parseInt(strNum);
							
							break;
						case 'X':
							break;
						default:
							return incorrect;
						}
				}
			}
		}
        String resultString="("+x+", "+y+")";
        return resultString;
    }
    
    private static boolean isInteger(String str){
        try{
            Integer.parseInt(str);
            return true;
        }catch(Exception e){
            return false;
        }
        
    }

    private static int parseInt(String str){
        if(str.length() == 1) return 1;
        
        return Integer.parseInt(str.substring(0, str.length()-1));
    }
    
//    public static void main(String[] args) {
//    	System.out.println(movePlane("7U3R"));
//    	System.out.println(movePlane("3U3L3R3D"));
//    	System.out.println(movePlane("7U3DX2D"));
//		System.out.println(movePlane("4D3ULXR3L"));
//	}
    
    public static void main(String[] args) {
    	// TODO Auto-generated method stub
    	{
    		String str = "4U4D4R4L";
    		System.out.println(str);
    		System.out.println(movePlane(str));
    		System.out.println(movePlaneNupur(str));
    	}
    	{
    		String str = "UDLL";
    		System.out.println(str);
    		System.out.println(movePlane(str));
    		System.out.println(movePlaneNupur(str));
    		
    	}
    	{
    		String str = "4U4??D4R4L";
    		System.out.println(str);
    		System.out.println(movePlane(str));
    		System.out.println(movePlaneNupur(str));
    	}
    	{
    		String str = "4U4XX";
    		System.out.println(str);
    		System.out.println(movePlane(str));
    		System.out.println(movePlaneNupur(str));
    	}
    	{
    		String str = "4U4";
    		System.out.println(str);
    		System.out.println(movePlane(str));
    		System.out.println(movePlaneNupur(str));
    	}
    	{
    		String str = "4U4D4R4LX";
    		System.out.println(str);
    		System.out.println(movePlane(str));
    		System.out.println(movePlaneNupur(str));
    	}
    	
    	{
    		String str = "4UDX4DX4R4LX";
    		System.out.println(str);
    		System.out.println(movePlane(str));
    		System.out.println(movePlaneNupur(str));
    	}
    	
    	{
    		String str = "X4UDX4DX4R4LX";
    		System.out.println(str);
    		System.out.println(movePlane(str));
    		System.out.println(movePlaneNupur(str));
    	}
    	}
}
