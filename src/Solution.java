import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 * 
 * @author arnavawasthi
 *
 */
public class Solution {
	
	//Duration of simulation
	private Integer duration;
	//LearnerIds in sorted order, assuming learnerIds in input may not be in sorted order
	private int[] learnerIds;
	//Map of learnerId to Learner. 
	private Map<Integer, Learner> learners;
	//List of open submissions
	private List<Submission> openSubmissions;
	//Map of all submissions (leanerId -> list of submissions for this learner)
	private Map<Integer, List<Submission>> allSubmissions;
	
	//Constants
	private final int submissionTime = 50;
	private final int reviewTime = 20;
	private final int passGrade = 240;
	private final int resubmissionIncremental = 15;
	private final int minScore = 0;
	private final int maxScore = 100;
	private final int reviewsPerSubmission = 3;
	

	public Solution(Integer duration, List<Learner> learners){
		this.duration = duration;
		this.learners = new HashMap<Integer, Learner>();
		//putting learners in map with learnerId as key. It gives O(1) access to learner, given a learnerId
		for(Learner learner: learners){
			this.learners.put(learner.learnerId, learner);
		}
				
		//Keeping learnerIds in array and sorted with increasing learnerIds.
		//This handles scenario, when learnerIds are not in sorted order in the input
		learnerIds = new int[learners.size()];
		for(int index = 0; index < learners.size(); index++){
			learnerIds[index] = learners.get(index).learnerId;
		}
		Arrays.sort(learnerIds);
		
		//All open submissions, which are submitted and all 3 reviews are not submitted yet.
		openSubmissions = new ArrayList<Submission>();
		//All open and graded submissions
		allSubmissions = new HashMap<Integer, List<Submission>>();
		
	}
	
	public void runSimulation(){
		
		/*
		 * For each tick there are two loops. First loop take care of assignment submissions 
		 * and review submissions. Second loop takes care of starting work on assignment and 
		 * starting review work.
		 */
		for(int tick = 0; tick < duration; tick++){
			
			List<Submission> currentTickSubmissions = new ArrayList<Submission>();
			
			//Loop 1: check if any learner is ready to submit assignment or review 
			for(int learnerId: learnerIds){

				Learner learner = getLearner(learnerId);
				
				//It's the last tick of working either for submission or review
				if(tick == learner.busyUntil){
					
					//learner is working on own submission
					if(!learner.submitted){

						Submission lastSubmission = getLatestSubmission(learnerId);
						submitAssignment(tick, lastSubmission, learner);
						
						//separate list for current tick submissions. 
						//So that others don't find it for review in the current tick
						currentTickSubmissions.add(lastSubmission);
					}
					//learner is working on review
					else if(learner.reviewLearnerId != -1){
						
						submitReview(tick, learner);
					}
				}
					
				
			}
			
			//Loop 2: 
			// check if any of them can start working on failed grade assignments or review
			for(int learnerId: learnerIds){
				
				Learner learner = getLearner(learnerId);
				//Skip processing, if first submission start time is in future or if user is busy with
				//assignment or review.
				if(learner.firstSubmissionStartTime > tick || tick < learner.busyUntil) continue;
				
				//Start working on first assignment submission or if last submission has failed grade
				if((learner.firstSubmissionStartTime == tick) ||
						(learner.submitted && learner.failed)){
					
					workOnSubmission(tick, learner);
				}
				//if done with submitting own assignment, work on other's assignment review
				else{
					
					workOnReview(tick, learner);
				}
			}
			
			openSubmissions.addAll(currentTickSubmissions);
		}
		
		//print simulation results
		printSimulationResults();
		
	}
	
	/**
	 * Checks if learner is eligible to review. Then it finds which submission learner can review. 
	 * If there is an eligible submission found, learner starts reviewing it.
	 * @param tick
	 * @param learner
	 */
	private void workOnReview(int tick, Learner learner) {
		if(
				learner.reviews < reviewsPerSubmission*learner.submissions //reviews are less than 3xSubmissions
				&& learner.busyUntil <= tick //Learner is not busy working on assignment or reviewing
			){
			//finding submission to review
			for(Submission openSubmission: openSubmissions){

				if(isAvailableForReview(openSubmission, learner.learnerId)){
					
					learner.reviewLearnerId = openSubmission.learnerId;
					learner.busyUntil = tick + reviewTime;
					
					openSubmission.reviewing += 1;
					
					break;
				}
			}
			
		}
	}

	/**
	 * Checks if learner is not already busy and not waiting for review. If not, learner starts working on assignment.
	 * @param tick
	 * @param learner
	 */
	private void workOnSubmission(int tick, Learner learner) {
		
		//If learner is already busy or waiting for review on already submitted assignment, 
		//leaner can't work on the submission again.
		if(learner.busyUntil > tick || learner.waitingForReview) return;
		
		int learnerId = learner.learnerId;
		
		List<Submission> submissions = allSubmissions.get(learnerId);
		Submission lastSubmission = null;
		if(submissions != null && submissions.size() > 0){
			lastSubmission = submissions.get(submissions.size()-1);
		}else{
			submissions = new ArrayList<Submission>();
			allSubmissions.put(learnerId, submissions);
		}
		
		Submission submission = new Submission();
		submission.gradeTick = -1;
		submission.learnerId = learnerId;
		submission.sequenceNumber = (lastSubmission == null)? 0: lastSubmission.sequenceNumber+1;
		submissions.add(submission);
		
		learner.busyUntil = tick + submissionTime;
		
		//resetting fields
		learner.submitted = false;
		learner.failed = false;
	}

	/**
	 * Prints simulation results.
	 */
	private void printSimulationResults() {
		for(Integer learnerId: learnerIds){

			for(Submission submission: allSubmissions.get(learnerId)){
				if(submission.submissionTick > 0)
					submission.print();
			}
		}
	}

	/**
	 * Submit review for given reviewer at the given tick.
	 * @param tick
	 * @param reviewingLearner
	 */
	private void submitReview(int tick, Learner reviewingLearner) {
		//Find latest submission, which was being reviewed
		//Leaner.reviewLeanerId keeps the learnerId, whom recent submission is being reviewed
		Submission reviewSubmission = getLatestSubmission(reviewingLearner.reviewLearnerId);
		Learner submissionLearner = getLearner(reviewingLearner.reviewLearnerId);

		if(reviewSubmission == null) return;

		reviewSubmission.scoreSum += calculateScore(submissionLearner.submissionTrueGrade, reviewingLearner.reviewBias);
		
		reviewSubmission.reviewing -= 1;
		reviewSubmission.reviewed += 1;
		reviewSubmission.reviewedBy.add(reviewingLearner.learnerId);
		
		//If submission is reviewed by 3 learners, grade can be assigned.
		if(reviewSubmission.reviewed == reviewsPerSubmission){
			reviewSubmission.gradeTick = tick;
			
			//if submission has got failing grade, set failed = true. So that learner can work on it again.
			if(reviewSubmission.scoreSum < passGrade){
				submissionLearner.failed = true;
				submissionLearner.waitingForReview = false;
				//If failed, increment trueGrade.
				submissionLearner.submissionTrueGrade = incrementTrueGrade(submissionLearner.submissionTrueGrade);
				
			}
			//Removing already reviewed submission from open submissions
			openSubmissions.remove(reviewSubmission);
		}
		
		//Set reviewing learner fields after review submission.
		reviewingLearner.reviews += 1;
		reviewingLearner.reviewLearnerId = -1;
		reviewingLearner.busyUntil = -1;
	}

	/**
	 * @param tick
	 * @param lastSubmission
	 * @param learner
	 */
	private void submitAssignment(int tick, Submission lastSubmission,
			Learner learner) {
		lastSubmission.submissionTick = tick;
		
		learner.submitted = true;
		learner.waitingForReview = true;
		learner.submissions += 1;
		learner.busyUntil = -1;
	}

	/**
	 * @param submissionTrueGrade
	 * @return
	 */
	private int incrementTrueGrade(int submissionTrueGrade) {
		
		int newTrueGrade = submissionTrueGrade + resubmissionIncremental;
		
		if(newTrueGrade > maxScore) newTrueGrade = maxScore;
		
		return newTrueGrade;
	}

	/**
	 * @param submissionTrueGrade
	 * @param reviewBias
	 * @return
	 */
	private int calculateScore(int submissionTrueGrade, int reviewBias) {
		
		int score = submissionTrueGrade + reviewBias;
		if(score < minScore) score = minScore;
		if(score > maxScore) score = maxScore;
		
		return score;
	}


	/**
	 * @param openSubmission
	 * @return
	 */
	private boolean isAvailableForReview(Submission openSubmission, int learnerId) {
		
		if(openSubmission.learnerId != learnerId //it's not learner's own submission
			&& openSubmission.reviewed + openSubmission.reviewing < reviewsPerSubmission //already reviewed and currently reviewing count is less than 3
			&& !openSubmission.reviewedBy.contains(learnerId) //learner has not already reviewed it
				)
		{
			
			return true;
		}
		
		return false;
	}

	/**
	 * @param learnerId
	 * @return
	 */
	private Learner getLearner(int learnerId) {
		
		return learners.get(learnerId);
	}

	/**
	 * @param reviewSubmissionId
	 * @return
	 */
	private Submission getLatestSubmission(int learnerId) {
		
		List<Submission> submissions = allSubmissions.get(learnerId);
		Submission lastSubmission = null;
		if(submissions != null && submissions.size() > 0){
			lastSubmission = submissions.get(submissions.size()-1);
		}else{
			submissions = new ArrayList<Submission>();
			allSubmissions.put(learnerId, submissions);
		}
		
		return lastSubmission;
	}
	
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
	
		int duration = Integer.parseInt(scanner.nextLine());
		int learnersCount = Integer.parseInt(scanner.nextLine());
		
		List<Learner> learners = new ArrayList<Learner>();
		
		for(int i = 0; i < learnersCount; i++){
			
			String[] learnerDescStr = scanner.nextLine().split(" ");
			
			Learner learner = new Learner();
			learner.learnerId = Integer.parseInt(learnerDescStr[0]);
			learner.firstSubmissionStartTime = Integer.parseInt(learnerDescStr[1]);
			learner.submissionTrueGrade = Integer.parseInt(learnerDescStr[2]);
			learner.reviewBias = Integer.parseInt(learnerDescStr[3]);
			
			
			learners.add(learner);
			
		}
		scanner.close();
		
		Solution solution = new Solution(duration, learners);
		solution.runSimulation();
		
	}
	
}

/**
 * Learner class.
 * 
 * @author arnavawasthi
 *
 */
class Learner{
	
	int learnerId;
	int firstSubmissionStartTime;
	int submissionTrueGrade;
	int reviewBias;
	
	//flag for waiting for review after submission
	boolean waitingForReview;
	//set to true, if user has submitted assignment
	boolean submitted;
	//set to true, if last submitted assignment has got failing grade.
	boolean failed;
	//Time in ticks until this learner is busy reviewing or working on assignment
	int busyUntil = -1;
	//LearnerId whose submission is being reviewed by this learner
	int reviewLearnerId = -1;
	
	//Keeping track of count of submissions by this learner
	int submissions;
	//Keeping track of count of reviews by this learner
	int reviews;
	
}

/**
 * Submission class.
 * @author arnavawasthi
 *
 */
class Submission{
	
	int learnerId;
	int sequenceNumber;
	int submissionTick;
	int scoreSum;
	int gradeTick;
	
	//Keeping track of count reviews
	int reviewed;
	//Keeping track of reviews in progress
	int reviewing;
	//Submission needs to keep track of, who has already reviewed it. So that no learner would review the same
	// submission again.
	Set<Integer> reviewedBy = new HashSet<Integer>();
	
	public void print() {
		System.out.println(learnerId+" "+sequenceNumber+" "+submissionTick+" "+scoreSum+" "+gradeTick);
	}

	
}