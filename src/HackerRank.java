import java.util.Arrays;

/**
 * 
 */

/**
 * Given an int array and a number (K). Find count of subarrays, for which product of subarray elements is less than K.
 * subarray: Contiguous subset of array elements.
 * @author arnavawasthi
 *
 */
public class HackerRank {
	
	/*
	 * Complete the function below.
	 */

	    static long count(int[] numbers, int k) {
	        int len = numbers.length;
	        //array for keeping 
	        long[] temp = new long[len];
	        
	        long output = 0;
	        for(int i = 1; i <= len; i++){
	            for(int j=0; j < len; j++){
	                int index = j + i - 1;
	                if(index >= len) break;
	                //if multiplication from previous test has exceeded, no need to 
	                //further test for it.
	                if(temp[j] >= k) continue;
	                
	                long cur = 0;
	                if(temp[j] == 0){
	                    cur = numbers[j];
	                }else{
	                    
	                    //casting int to long will ensure that it doesn't go out
	                    //of range
	                    cur = (long)temp[j]*(long)numbers[index];
	                   
	                }
	                
	                if(cur < k){
	                    output++;
	                }
	                
	                temp[j] = cur;
	            }
	            
	        }
	        
	        return output;

	    }


	
	public static void main(String[] args) {
//		{
//			int[] a = {1,2 ,3};
//			System.out.println(count(a, 4));
//			
//		}
		int i = Integer.MAX_VALUE;
		long l = Integer.MAX_VALUE*Integer.MAX_VALUE;
		long l1 = (long)i*i;
		System.out.println(l);
		System.out.println(l1);
	}

}
