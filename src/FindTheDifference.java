import java.util.HashMap;
import java.util.Map;



public class FindTheDifference {
    public char findTheDifference(String s, String t) {
    	char[] scs = s.toCharArray();
    	char[] tcs = t.toCharArray();
    	char c = tcs[0];
    	
    	Map<Character, Integer> map = new HashMap<Character, Integer>();
    	
    	for(char sc: scs){
    		int count = 1;
    		if(map.containsKey(sc)){
    			count = map.get(sc) + 1;
    		}
    		
    		map.put(sc, count);
    	}
    	
    	for(char tc: tcs){
    		if(map.containsKey(tc)){
    			int newCount = map.get(tc) - 1;
    			if(newCount == 0){
    				map.remove(tc);
    			}else{
    				map.put(tc, newCount);
    			}
    		}else{
    			c = tc;
    			break;
    		}
    	}
     	
    	
    	return c;
    }
    
    public static void main(String[] args) {
		FindTheDifference s = new FindTheDifference();
		System.out.println(s.findTheDifference("abcd", "abcde"));
	}
}