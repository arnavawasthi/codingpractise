import java.util.ArrayList;
import java.util.List;

/**
 * 
 */

/**
 * @author arnavawasthi
 *
 */
public class JustifyText {

	public static void part1(String text, int length){
		
		
		String[] words = text.split(" ");
		
		List<String> tempLine = new ArrayList<String>();
		int remLen = length;
		for(int i = 0; i < words.length; i++){
			String word = words[i];
			boolean added = false;
			if(word.length() <= remLen){
				tempLine.add(word);
				remLen = remLen - word.length();
				if(remLen > 0) remLen--;
				
				added = true;
			}
			
			if(remLen == 0 || i == words.length -1){
				StringBuilder sb = new StringBuilder();
				int j = 0;
				//TODO: adjustSpaces (can be used here as well, with spaceInc = 0 and spaceRem = 0)
				while(j < tempLine.size() - 1){
					sb.append(tempLine.get(j)).append(" ");
					j++;
				}
				sb.append(tempLine.get(j));
				System.out.println(sb);
				tempLine = new ArrayList<String>();
				remLen = length;
			}else if(remLen < word.length() && added == false){
				int spaceInc = remLen/(tempLine.size() - 1);
				int spaceRem = remLen%(tempLine.size() - 1);
				
				StringBuilder sb = new StringBuilder();
				int j = 0;
				//TODO: This can be moved to a separate function (adjustSpaces)
				while(j < tempLine.size() - 1){
					sb.append(tempLine.get(j)).append(" ");
					for(int k = 0; k < spaceInc; k++){
						sb.append(" ");
					}
					if(spaceRem > 0){
						sb.append(" ");
						spaceRem--;
					}
					j++;
				}
				sb.append(tempLine.get(j));
				System.out.println(sb);
				tempLine = new ArrayList<String>();
				remLen = length;
				
			}
			
		}
		
	}
	
	public static void part2(String text, int length){
		
		String[] words = text.split(" ");
		
		int remLen=length;
		StringBuilder sb = new StringBuilder();
		
		for(int i = 0; i < words.length; i++){
			String word = words[i];
			if(word.length() <= remLen){
				sb.append(word);
				remLen = remLen - word.length();
				if(remLen > 1){
					sb.append(" ");
					remLen--;
				}else{
					//line finished;
					System.out.println(sb);
					sb = new StringBuilder();
					remLen = length;
				}
			}else{
				int subtextLen = remLen - 1;
				String subStr = word.substring(0, subtextLen);
				sb.append(subStr).append("-");
				System.out.println(sb);
				
				sb = new StringBuilder();
				sb.append(word.substring(subtextLen)).append(" ");
				
				remLen = length - (word.length() - subtextLen) - 1;
				
			}
			
		}
		
		//last line
		System.out.println(sb);
		
		
	}
	
	public static void main(String[] args) {
		{
			String str = "Though she was able to respond, doctors had asked her to keep restraint from talking, the Congress leader said.";
			
			part1(str, 30);
			part2(str, 25);
		}
		
		{
			String str = "The innings ended in the final over of the day as Trent Boult (4) offered a simple return catch to Ashwin. This was a sixth ten-wicket haul in test cricket for the off-spinner, who finished with match figures of 13 wickets for 140 runs.";
			
			part1(str, 30);
			part2(str, 25);			
		}
	}
}
