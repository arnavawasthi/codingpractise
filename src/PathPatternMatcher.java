import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;


/**
 * TODO: Try Trie. Think about it.
 * 
 * Time complexity analysis:
 * Patterns: N, Paths: M, Avg length of pattern: j, Avg length of path: k
 * Preprocessing of Patterns (ie. sorting by length): NLog(N)
 * Preprocessing of Paths: Mk
 * Finding pattern to start the matching: 
 * * If all patterns are of same length (worst case): NM (N for each path, hence NM for M paths)
 * * There are a few patterns of any given length (Avg case): MLog(N) (Log(N) for each path.)
 * Finding matched pattern:
 * * Worst case: NMk
 * * Avg case: CMk (omiiting constant C, Mk)
 * 
 * Overall:
 * * Worst case: NLogN + Mk + NM + NMk 
 * * Avg case: NLogN + Mk + MLog(N) + Mk
 * 
 * Simplifying:
 * N == M and k is very small as compared to N and M
 * * Worst case: NLogN + N + N^2 + N^2 == N^2 (Quadratic)
 * * Avg case: NLogN + N + NLogN + N == NLogN 
 * 
 * @author arnavawasthi
 *
 */
public class PathPatternMatcher {
	
	/**
	 * This method returns best matching pattern for each path or NO MATCH, if no matching pattern is found
	 * 
	 * @param patterns: Array of patterns
	 * @param paths: Array of paths
	 */
	public void getBestMatchedPattern(String[] patterns, String[] paths){
		
		/*
		 * Preprocessing of patterns: Start
		 */
		
		List<String[]> patternTokens = new ArrayList<String[]>();
		
		for(String pattern: patterns){
			//Add tokens (fragments) of pattern as an array of string into List.
			String[] temp = pattern.split(",");
			patternTokens.add(temp);
			
		}
		
		//sorting patterns by pattern tokens (fragments) count
		sortByLength(patternTokens);
		
		//Create an array of counts of token (fragments) for each pattern from sort list of patterns.
		//This array is used to find the start index, where path-pattern compare starts.
		int[] pLen = new int[patterns.length];
		for(int i = 0; i < pLen.length; i++){
			pLen[i] = patternTokens.get(i).length;
		}
		
		/*
		 * Preprocessing of patterns: End
		 */
		
//		System.out.println(Arrays.toString(pLen));
		//Process each path from input
		for(String path: paths){
			
			/*
			 * Preprocessing of path: Start
			 */
			//Split path with path separator
			String[] pathTokens = path.split("/");
			int pathLen = 0;
			
			//count of valid path fragments.
			for(String pathToken: pathTokens){
				if(!pathToken.equals("")){
					pathLen++;
				}
			}
			
			/*
			 * Preprocessing of path: End
			 */
			
//			System.out.println(String.format("Path: %s, Len: %d", path, pathLen ));
			
			//Do binary search on patterns length array for matching path fragments count
			//If -1, no matching pattern. Otherwise start matching pattern from found startIndex.
			int startIndex = getPatternStart(pLen, pathLen);
			int matchedIndex = -1;
//			System.out.println("Start index:"+ startIndex);
			
			if(startIndex != -1){
				
				List<Integer> bestMatchStarPositions = new ArrayList<Integer>();
				List<Integer> curMatchStarPositions = null;
				
				//Match patterns until pattern fragments count is equal to path fragments count
				while(startIndex < patternTokens.size() && patternTokens.get(startIndex).length == pathLen){
					
					boolean matched = true;
//					System.out.println("Attempt: "+Arrays.toString(patternTokens.get(startIndex)));
					String[] pattern = patternTokens.get(startIndex);
					curMatchStarPositions = new ArrayList<Integer>();
					int fragmentIndex = 0;
					
					for(String pathToken: pathTokens){
						
						if(pathToken.equals("")) continue;
						
						boolean isStar = pattern[fragmentIndex].equals("*");
						//add star position to current position list
						if(isStar){
							curMatchStarPositions.add(fragmentIndex);
						}
						
//						System.out.println(String.format("PathToken: %s, Pattern: %s", pathToken, pattern[index]));
						//if current fragment doesn't match, pattern doesn't match with path
						if(!(pathToken.equals(pattern[fragmentIndex]) || isStar)){
							matched = false;
							break;
						}
						//if matches, go to next fragment
						fragmentIndex++;
					}
					
					//if matched is still true, pattern matched with path
					if(matched){
						//if this is the first match of better than best so far, update best and matchedIndex for list of patterns
						if(matchedIndex == -1 || isCurMatchBest(bestMatchStarPositions, curMatchStarPositions)){
							matchedIndex = startIndex;
							bestMatchStarPositions = curMatchStarPositions;
						}
						
					}
					
					//go to next pattern
					startIndex++;
					
				}
				
			}
			
			//finally when eligible patterns are matched with path, get the result
			String matchedPattern = getMatchedPattern(patternTokens, matchedIndex);
			
			System.out.println(matchedPattern);
		}
		
	}
	
	/**
	 * This helper method is used for comparing two results 1. so far best match, 2. current match.
	 * And returns true if current match is best, false otherwise.
	 * @param bestMatch
	 * @param curMatch
	 * @return 
	 */
	private boolean isCurMatchBest(List<Integer> bestMatch,
			List<Integer> curMatch) {
		boolean ret = false;
		
		//if number of * are higher in the best match, current will be new best
		if(bestMatch.size() > curMatch.size()){
			ret = true;
		}
		//if number of * are lower in the best match, best will remain the best
		else if(bestMatch.size() < curMatch.size()){
			ret = false;
		}
		//find out which match has * towards right
		else{
			for(int i = 0; i < bestMatch.size(); i++){
				//continue until * are at the same position in both patterns
				if(curMatch.get(i) == bestMatch.get(i)) continue;
				
				//first * starting from left for which position is different in both patterns
				
				//position is more towards right in the current match, so current match is new best.
				if(curMatch.get(i) > bestMatch.get(i)){
					ret = true;
				}
				//else best remains the best.
				else{
					ret = false;
				}
				break;
			}
			
		}
		
		return ret;
	}

	/**
	 * This method utilized binary search to find the start point for path and pattern comparison.
	 * If this method return -1, it means there is no matching pattern.
	 * @param pLen
	 * @param pathLen
	 * @return
	 */
	private int getPatternStart(int[] pLen, int pathLen) {
		return getPatternStartHelper(pLen, pathLen, 0, pLen.length-1);
	}

	/**
	 * This is a simple recursive binary search helper.
	 * 
	 * @param pLen
	 * @param pathLen
	 * @param i
	 * @param length
	 * @return
	 */
	private int getPatternStartHelper(int[] pLen, int pathLen, int low, int high) {
		if(low > high) return -1;
		
		int mid = low + (high - low)/2;
		
		//Same length found
		if(pLen[mid] == pathLen){
			//Move to the first matching length
			while(mid >= 0 && pLen[mid] == pathLen) mid--;
			return mid+1;
		}else if(pLen[mid] > pathLen){
			//recursively search in lower half
			return getPatternStartHelper(pLen, pathLen, low, mid-1);
		}else{
			//recursively search in the higher half
			return getPatternStartHelper(pLen, pathLen, mid+1, high);
		}
	}

	/**
	 * This method takes List of String arrays and sorts the List based on String array sizes. 
	 * This is used to sort patterns based on tokens length (Note: Not just by pattern.length).
	 * Once patterns are sorted, it becomes easy to figure out which patterns must be compared
	 * with given path.
	 * @param listStrArr
	 */
	private void sortByLength(List<String[]> listStrArr){
		
		Collections.sort(listStrArr, new Comparator<String[]>() {
			@Override
			//Sort pattern by number of tokens (fragments)
			public int compare(String[] str1, String[] str2) {
				return str1.length - str2.length;
			}
		});
		
	}
	
	
	/**
	 * Helper method to get the matching pattern or NO MATCH.
	 * 
	 * @param patternTokens
	 * @param matchedIndex
	 */
	private String getMatchedPattern(List<String[]> patterns,
			int matchedIndex) {
		
		//Matching pattern is found
		if(matchedIndex != -1){
			String[] pattern = patterns.get(matchedIndex);
			int i = 0;
			//print in the same line one fragment of pattern and one comma
			StringBuilder builder = new StringBuilder();
			for( ;i < pattern.length-1; i++){
//				System.out.print(pattern[i]);
//				System.out.print(",");
				builder.append(pattern[i]).append(",");
			}
			//print last fragment and next line
//			System.out.println(pattern[i]);
			builder.append(pattern[i]);
			
			return builder.toString();
		}
		//No match
		else{
			return "NO MATCH";
		}
		
	}
	
	public static void main(String[] args) {
		
		
//		String[] patterns = {"a", "a,b,*", "b,*,e", "c,*,*", "a,*,*", "a,b,c", "a,*,c", "vd", "bd,es,*,cs"};
//		String[] paths = {"/a/b/c", "a", "b/aa/e", "b/aa/c", "/a/bds/c/", "/a/b/d/s/c/", "a/b", "aaa/bbb","/a/b/c/", "/a//b//c/", "/bd/es/c/cs"};
		
		Scanner scanner = new Scanner(System.in);
		
		try{
			int patternsCount = Integer.parseInt(scanner.nextLine().trim());
			String[] patterns = new String[patternsCount];
			
			for(int i = 0; i < patternsCount; i++){
				patterns[i] = scanner.nextLine().trim();
			}
			
			int pathsCount = Integer.parseInt(scanner.nextLine().trim());
			String[] paths = new String[pathsCount];
			
			for(int i = 0; i < pathsCount; i++){
				paths[i] = scanner.nextLine().trim();
			}
			
			PathPatternMatcher ppm = new PathPatternMatcher();
			ppm.getBestMatchedPattern(patterns, paths);
			
		}catch(Exception e){
			System.out.println("Corrupted input!!");
		}
		
		scanner.close();
		
		
		
	}

}
