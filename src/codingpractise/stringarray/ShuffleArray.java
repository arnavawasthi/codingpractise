/**
 * 
 */
package codingpractise.stringarray;

import java.util.Arrays;
import java.util.Random;

/**
 * https://www.careercup.com/question?id=6211592599371776
 * @author arnavawasthi
 *
 */
public class ShuffleArray {

	public static void solution(int a[]){
	
		Random r = new Random();
		for(int i = 0; i < a.length; i++){
			int s = r.nextInt(a.length);
			
			int temp = a[i];
			a[i] = a[s];
			a[s] = temp;
		}
		
	}
	
	public static void main(String[] args) {
		int a[] = {1, 2, 3, 4, 5, 6, 7};
		solution(a);
		System.out.println(Arrays.toString(a));
	}
}
