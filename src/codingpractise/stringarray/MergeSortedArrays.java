/**
 * 
 */
package codingpractise.stringarray;

import java.util.Arrays;

/**
 * http://www.programcreek.com/2012/12/leetcode-merge-sorted-array-java/
 * @author arnavawasthi
 *
 */
public class MergeSortedArrays {
	
	
	/**
	 * 
	 * @param a Array with empty spaces in the end
	 * @param b Another array
	 * @param k Number of elements filled in a[]
	 */
	public static void merge(int[] a, int[] b, int k){
		int aend = k - 1;
		int bend = b.length - 1;
		int fill = a.length-1;
				
		while(aend >=0 && bend >= 0){
			if(a[aend] > b[bend]){
				a[fill] = a[aend];
				aend--;
			}else{
				a[fill] = b[bend];
				bend--;
			}
			fill--;
		}
		
		/**
		 * This part copy remaining b[] elements, if any, to a[]
		 */
		while(bend >= 0){
			a[fill] = b[bend];
			bend--;
			fill--;
		}
	}
	
	/**
	 * Much smaller solution. In this we loop for the length of a[]
	 * @param a
	 * @param b
	 * @param k
	 */
	public static void merge1(int[] a, int[] b, int k){
		int fill = a.length - 1;
		int aend = k - 1;
		int bend = b.length - 1;
		
		while(fill >= 0){			
			if(bend < 0 || (aend >= 0 && a[aend] > b[bend])){
				a[fill--] = a[aend--];
			}else{
				a[fill--] = b[bend--];
			}
		}
	}
	
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a = {3, 5, 7, 9, 0, 0, 0};
		int[] b = {1, 2, 4};
		
//		merge(a, b, 4);
		merge1(a, b, 4);
		System.out.println(Arrays.toString(a));

		int[] c = {1, 2, 4, 0, 0};
		int[] d = {3, 5};
		
//		merge(c, d, 3);
		merge1(c, d, 3);
		System.out.println(Arrays.toString(c));
		

	}

}
