/**
 * 
 */
package codingpractise.stringarray;

import java.util.ArrayList;
import java.util.List;

/**
 * http://www.programcreek.com/2014/07/leetcode-summary-ranges-java/
 * 
 * @author arnavawasthi
 *
 */
public class SummaryRanges {
	
	public static List<String> solution(int[] a){
		int start = a[0], prev = a[0];
		
		List<String> o = new ArrayList<String>();
		
		for(int i = 1; i < a.length; i++){
			if(a[i] - prev > 1){
				helper(o, start, prev);
				start = a[i];
				prev = a[i];
			}else{
				prev = a[i];
			}
		}
		
		helper(o, start, prev);
		
		return o;
	}
	
	private static void helper(List<String> o, int start, int prev){
		if(start == prev){
			o.add(start+"");
		}else{
			o.add(start+"->"+prev);
			
		}
	}
	
	public static void main(String[] args) {
		int a[] = {0,1,2,4,5,7};
		System.out.println(solution(a));
		
		int b[] = {0, 2,3,4,6,7, 8, 10,14,15,16};
		System.out.println(solution(b));
	}

}
