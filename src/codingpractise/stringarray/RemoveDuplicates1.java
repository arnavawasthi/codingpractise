/**
 * 
 */
package codingpractise.stringarray;

import java.util.Arrays;

/**
 * http://www.programcreek.com/2013/01/leetcode-remove-duplicates-from-sorted-array-java/
 * http://www.programcreek.com/2013/01/leetcode-remove-duplicates-from-sorted-array-ii-java/
 * @author arnavawasthi
 *
 */
public class RemoveDuplicates1 {

	public static int solution(int a[]){
		int i = 0, j = 1;
		int count = 0;
		while(i < a.length && j < a.length){
			if(a[i] == a[j]){
				j++;
				count++;
				continue;
			}else{
				i++;
				a[i] = a[j];
				j++;
			}
			
			
		}
		System.out.println("i: "+i); 
		//No need to maintain duplicates count. Because 'i' will be at the final element of modified array.
//		return a.length - count;
		return i+1;
	}
	public static void main(String[] args) {
		int a[] = {1, 1, 1, 1, 2};
		System.out.println(solution(a));
		System.out.println(Arrays.toString(a));
		
		int b[] = {1, 2, 2, 3, 4, 5, 6, 6};
		System.out.println(solution(b));
		System.out.println(Arrays.toString(b));
		
		int c[] = {1, 2, 3, 4, 5, 6};
		System.out.println(solution(c));
		System.out.println(Arrays.toString(c));
		
		
	}
}
