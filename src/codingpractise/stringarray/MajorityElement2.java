/**
 *
 */
package codingpractise.stringarray;

/**
  http://www.programcreek.com/2014/07/leetcode-majority-element-ii-java/
 * @author arnavawasthi
 *
 */
public class MajorityElement2 {

  //1,2,3,1,2,1,2,4
  public static void solution(int[] input){
    int elem1 = input[0], elem2 = input[1], count1 = 1, count2 = 1;

    for(int i = 2; i < input.length; i++){
      if(elem1 == input[i]){
        count1++;
      }else if(elem2 == input[i]){
        count2++;
      }else if(count1 == 0){
        elem1 = input[i];
        count1 = 1;
      }else if(count2 == 0){
        elem2 = input[i];
        count2 = 1;
      }else{
        count1--;
        count2--;
      }

    }
    //Right after this loop, i may get wrong indication of n/3 majority elements.
    //So I need to iterate through array once more to count so far found elements.

    count1 = 0; count2 = 0;
    for(int x: input){
      if(x == elem1){
        count1++;
      }

      if(x == elem2){
        count2++;
      }
    }
    if(count1 > input.length/3){
        System.out.println("one: "+elem1);
    }
    if(count2 > input.length/3){
      System.out.println("two: "+elem2);
    }
  }

  public static void main(String[] args){
    int a[] = new int[]{1,2,3,1,2,1,2,4};
    solution(a);
  }

}
