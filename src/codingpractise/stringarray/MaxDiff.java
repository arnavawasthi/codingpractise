/**
 * 
 */
package codingpractise.stringarray;

import java.util.Random;

/**
 * https://www.interviewcake.com/question/java/stock-price
 * Maximum diff of two values in given array, such that min value is at lower index.
 * @author arnavawasthi
 *
 */
public class MaxDiff {
	
	//Time: N
    public static int solution(int[] a) {
    	System.out.println("================ solution =================");
    	
    	long start = System.currentTimeMillis();
        // write the body of your function here
        int maxDiff = Integer.MIN_VALUE; 
        int min = Integer.MAX_VALUE;
        for(int curValue: a){
            if(curValue - min > maxDiff){
             	maxDiff = curValue - min;   
            }
            if(min > curValue){
            	min = curValue;
            }
            
        }
        
        long time = System.currentTimeMillis() - start;
        System.out.println("N Time: "+time);
        return maxDiff;
    }
    
    //Time: N*N
    public static int solution1(int[] a){
    	System.out.println("================ solution1 =================");
    	long start = System.currentTimeMillis();
    	
    	int maxDiff = -1;
    	int len = a.length;
    	
    	for(int i = 0; i < len-1;i++){
    		for(int j = i+1; j < len; j++){
    			maxDiff = Math.max(maxDiff, a[j] - a[i]);
    		}
    	}
    	

        long time = System.currentTimeMillis() - start;
        System.out.println("N*N Time: "+time);
        
    	return maxDiff;
    }
    
    //Time: N, Space: N
    public static int solution2(int[] a){
    	System.out.println("================ solution2 =================");
    	long start = System.currentTimeMillis();
    	
    	int len = a.length;
    	int[] maxArray = new int[len];
    	
    	int tempMax = Integer.MIN_VALUE;
    	for(int i = len-1; i >= 0; i--){
    		tempMax = Math.max(tempMax, a[i]);
    		maxArray[i] = tempMax;
    	}
    	
    	tempMax = Integer.MIN_VALUE;
    	
    	for(int i = 0; i < len-1; i++){
    		tempMax = Math.max(tempMax, maxArray[i+1] - a[i]);
    	}
    	
        long time = System.currentTimeMillis() - start;
        System.out.println("N Time: "+time);
    	
    	return tempMax;
    	
    }
    
    
    public static void main(String[] args) {
        // run your function through some test cases here
        // remember: debugging is half the battle!
    	{
    		int[] testInput = {10, 7, 5, 2, 9};
    		System.out.println(solution(testInput));
    		System.out.println(solution1(testInput));
    		System.out.println(solution2(testInput));
    		
    	}
    	
    	{
    		int[] input = {4, 1, 3, 2};
    		System.out.println(solution(input));
    		System.out.println(solution1(input));
    		System.out.println(solution2(input));
    	}
    	{
    		
    		int[] a = new int[100000];
    		Random r = new Random();
    		for(int i = 0; i < a.length; i++){
    			a[i] = r.nextInt(a.length);
    		}
    		
    		System.out.println(solution(a));
    		System.out.println(solution1(a));
    		System.out.println(solution2(a));
    		
    	}
    	
    	{
    		
    		int[] a = new int[1000000];
    		Random r = new Random();
    		for(int i = 0; i < a.length; i++){
    			a[i] = r.nextInt(a.length);
    		}
    		
    		System.out.println(solution(a));
//    		System.out.println(solution1(a));
    		System.out.println(solution2(a));
    		
    	}

		{
			
			int[] a = new int[10000000];
			Random r = new Random();
			for(int i = 0; i < a.length; i++){
				a[i] = r.nextInt(a.length);
			}
			
			System.out.println(solution(a));
		//	System.out.println(solution1(a));
			System.out.println(solution2(a));
			
		}
		
		{
			
			int[] a = new int[100000000];
			Random r = new Random();
			for(int i = 0; i < a.length; i++){
				a[i] = r.nextInt(a.length);
			}
			
			System.out.println(solution(a));
		//	System.out.println(solution1(a));
			System.out.println(solution2(a));
			
		}		
			
    }

}
