/**
 * 
 */
package codingpractise.stringarray;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author arnavawasthi
 *
 */
public class LongestConsecutiveSequence {

	public static int solution1(int[] a){
		System.out.println("solution1");
		int max = 0;
		int steps = 0;
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		for(int i = 0; i < a.length; i++){
			map.put(a[i], 0);
			steps++;
		}
		
		for(int i = 0; i < a.length; i++){
			if(map.get(a[i]) > 0) continue;
			Integer temp = a[i] - 1;
			Integer c = 1;
			while(map.containsKey(temp)){
				steps++;
				Integer count = map.get(temp);
				if(count > 0){
					c = count+1;
					break;
				}else{
					c++;
					temp--;
				}
			}
			map.put(a[i], c);
			
			max = Math.max(max, c);
		}
		
		System.out.println(steps);
		return max;
	}

	/**
	 * This is a better solution
	 */
	public static int solution2(int[] a){
		System.out.println("solution2");
		int max = 0;
		int steps = 0;
		Set<Integer> set = new HashSet<Integer>(); 
		for(int i: a){
			steps++;
			set.add(i);
		}
		
		for(int i: a){
			
			int leftCount = 0;
			int rightCount = 0;
			int left = i-1;
			int right = i+1;
			//calculate left count
			while(set.contains(left)){
				steps++;
				leftCount++;
				set.remove(left);
				left--;
			}
			while(set.contains(right)){
				steps++;
				rightCount++;
				set.remove(right);
				right++;
			}
			max = Math.max(max, rightCount+leftCount+1);
		}
		
		System.out.println(steps);
		return max;
	}
	public static void main(String[] args) {
		int a[] = {100, 4, 200, 1, 3, 2};
		System.out.println(solution1(a));
		System.out.println(solution2(a));
	}
}
