/**
 * 
 */
package codingpractise.stringarray;

/**
 * http://www.programcreek.com/2014/06/leetcode-shortest-palindrome-java/
 * 
 * @author arnavawasthi
 *
 */

//TODO: This solution is not optimized, takes O(n^2);
public class ShortestPalindrome {

	
	public static String solution(String input){
		
		char[] chArr = input.toCharArray();
		int len = chArr.length;
		String output = input;
		for(int o = 0; o < len; o++){
			boolean palindrome = true;
			for(int i = 0, j=len-1 - o; j > i; i++, j--){
				if(chArr[i] != chArr[j]){
					palindrome = false;
					break;
				}
			}
			if(palindrome){
				System.out.println(o);
				String sb = input.substring(len-o);
				output = sb + output;
				break;
			}
			
		}
		
		return output;
	}
	
	public static void main(String[] args) {
		System.out.println(solution("aacecaaa"));
	}
}
