/**
 * 
 */
package codingpractise.stringarray;

import java.util.HashMap;
import java.util.Map;

/**
 * http://www.programcreek.com/2012/12/leetcode-solution-of-two-sum-in-java/
 * @author arnavawasthi
 *
 */
public class TwoSumNotSorted {

	public static void findTwoSum(int[] arr, int target){
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		
		for(int i = 0; i < arr.length; i++){
			map.put(arr[i], i);
		}
		
		boolean found = false;
		for(int i = 0; i < arr.length; i++){
			Integer j = map.get(target - arr[i]);
			if(j != null){
				found = true;
				System.out.println("one: "+i+", two: "+ j);
				break;
			}
		}
		
		if(!found){
			System.out.println("Not found");
		}
		
	}
	
	public static void main(String[] args) {
		int[] arr = {3, 1, 5, 2, 10, 6, 8};
		findTwoSum(arr, 12);
		findTwoSum(arr, 13);
		findTwoSum(arr, 11);
		findTwoSum(arr, 5);
		findTwoSum(arr, 19);
		
	}
}
