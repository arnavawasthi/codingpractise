/**
 * 
 */
package codingpractise.stringarray;

/**
 * http://www.programcreek.com/2015/04/leetcode-reverse-vowels-of-a-string-java/
 * @author arnavawasthi
 *
 */
public class ReverseVowels {

	
	public static String solution(String s){
		
		char[] ca = s.toCharArray();
		int i = 0, j = ca.length-1;
		
		while(i < j){
			while(!isVowel(ca[i])) i++;
			
			while(!isVowel(ca[j])) j--;
			
			//without this condition, it breaks the while loop condition and could revert one reverse.
			//TODO: Is there any way to not to duplicate while condition here?
			if(i > j) break;
			
			System.out.println(String.format("Reversing: %d %d", i, j));
			char temp = ca[i];
			ca[i] = ca[j];
			ca[j] = temp;
			
			
			i++;
			j--;
		}
		
		
		return new String(ca);
	}
	
	private static boolean isVowel(char c){
		if(c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u'){
			return true;
		}
		
		return false;
	}
	
	public static void main(String[] args) {
		System.out.println(solution("anger"));
		System.out.println(solution("dangerous"));
	}
}
