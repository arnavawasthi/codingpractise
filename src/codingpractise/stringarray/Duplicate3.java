/**
 * 
 */
package codingpractise.stringarray;

import java.util.Arrays;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * http://www.programcreek.com/2014/06/leetcode-contains-duplicate-iii-java/
 * @author arnavawasthi
 *
 */
public class Duplicate3 {

	/**
	 * No special data structure used. Time complexity: O(n*k)
	 * @param a
	 * @param t
	 * @param k
	 * @return
	 */
	public static boolean solution(int[] a, int t, int k){

		System.out.println("Solution: Diff: "+t+", Distance: "+k);
		for(int i = 0; i < a.length; i++){
			for(int j = i+1; j <= i+k && j < a.length; j++){
				if(Math.abs(a[j] - a[i]) <= t){
//					System.out.println("i: "+i+", j: "+j);
					return true;
				}
			}
			
		}
		return false;
	}
	
	/**
	 * Use sorted set (TreeSet)
	 * @param a
	 * @param t
	 * @param k
	 * @return
	 */
	public static boolean solution1(int[] a, int t, int k){
		System.out.println("Solution1: Diff: "+t+", Distance: "+k);
		
		SortedSet<Integer> set = new TreeSet<Integer>();
		for(int i = 0; i<a.length; i++){
			int cur = a[i];
			int left = cur - t;
			int right = cur + t  + 1;
			
			SortedSet<Integer> subset = set.subSet(left, right);
			
			if(subset.size() > 0){
				return true;
			}
			set.add(cur);
			
			if(i - k >= 0){
				set.remove(a[i-k]);	
			}
			
		
			
		}
		
		return false;
	}
	
	public static void main(String[] args) {
		int a[] = {2, 5, 8, 10, 7, 8, 2};
		
		System.out.println(Arrays.toString(a));
		System.out.println(solution(a, 2, 1));
		System.out.println(solution(a, 1, 2));
		System.out.println(solution(a, 3, 3));
		System.out.println(solution(a, 0, 1));
		
		System.out.println(Arrays.toString(a));
		System.out.println(solution1(a, 2, 1));
		System.out.println(solution1(a, 1, 2));
		System.out.println(solution1(a, 3, 3));
		System.out.println(solution1(a, 0, 1));
		
		int b[] = {2, 2, 2, 2, 2, 2};
		System.out.println(Arrays.toString(b));
		System.out.println(solution1(b, 2, 1));
		System.out.println(solution1(b, 1, 2));
		System.out.println(solution1(b, 3, 3));
		System.out.println(solution1(b, 0, 1));
	}
}
