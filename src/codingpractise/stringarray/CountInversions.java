/**
 * 
 */
package codingpractise.stringarray;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * @author arnavawasthi
 *
 */
public class CountInversions {

	private int[] aux;
	
	public long solution(Integer[] a){

		aux = new int[a.length];

		return halfInversions(a, 0, a.length-1);
	}
	
	public long halfInversions(Integer[] a, int i, int j){
		
		if(j-i <= 0) return 0;
		
		int m = (i + j - 1)/2;
		long r = halfInversions(a, i, m);
		long l = halfInversions(a, m+1, j);
		long s = splitInversions(a, i, m, j);
		
		return r+l+s;
	}
	
	public long splitInversions(Integer[] a, int s, int m, int e){
		int i = s, j = m+1, k = s;
		
		long inv = 0;
		while(i <= m && j <= e){
			if(a[i] > a[j]){
				//First thought is to increment the inversions count by 1. But that is incorrect.
				//Consider this: {4, 5, 6 | 1, 2, 3}. 4 > 1, it means all the remaining elements in first half are 
				//also greater than 1. So increment values becomes (last index of first half - index of 4) +1
				inv = inv + m - i + 1;
				aux[k] = a[j];
				j++;
			}else{
				aux[k] = a[i];
				i++;
			}
			k++;
		}
		
		while(i <= m){
			aux[k] = a[i];
			i++;
			k++;
		}
		while(j <= e){
			aux[k] = a[j];
			j++;
			k++;
		}
		
		for(int x=s; x<=e; x++){
			a[x] = aux[x];
		}
//		System.out.println(inv);
		return inv;
	}
	
	public static void main(String[] args) {
		Integer a[] = {6, 5, 4, 3, 2, 1};
		
		CountInversions ci = new CountInversions();
		System.out.println(ci.solution(a));
		
		System.out.println(Arrays.toString(a));
		
		Integer a1[] = {4, 5, 6, 1, 2, 3};
		System.out.println(ci.splitInversions(a1, 0, 2, 5));
		
		try {
			Scanner s = new Scanner(new File("/Users/arnavawasthi/Downloads/IntegerArray.txt"));
			List<Integer> list = new ArrayList<Integer>();
			while(s.hasNextLine()){
				list.add(Integer.parseInt(s.nextLine()));
			}
			
			s.close();
			
			CountInversions ci1 = new CountInversions();
			Integer arr[] = list.toArray(new Integer[0]);
			System.out.println(arr.length);
			System.out.println(ci1.solution(arr));
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	
}
