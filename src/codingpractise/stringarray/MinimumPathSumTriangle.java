package codingpractise.stringarray;
/**
 * 
 */

/**
 * http://www.programcreek.com/2013/01/leetcode-triangle-java/
 * @author arnavawasthi
 *
 */

//TODO: Better solution for this one?
public class MinimumPathSumTriangle {
	
	private static int min = Integer.MAX_VALUE;

	public static int solution(int[][] arr){
		
		return helper(arr, 0, 0, 0);
	}
	
	private static int helper(int[][] arr, int x, int y, int sum){
		
		if(arr.length < x + 1 || arr[x].length < y + 1){
			return sum;
		}
		
		
		int leftSum = helper(arr, x+1, y, sum + arr[x][y]);
		
		int rightSum = helper(arr, x+1, y+1, sum + arr[x][y]);
		
		System.out.println("LeftSum: "+leftSum+", RightSum: "+rightSum);
	
		int tempPathMin = Math.min(leftSum, rightSum);
		
		min = Math.min(tempPathMin, min);
		
		return min;
	}
	
	
	
	public static void main(String[] args) {
		
		int[][] arr = {{2}, {3, 4}, {6, 5, 7}, {4, 1, 8, 3}};
		System.out.println(solution(arr));
		
		int[][] arr1 = {{2}, {3, -4}, {-6, 5, 7}};
		System.out.println(solution(arr1));
		
	}
}
