/**
 * 
 */
package codingpractise.stringarray;

/**
 * @author arnavawasthi
 *
 */
public class ArrayHopsRecursive {

	public int hops(int[] a, int t){
		return hops(a, t, t);
	}
	
	private int hops(int[] a, int i, int t){
		if(a[i] == t) return 0;
		return hops(a, a[i], t) + 1;
	}
	
	public static void main(String[] args) {
		ArrayHopsRecursive ahr = new ArrayHopsRecursive();
		int[] a = {0,1,2,3};
		System.out.println(ahr.hops(a, 2));
		
		int[] b = {3,2,0,1};
		System.out.println(ahr.hops(b, 3));
	}
}
