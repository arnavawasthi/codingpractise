/**
 * 
 */
package codingpractise.stringarray;

/**
 * https://www.careercup.com/question?id=5863307617501184
 * @author arnavawasthi
 *
 */
public class Triplets {

	public static void solution(int a[]){
		int l = a.length;
		
		for(int i = 0; i < l - 2; i++){
			for(int j=i+1; j<l-1; j++){
				for(int k=j+1; k<l; k++){
					System.out.println(a[i]+","+a[j]+","+a[k]);
				}
			}
		}
		
	}
	
	public static void main(String[] args) {
		int a[] = {1,2,3,4};
		solution(a);
		
		int b[] = {1,2,3};
		solution(b);
		
		int c[] = {9,8,10,7};
		solution(c);
	}
}
