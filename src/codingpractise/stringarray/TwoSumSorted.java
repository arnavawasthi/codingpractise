/**
 * 
 */
package codingpractise.stringarray;

/**
 * http://www.programcreek.com/2014/03/two-sum-ii-input-array-is-sorted-java/
 * @author arnavawasthi
 *
 */
public class TwoSumSorted {
	
	//Use two pointers
	public static void soln1(int[] arr, int target){
		
		int i = 0;
		int j = arr.length -1 ;
		
		boolean matched = false;
		while(i<j){
			if(arr[i]+arr[j] > target){
				j--;
			}
			else if(arr[i]+arr[j] < target){
				i++;
			}
			else{
				matched = true;
//				System.out.println("Target: %1$d"+target+", one: "+i+", two: "+ j);
				System.out.println(String.format("Soln1: Target: %1$d, arr[%2$d]=%3$d + arr[%4$d]=%5$d", target, i, arr[i], j, arr[j] ));
				break;
			}
		}
		
		if(!matched){
			System.out.println("Soln1: Not matched for: "+target);
		}
		
	}
	
	//Use one pointer and do binary search on (i+1, length-1)
	public static void soln2(int[] arr, int target){
		boolean found = false;
		for(int i=0; i<arr.length; i++){
			int s = target - arr[i];
			if(s >= arr[i]){
				int j = binarySearch(arr, i+1, arr.length-1, s);
				if(j!=-1){
					System.out.println(String.format("Soln2: Target: %1$d, arr[%2$d]=%3$d + arr[%4$d]=%5$d", target, i, arr[i], j, arr[j] ));
					found = true;
					//If we don't break, it goes on to find other matches.
					break;
				}
			}else{
				break;
			}
		}
		
		if(!found){
			System.out.println("Soln2: Not matched for: "+target);
		}
	}
	
	private static int binarySearch(int[] arr, int from, int to, int value){
		
		if(from > to){
			return -1;
		}
		int mid = (from + to)/2;
		
		if(value > arr[mid]){
			return binarySearch(arr, mid+1, to, value);
		}else if(value < arr[mid]){
			return binarySearch(arr, from, mid-1, value);
		}else{
			return mid;
		}
		
	}

	
	public static void main(String[] args) {
		int[] arr = {1, 2, 3, 5, 6, 8, 10};
		
		for(int i = 0; i<25; i++){
			soln1(arr, i);
			soln2(arr, i);
		}
		
	}
}
