/**
 * 
 */
package codingpractise.stringarray;

/**
 * http://www.programcreek.com/2014/06/leetcode-search-in-rotated-sorted-array-java/
 * @author arnavawasthi
 *
 */
public class RotatedArraySearch {
	
	public static int solution(int[] a, int t){
		return helper(a, t, 0, a.length - 1);
	}
	
	private static int helper(int[] a, int t, int l, int h){
		if(l > h) return -1;
		
		int m = l + (h-l)/2;
		
		if(a[m] == t) return m;
		
		//Without equality check (skipping), it fails to find 1 in {3, 3, 1, 3, 3, 3, 3, 3}
		if(a[l] == a[h]){
			return helper(a, t, l+1, h);
		}
		//take "b" array from main as example
		else if(a[m] < a[l]){
			if(a[m] > t || (a[m] < t && a[l] <= t)){
				return helper(a, t, l, m-1);
			}else{
				return helper(a, t, m+1, h);
			}
			
		}
		//take "a" array from main as example
		else{
			if(a[m] < t || (a[m] > t && a[l] > t)){
				return helper(a, t, m+1, h);
			}else{
				return helper(a, t, l, m-1);
			}
		}
	}
	
	public static void main(String[] args) {
		
		int a[] = {3, 4, 5, 6, 7, 0, 1, 2};
		for(int i = 0; i <= 7; i++){
			System.out.println("Position of "+ i+": "+solution(a, i));
		}
		
		System.out.println("====================");
		int b[] = {7, 2, 3, 4, 5, 6};
		for(int i = 0; i <= 7; i++){
			System.out.println("Position of "+ i+": "+solution(b, i));
		}
//		
		System.out.println("====================");
		int c[] = {3, 3, 1, 3, 3, 3, 3, 3};
		for(int i = 0; i <= 7; i++){
			System.out.println("Position of "+ i+": "+solution(c, i));
		}
		
		
	}

}
