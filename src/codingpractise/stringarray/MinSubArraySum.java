/**
 * 
 */
package codingpractise.stringarray;

/**
 * http://www.programcreek.com/2014/05/leetcode-minimum-size-subarray-sum-java/
 * @author arnavawasthi
 *
 */
public class MinSubArraySum {
	
	
	/**
	 * Basic O(n^2) solution
	 * @param a
	 * @param sum
	 * @return
	 */
	public static int solution(int[] a, int sum){
		System.out.println("solution() for sum = "+sum);
		int l = a.length;
		int min = 0;
		
		
		for(int i = 0;i<l; i++){
			int ts = 0, tm = 0;
			for(int j = i; j < l; j++){
				ts += a[j];
				tm++;
				
				if(ts >= sum){
					if(min == 0 || tm < min){
						min = tm;
						System.out.println(min);
					}
					break;
				}
			}
		}
		
		return min;
	}
	
	/**
	 * 
	 * @param a
	 * @param sum
	 * @return
	 */
	public static int slidingWindow(int[] a, int sum){
		System.out.println("slidingWindow() for sum = "+sum);
		int min = 0, ts = 0, left = 0, right = 0;
		int i = 0;
		while(i < a.length){
			//TODO: ts (tempSum) can exceed "sum" as well. This usecase is not handled.
			if(ts < sum){
				ts += a[i];
				right++;
				//When adding increment i
				i++;
			}else{
				if(min == 0 || min > right - left){
					min = right - left;
					System.out.println(min);
				}
				//When subtracting, i should remain same
				ts -= a[left];
				left++;
			}
			
		}
		
		return min;
	}
	
	
	public static void main(String[] args) {
		int a[] = {2, 3, 1, 2, 4, 3};
		
		System.out.println(solution(a, 7));
		System.out.println(solution(a, 10));
		System.out.println(solution(a, 20));
		
		System.out.println(slidingWindow(a, 7));
		System.out.println(slidingWindow(a, 10));
		System.out.println(slidingWindow(a, 20));
		
	}

}
