/**
 * 
 */
package codingpractise.stringarray;

import java.util.HashMap;
import java.util.Map;

/**
 * http://www.programcreek.com/2014/05/leetcode-isomorphic-strings-java/
 * @author arnavawasthi
 *
 */
public class Isomorphic {
	
	public static boolean isIsomorphic(String s, String t){
	
		if(s == null || t == null || s.length() != t.length()){
			return false;
		}
		
		int length = s.length();
		
		Map<Character, Character> map = new HashMap<Character, Character>();
		
		for(int i = 0; i< length; i++){
			if(map.containsKey(s.charAt(i))){
				if(map.get(s.charAt(i)) != t.charAt(i)){
					return false;
				}
			}else{
				//without this condition "arnav" and "aaaaa" will be isomorphic
				//containsValue won't be O(1) operation, better will be to create a reverse Map as well.
				if(map.containsValue(t.charAt(i))){
					return false;
				}
				map.put(s.charAt(i), t.charAt(i));
			}
		}
		System.out.println(map);
		return true;
	}
	
	
	public static void main(String[] args) {
		
		System.out.println(isIsomorphic("arnav", "nupur"));
		System.out.println(isIsomorphic("arnav", "nupnr"));
		System.out.println(isIsomorphic("arnav", "kavita"));
		System.out.println(isIsomorphic("tanu", "manu"));
		System.out.println(isIsomorphic("arnav", "aaaaa"));
	}

}
