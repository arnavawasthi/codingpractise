/**
 * 
 */
package codingpractise.stringarray;

/**
 * http://www.programcreek.com/2013/01/leetcode-search-insert-position/
 * @author arnavawasthi
 *
 */
public class SearchInsertPosition {
	
	public static int solution(int[] a, int t){
		
		return binarySearch(a, 0, a.length-1, t);
		
	}
	
	/**
	 * 
	 * @param a array
	 * @param l left position (start)
	 * @param r right position (end)
	 * @param t target element
	 * @return
	 */
	
	private static int binarySearch(int[] a, int l, int r, int t){
		
		if(l > r){
			return l;
		}
		
		int mid = (l + r)/2;
		
		if(a[mid] > t){
			return binarySearch(a, l, mid - 1, t);
		}else if(a[mid] < t){
			return binarySearch(a, mid + 1, r, t);
		}else{
			return mid;
		}
		
	}
	
	public static void main(String[] args) {
//		[1,3,5,6], 5 -> 2
//		[1,3,5,6], 2 -> 1
//		[1,3,5,6], 7 -> 4
//		[1,3,5,6], 0 -> 0
		 
		int[] a = {1,3,5,6};
		
		System.out.println(solution(a, 5));
		System.out.println(solution(a, 2));
		System.out.println(solution(a, 7));
		System.out.println(solution(a, 0));
	}

}
