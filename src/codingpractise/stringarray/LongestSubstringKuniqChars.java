/**
 * 
 */
package codingpractise.stringarray;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Solution:
 * Iterate through string and maintain latest index of every char in a map (maxMap)
 * min is initialized with 0
 * at any point if adding a new char, makes maxMap size n+1. Find min value in the map.
 * Remove min entry (delete key for that value)
 * make min = min from prev step + 1
 * 
 * http://www.programcreek.com/2013/02/longest-substring-which-contains-2-unique-characters/
 * @author arnavawasthi
 *
 */
public class LongestSubstringKuniqChars {

	/**
	 * 
	 * @param s
	 * @param n
	 */
	public static void solution(String s, int n){
		Map<Character, Integer> maxMap = new HashMap<Character, Integer>();
		int min = 0;
		int max = 0;
		int finalMin = 0;
		int finalMax = 0;
		
		char[] sc = s.toCharArray();
		
		for(int i = 0; i < sc.length; i++){
			maxMap.put(sc[i], i);
			if(maxMap.size() > n){
				max = i-1;
//				System.out.println("Min: "+min+", Max: "+max);
				//This part added to keep overall max-min
				if(finalMax - finalMin < max - min){
					finalMax = max;
					finalMin = min;
				}
				min = removeMin(maxMap) + 1;
				
			}
		}
		System.out.println("Min: "+finalMin+", Max: "+finalMax);
		System.out.println("SubString: "+s.substring(finalMin, finalMax+1));
	}

	/**
	 * Need a better solution for finding min of maxMap
	 * @param maxMap
	 * @return
	 */
	private static int removeMin(Map<Character, Integer> maxMap) {
		Character mc = null;
		Integer m = Integer.MAX_VALUE;
		for(Entry<Character, Integer> entry: maxMap.entrySet()){
			if(entry.getValue() < m){
				mc = entry.getKey();
				m = entry.getValue();
			}
		}
		
		maxMap.remove(mc);
		return m;
	}
	
	public static void main(String[] args) {
		solution("abcbbbbcccbdddadacb", 2);
	}
}
