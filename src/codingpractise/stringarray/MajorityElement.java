/**
 * 
 */
package codingpractise.stringarray;

/**
 * http://www.programcreek.com/2014/02/leetcode-majority-element-java/
 * @author arnavawasthi
 *
 */
public class MajorityElement {

	/**
	 * Why this simple solution should work?
	 * Theory: Either majority element will be the first element or there will at least one instance where at least two of them will
	 * be at consecutive indices.
	 * - When odd number of elements and there is a different number between each pair. One of the majority element will be at 0th pos.
	 * - In all other cases, two or more of them will have to be on consecutive indices.
	 * @param inputArr
	 * @return
	 */
	public static int solution(int[] inputArr){
		int majorityElem = inputArr[0];
		for(int i = 0; i < inputArr.length-1; i++){
			if(inputArr[i] == inputArr[i+1]){
				majorityElem = inputArr[i];
			}
		}
		
		return majorityElem;
	}
	
	/**
	 * Liner time majority vote algorithm
	 * @param intputArr
	 * @return
	 */
	public static int solution1(int[] inputArr){
		int majorityElem = 0; 
		int counter = 0;
		
		for(int i = 0; i < inputArr.length; i++){
			if(counter == 0){
				majorityElem = inputArr[i];
				counter = 1;
			}else if(majorityElem == inputArr[i]){
				counter++;
			}else{
				counter--;
			}
		}
		
		System.out.println("Counter: "+ counter);
		
		return majorityElem;
	}
	
	public static void main(String[] args) {
		int a[] = new int[]{2,1,2,3,2,1,2};
		System.out.println(solution(a));
		System.out.println(solution1(a));
		
		int b[] = new int[]{2,2,3,2,1,2};
		System.out.println(solution(b));
		System.out.println(solution1(b));

	}
}
