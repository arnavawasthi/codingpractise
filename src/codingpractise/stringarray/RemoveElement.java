/**
 * 
 */
package codingpractise.stringarray;

import java.util.Arrays;

/**
 * http://www.programcreek.com/2014/04/leetcode-remove-element-java/
 * @author arnavawasthi
 *
 */
public class RemoveElement {
	
	/**
	 * Two pointers, one increasing and other decreasing. This uses swap, so all the elements remain in array.
	 * Element to be removed is moved to the end.
	 * @param a
	 * @param k
	 * @return
	 */
	public static int solution(int[] a, int k){
		System.out.println("Solution");
		int i = 0, j = a.length - 1;
		
		while(i < j){
			
			if(a[i] == k && a[j] != k ){
				int temp = a[i];
				a[i] = a[j];
				a[j] = temp;
				i++;
				j--;
			}else if(a[i] != k){
				i++;
			}else if(a[j] == k){
				j--;
			}
		}
		
		return i;
	}
	
	/**
	 * Two pointers, both increasing. This copies to be retained elements. Order is preserved but elements after returned length are gone.
	 * @param a
	 * @param k
	 * @return
	 */
	public static int solution1(int[] a, int k){
		System.out.println("Solution1");
		int i = 0, j = 0;
		
		while(i < a.length && j < a.length){
			if(a[j] != k){
				a[i] = a[j];
				i++;
			}
			j++;
		}
		return i;
	}
	
	public static void main(String[] args) {
		int[] a = {2, 1, 2, 3, 4, 2};
		
		System.out.println(solution(a, 2));
		System.out.println(Arrays.toString(a));
		System.out.println(solution(a, 3));
		System.out.println(Arrays.toString(a));
		
		int[] b = {2, 1, 2, 3, 4};
		
		System.out.println(solution(b, 2));
		System.out.println(Arrays.toString(b));
		
		
		int[] c = {2, 1, 2, 3, 4, 2};
		
		System.out.println(solution1(c, 2));
		System.out.println(Arrays.toString(c));
		System.out.println(solution1(c, 3));
		System.out.println(Arrays.toString(c));
		
		int[] d = {2, 1, 2, 3, 4};
		
		System.out.println(solution1(d, 2));
		System.out.println(Arrays.toString(d));
	}

}
