/**
 * 
 */
package codingpractise.stringarray;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * http://www.programcreek.com/2014/05/leetcode-contains-duplicate-java/
 * @author arnavawasthi
 *
 */
public class Duplicate1 {

	public static boolean solution1(int[] a){

		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		
		for(int i: a){
			if(map.containsKey(i)){
				return true;
			}else{
				map.put(i, 1);
			}
			
		}
		
		return false;
	}
	
	public static boolean solution2(int[] a){
		Set<Integer> set = new HashSet<Integer>();
		
		for(int i: a){
			if(!set.add(i)){
				return true;
			}
		}
		return false;
	}
	
	public static void main(String[] args) {
		
		int a[] = {2, 3, 1, 4, 5, 2, 3, 4};
		System.out.println(solution1(a));
		System.out.println(solution2(a));
	}
}
