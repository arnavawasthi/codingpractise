/**
 * 
 */
package codingpractise.stringarray;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * http://www.programcreek.com/2014/05/leetcode-minimum-window-substring-java/
 * @author arnavawasthi
 *
 */
public class MinimumWindowSubstring {
	
	/**
	 * Limitation: This solution assumes that "t" will have all unique chars.
	 * Analysis:
	 * Length of s: N
	 * Length of t: M
	 * O(M) time for remainingChars init
	 * O(M*N) for main loop to find ranges
	 * 
	 * @param s
	 * @param t
	 */
	
	public static void solution(String s, String t){
		Map<Character, Integer> positionsMap = new HashMap<Character, Integer>();
		Set<Character> remainingChars = new HashSet<Character>();
		int min = 0;
		int max = 0;
		
		//Put all target string chars into a hash set. This will maintain chars not found yet
		char[] tc = t.toCharArray();
		for(Character c: tc){
			remainingChars.add(c);
		}
		
		char[] sc = s.toCharArray();
		for(int i = 0; i < sc.length; i++){
			char c = sc[i];
			if(remainingChars.contains(c)){
				
				positionsMap.put(c, i);
				remainingChars.remove(c);
				
				if(remainingChars.isEmpty()){
					int temp = sc.length;
					for(Integer index: positionsMap.values()){
						if(index < temp){
							temp = index;
						}
					}
					min = temp;
					max = i;
					System.out.println("Positions: Min: "+ min+", Max: "+max);
					
					remainingChars.add(sc[min]);
				}
			}else if(positionsMap.containsKey(c)){
				positionsMap.put(c, i);
			}
			
		}
		
	}
	
	//TODO: Instead of HashSet, use HashMap target chars.
	public static void solution1(String s, String t){
		
	}
	
	
	public static void main(String[] args) {
		solution("ADOBECODEBANC", "ABC");
		System.out.println("Wrong answers: ");
		solution("ADOBECODEBANC", "ABBC");
	}

}
