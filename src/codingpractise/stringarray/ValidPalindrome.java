/**
 * 
 */
package codingpractise.stringarray;

/**
 * http://www.programcreek.com/2013/01/leetcode-valid-palindrome-java/
 * @author arnavawasthi
 *
 */
public class ValidPalindrome {
	
	/**
	 * Suggestion: You can also create charArray by str.toCharArray(). To check if num, you can call isNum and simply compare without
	 * calling toLowerCase on it.
	 * @param s
	 * @return
	 */
	public static boolean solution(String s){
		
		int l = s.length();
		
		for(int i = 0, j = l-1; j>=i;){
			if(isAlphaNumeric(s.charAt(i)) && isAlphaNumeric(s.charAt(j)) && Character.toLowerCase(s.charAt(i)) == Character.toLowerCase(s.charAt(j))){
				i++;
				j--;
			}else if(!isAlphaNumeric(s.charAt(i))){
				i++;
			}else if(!isAlphaNumeric(s.charAt(j))){
				j--;
			}else{
				return false;
			}
		}
		
		
		return true;
	}
	
	public static boolean isAlphaNumeric(char a){
		if(isAlpha(a) || isNum(a)){
			return true;
		}else{
			return false;
		}
	}
	
    public  static boolean isAlpha(char a){
        if((a >= 'a' && a <= 'z') || (a >= 'A' && a <= 'Z')){
            return true;
        }else{
            return false;
        }
    }
 
    public  static boolean isNum(char a){
        if(a >= '0' && a <= '9'){
            return true;
        }else{
            return false;
        }
    }
	
	public static void main(String[] args) {
		String s = "Red rum, sir, is murder";
		System.out.println(solution(s));
		
		String t = "Programcreek is awesome";
		System.out.println(solution(t));
		
//		System.out.println(Character.toLowerCase('0'));
				
				
	}

}
