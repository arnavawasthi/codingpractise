/**
 * 
 */
package codingpractise.stringarray;

import java.util.HashSet;
import java.util.Set;

/**
 * http://www.programcreek.com/2013/02/longest-substring-which-contains-2-unique-characters/
 * @author arnavawasthi
 *
 */
public class LongestSubStrN {

	/**
	 * O(n^2) time and O(n) space
	 * @param s
	 * @return
	 */
	public static String solution(String s){
		char[] c = s.toCharArray();
		int fStart = 0, fEnd = 0;
		
		for(int i = 0; i < c.length; i++){
			int start = i, end = i;
			Set<Character> set = new HashSet<Character>();
			for(int j = i; j < c.length; j++){
				set.add(c[j]);
				
				if(set.size() == 3){
//					System.out.println(set);
//					System.out.println("Start: "+start+", End: "+ end);
					if(end - start > fEnd - fStart){
						fEnd = end;
						fStart = start;
					}
					break;
				}else{
					end = j;
				}				
			}

		}
		
		
		return s.substring(fStart, fEnd+1);
	}
	
	public static void main(String[] args) {
		String s = "abcbbbbcccbdddadacb";
		System.out.println(solution(s));
		
		System.out.println(s.substring(1, 3));
	}
}
