/**
 * 
 */
package codingpractise.stringarray;

/**
 * http://www.programcreek.com/2014/02/leetcode-find-minimum-in-rotated-sorted-array/
 * http://www.programcreek.com/2014/03/leetcode-find-minimum-in-rotated-sorted-array-ii-java/
 * @author arnavawasthi
 *
 */
public class RotatedArrayMinimum {

	public static int solution(int a[]){
		//this exit case should be part of helper, because after any partition array can become sorted
//		if(a[0] < a[a.length -1]) return a[0];
		
		return helper(a, 0, a.length-1);
	}
	
	private static int helper(int a[], int l, int h){
		if(a[l] < a[h]) return a[l];
		if(h == l) return a[l];
		if(h-l == 1) return Math.min(a[l], a[h]);
		
		int m = l + (h-l)/2;
		
		//Skipping duplicates
		//Example 1: 6,6,6,6,6,6,6,6,1,6
		//Example 2: 6,6,1,6,6,6,6,6,6,6
		//Without this check, "else if" would go to right
		if(a[l] == a[h]){
			return helper(a, l+1, h);
		}
		//Equality check ensures that it goes to right
		//Example: 6,6,6,6,4,6,6
		else if(a[l] <= a[m]){
			return helper(a, m+1, h);
		}else{
			return helper(a, l, m);
		}
	}
	
	
	
	public static void main(String[] args) {
		int a[] = {3, 4, 5, 6, 7, 0, 1, 2};
		System.out.println(solution(a));
		
		int b[] = {7, 2, 3, 4, 5, 6};
		System.out.println(solution(b));
		
		int c[] = {3, 3, 1, 3, 3, 3, 3, 3};
		System.out.println(solution(c));
		
	}
}
