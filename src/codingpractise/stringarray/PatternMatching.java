/**
 * 
 */
package codingpractise.stringarray;

/**
 * http://www.programcreek.com/2014/06/leetcode-wildcard-matching-java/
 * Implement wildcard pattern matching with support for '?' and '*'
 * TODO: This is dynamic programming problem.
 * @author arnavawasthi
 *
 */
public class PatternMatching {
	
	public static boolean isMatch(String str, String pattern){
		
		
		return true;
	}
	
	public static boolean isSubstr(String str, String sub){
		
		int len = str.length();
		int slen = sub.length();
		boolean matched = true;
		for(int i=0; i< len-slen + 1; i++){
			
			matched = true;
			for(int j=0; j < slen; j++){
				
				if(str.charAt(i+j) != sub.charAt(j)){
//					System.out.println("i: "+i+", j: "+j+", str.charAt(i+j): "+str.charAt(i+j)+", sub.charAt(j): "+sub.charAt(j));
					matched = false;
					break;
				}
			}
			
			if(matched){
				break;
			}
		}
		
		return matched;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println(isSubstr("Nupur", "pur"));

	}

}
