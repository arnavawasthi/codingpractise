package codingpractise.stringarray;

//http://www.programcreek.com/2014/05/leetcode-reverse-words-in-a-string-ii-java/
//http://www.programcreek.com/2014/02/leetcode-reverse-words-in-a-string-java/
public class ReverseWords {
	
	
	public static String reverseWords(String s){
		char[] str = s.toCharArray();
		int start = 0;
		int length = str.length;
		
		for(int i = 0; i< length-1; i++){
			if(str[i+1] == ' '){
				reverse(str, start, i);
				start = i+2;
			}
		}
		
		reverse(str, start, length-1);
		
		reverse(str, 0, length-1);
		
		return String.valueOf(str);
	}
	public static void reverse(char[] str, int start, int end){
		
		while(start < end){
			char temp = str[start];
			str[start] = str[end];
			str[end] = temp;
			
			start++;
			end--;
		}
		
//		System.out.println(String.valueOf(str));
	}
	
	public static void main(String args[]){
		System.out.println(reverseWords("my name is arnav"));
	}

}
