package codingpractise.stringarray;

import java.util.Arrays;

//http://www.programcreek.com/2015/03/rotate-array-in-java/
public class RotateArray {
	
	public static void rotateArray(int[] arr, int k){
		if( k >= arr.length) return;
		
		reverseArray(arr, 0, k-1);
		printArray(arr);
		
		reverseArray(arr, k, arr.length-1);
		printArray(arr);
		
		reverseArray(arr, 0, arr.length-1);
		
	}

	public static void reverseArray(int[] arr, int left, int right){
//		int right = arr.length;
//		int left = 0;
		
		while(left < right){
			int temp = arr[left];
			arr[left] = arr[right];
			arr[right] = temp;
			
			left++;
			right--;
		}
	}
	
	public static void printArray(int[] arr){
		System.out.println(Arrays.toString(arr));
	}
	public static void main(String args[]){
		int[] arr = {1,2,3,4,5,6,7};
		int k = 3;
		rotateArray(arr, k);
		
		printArray(arr);
		
	}

}
