package codingpractise.stringarray;

/**
 * http://www.programcreek.com/2014/02/leetcode-longest-common-prefix-java/
 * @author arnavawasthi
 *
 */
public class LongestCommonPrefix{

  //This solution works, but need to improve on the time complexity.
  //TODO: Using any string data-structure (tries/suffix-tree/prefix-tree?) might help.
	
  public static String solution(String[] strings){
    int max = 0;
    String lP = "";
    for(int outerLoop = 0; outerLoop < strings.length-1; outerLoop++){
      String baseStr = strings[outerLoop];
      int baseStrLen = baseStr.length();

      for(int innerLoop = outerLoop+1; innerLoop < strings.length; innerLoop++){
        String testStr = strings[innerLoop];
        int testStrLen = testStr.length();
        int i = 0;
        for(; i < baseStrLen; i++){
          if(i >= testStrLen || baseStr.charAt(i) != testStr.charAt(i)){
            break;
          }
        }
        if(i > max){
          max = i-1;
          lP = baseStr.substring(0, max+1);
        }
      }
    }

    return lP;
  }

  public static void main(String args[]){
    String[] strings = new String[]{"flower", "flute", "fluke", "flough", "flight"};
    System.out.println(solution(strings));
  }

}
