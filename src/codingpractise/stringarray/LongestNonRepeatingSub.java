/**
 * 
 */
package codingpractise.stringarray;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * http://www.programcreek.com/2013/02/leetcode-longest-substring-without-repeating-characters-java/
 * @author arnavawasthi
 *
 */
public class LongestNonRepeatingSub {

	/**
	 * O(n) time and O(n) space
	 * @param s
	 * @return
	 */
	public static int solution1(String s){
		System.out.println("O(n) solution: "+s);
		char[] c = s.toCharArray();
		Map<Character, Integer> map = new HashMap<Character, Integer>();
		int max = 0;
		int tempMax = 0;
		for(int i = 0; i < c.length;){
			//When a duplicate element is found, we can skip iterating elements iterated so far.
			//Use map to keep index of elements. tempMax will be current index - last found index of duplicate element
			if(map.containsKey(c[i])){
				max = Math.max(max, tempMax);
				tempMax = i - map.get(c[i]) - 1;
				map.remove(c[i]);
			}else{
				map.put(c[i], i);
				tempMax++;
				i++;
			}
		}
		
		return max;
	}
	
	/**
	 * O(n^2) time and O(n) space
	 * @param s
	 * @return
	 */
	public static int solution(String s){
		System.out.println("O(n^2) solution: "+s);
		char[] c = s.toCharArray();
		int max = 0;
		
		for(int j = 0; j < c.length; j++){
			int temp = 0;
			Set<Character> set = new HashSet<Character>();
			for(int i = j; i < c.length;){
				if(set.add(c[i])){
					temp++;
					i++;
				}else{
					max = Math.max(temp, max);
					break;
				}
			}			
		}

		return max;
	}
	
	public static int wrongSolution(String s){
	
		char[] c = s.toCharArray();
		Set<Character> set = new HashSet<Character>();
		int max = 0;
		int temp = 0;
		for(int i = 0; i < c.length;){
			if(set.add(c[i])){
				temp++;
				i++;
			}else{
				max = Math.max(temp, max);
//				System.out.println(max);
				temp = 0;
				set.clear();
			}
		}
		return max;
	}
	
	public static void main(String[] args) {
		String str1 = "abcabcbb";
		String str2 = "abcadebb";
		System.out.println(solution(str1));
		System.out.println(solution1(str1));
		
		System.out.println(solution(str2));
		System.out.println(solution1(str2));
		
		
	}
}
