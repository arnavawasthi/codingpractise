/**
 * 
 */
package codingpractise.stringarray;

import java.util.Arrays;

/**
 * https://www.careercup.com/question?id=5746483519291392
 * @author arnavawasthi
 *
 */
//TODO: Do this for Linked Lists as well 
public class MergeUnionIntersection {


	public static int[] merge(int a[], int b[]){
	
		int c[] = new int[a.length+b.length];
		
		int ai = 0, bi = 0, ci = 0;
		
		while(ai < a.length && bi < b.length){
			
			if(a[ai] <= b[bi]){
				c[ci] = a[ai];
				ai++;
			}else{
				c[ci] = b[bi];
				bi++;
			}
			ci++;
		}
		
		while(ai < a.length){
			c[ci] = a[ai];
			ai++;
			ci++;
		}
		
		while(bi < b.length){
			c[ci] = b[bi];
			bi++;
			ci++;
		}
		
		return c;
	}
	
	public static int[] union(int a[], int b[]){
		
		int c[] = new int[a.length + b.length];
		int ai = 0, bi = 0, ci = 0;
		
		while(ai < a.length && bi < b.length){
			if(a[ai] < b[bi]){
				c[ci] = a[ai];
				ai++;
			}else if(a[ai] > b[bi]){
				c[ci] = b[bi];
				bi++;
			}else{
				c[ci] = a[ai];
				ai++;
				bi++;
			}
			ci++;
		}
		
		while(ai < a.length){
			c[ci] = a[ai];
			ai++;
			ci++;
		}
		
		while(bi < b.length){
			c[ci] = b[bi];
			bi++;
			ci++;
		}
		
		return c;
	}
	
	public static int[] intersection(int a[], int b[]){
		int al = a.length, bl = b.length;
		int cl = Math.min(al, bl);
		int c[] = new int[cl];
		
		int ai = 0, bi = 0, ci = 0;
		
		while(ai < al && bi < bl){
			if(a[ai] == b[bi]){
				c[ci] = a[ai];
				ai++;
				bi++;
				ci++;
			}else if(a[ai] < b[bi]){
				ai++;
			}else{
				bi++;
			}
		}
		
		return c; 
	}
	
	public static void main(String[] args) {
		int[] a = {2, 3, 5, 7, 9};
		int[] b = {1, 2, 4, 6, 7, 9};
		
		System.out.println(Arrays.toString(merge(a, b)));
		System.out.println(Arrays.toString(union(a, b)));
		System.out.println(Arrays.toString(intersection(a, b)));
	}
}
