/**
 * 
 */
package codingpractise.stringarray;

import java.util.HashMap;
import java.util.Map;

/**
 * http://www.programcreek.com/2014/05/leetcode-contains-duplicate-ii-java/
 * @author arnavawasthi
 *
 */
public class Duplicate2 {
	
	
	/**
	 * This solves the problem, if distance between indices is "k".
	 * @param a
	 * @param k
	 * @return
	 */
	public static boolean wrongSolution(int a[], int k){
		
		for(int i = 0; i < a.length - k; i++){
			if(a[i] == a[i+k]){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Uses map to reduce time complexity
	 * @param a
	 * @param k
	 * @return
	 */
	public static boolean solution(int a[], int k){
		
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		for(int i=0; i<a.length;i++){
			if(map.containsKey(a[i])){
				if(i - map.get(a[i]) <= k){
					return true;
				}
			}else{
				map.put(a[i], i);
			}
		}
		return false;
	}
	
	/**
	 * Doesn't use map. Time complexity would be O(n*k)
	 * @param a
	 * @param k
	 * @return
	 */
	public static boolean solution1(int a[], int k){
		
		for(int i = 0; i < a.length; i++){
			for(int j = i+1; j <= i + k && j < a.length; j++){
				if(a[i] == a[j]){
					return true;
				}
			}
		}
		
		return false;
	}
	
	public static void main(String[] args) {
	
		int[] a = {1, 2, 3, 2};
		System.out.println(solution(a, 1));
		System.out.println(solution(a, 2));
		System.out.println(solution(a, 3));
		System.out.println(solution(a, 4));
		System.out.println(solution(a, 5));
		
		System.out.println(solution1(a, 1));
		System.out.println(solution1(a, 2));
		System.out.println(solution1(a, 3));
		System.out.println(solution1(a, 4));
		System.out.println(solution1(a, 5));
		
	}

}
