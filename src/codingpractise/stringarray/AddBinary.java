/**
 * 
 */
package codingpractise.stringarray;

/**
 * http://www.programcreek.com/2014/05/leetcode-add-binary-java/
 * @author arnavawasthi
 *
 */
public class AddBinary {
	
	public static String solution(String s, String t){
		
		int ls = s.length() - 1;
		int lt = t.length() - 1;
		
		int c = 0;
		StringBuilder o = new StringBuilder();
		
		while(true){
			int bs = 0, bt = 0;
			if(ls >= 0){
				bs = getBit(s.charAt(ls));
				ls--;
			}
			
			if(lt >= 0){
				bt = getBit(t.charAt(lt));
				lt--;
			}
			
			int sum = bt + bs + c;
			
			int bit = (sum > 1)?sum - 2: sum;
			c = (sum > 1)?1:0;
			
			o.insert(0, bit);
			
			if(ls < 0 && lt < 0){
				o.insert(0, c);
				break;
			}
		}
		
		
		return o.toString();
	}
	
	public static int getBit(char c){
		if('0' == c){
			return 0;
		}
		
		return 1;
	}
	
	public static void main(String[] args) {
		System.out.println(solution("1011", "111"));
		System.out.println(solution("1011", "1110"));
		System.out.println(solution("1011", "10"));
	}

}
