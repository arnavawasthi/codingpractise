/**
 * 
 */
package codingpractise.stringarray;

import java.util.Arrays;

/**
 * http://www.programcreek.com/2014/05/leetcode-kth-largest-element-in-an-array-java/
 * @author arnavawasthi
 *
 */
public class QuickSelect {

	public static void main(String[] args) {
//		int[] x = { 9, 2, 4, 7, 3, 7, 10 };
		int[] x = {3, 1, 4, 1, 5, 9, 2, 6, 5, 3};
		System.out.println(kthLargest(x, 7));
	}
	
	private static int quickSelect(int[] arr, int low, int high, int k){
		System.out.println("=================");
		System.out.println(Arrays.toString(arr));
		System.out.println("Low: "+low+", High: "+high);
		
//		if(high - low == 1){
//			return arr[k];
//		}
//		if(high - low == 1){
//			int max = Math.max(arr[low], arr[high]);
//			int min = Math.min(arr[low], arr[high]);
//			
//			arr[low] = min;
//			arr[high] = max;
//			
//			return arr[k];
//		}
		
		if(low >= high){
			return arr[k];
		}
		
		int middle = (high+low)/2;
		int pivot = arr[middle];
		
		int i = low;
		int j = high;
		
		while(i < j){
			
			while(arr[i] < pivot){
				i++;
			}
			while(arr[j] > pivot){
				j--;
			}
			
			if(i < j){
				int temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
				
				i++;
				j--;
				
			}
			
		}
		
		System.out.println(Arrays.toString(arr));

		if(low == high -1){
			return arr[k];
		}
		else if(k <= j){
			return quickSelect(arr, low, j, k);
		}
		else{
			return quickSelect(arr, j+1, high, k);
		}
		
	}
	
	public static int kthLargest(int[] arr, int k){
		int low = 0;
		int high = arr.length-1;
		
		int target = arr.length - k;
		
		int kth = quickSelect(arr, low, high, target);
		
		return kth;
	}
}
