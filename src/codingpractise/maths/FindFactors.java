/**
 * 
 */
package codingpractise.maths;

import java.util.HashSet;
import java.util.Set;

/**
 * https://www.careercup.com/question?id=5759894012559360
 * @author arnavawasthi
 *
 */
public class FindFactors {
	
	public static void solution(int a){
		
	}
	
	public static void wrongSolution(int a){
		Set<Integer> set = new HashSet<Integer>();
		for(int i=1; i<=a; i++){
			int r = a%i;
			if(r == 0){
				int d = a/i;
				if(set.contains(d) || set.contains(i)){
					continue;
				}
				set.add(i);
				System.out.println(i+"*"+d);
			}
		}
	}
	
	public static void main(String[] args) {
		solution(12);
	}

}
