/**
 * 
 */
package codingpractise.maths;

import java.util.Arrays;

/**
 * https://www.careercup.com/question?id=5826758028820480
 * https://www.interviewcake.com/question/java/product-of-other-numbers
 * Division is not permitted.
 * @author arnavawasthi
 *
 */
public class SelfExcludingProduct {
    public static int[] myFunction(int[] arg) {
        // write the body of your function here
        int output[] = new int[arg.length]; 
        /*for(int i = 0; i < arg.length; i++){
            int temp  = 1;
            for(int j = 0; j < arg.length; j++){
                if(arg[i] != arg[j]){
                	temp *= arg[j];
                }
            }
            output[i] = temp;
        }*/
        int curMult = 1;
        for(int i = 0; i < arg.length; i++){
         	output[i] = curMult;
            curMult *= arg[i];
        }
        curMult = 1;
        for(int i = arg.length - 1; i >= 0; i--){
        	output[i] *= curMult;
            curMult *= arg[i];
        }
        
        return output;
    }
    public static void main(String[] args) {
        // run your function through some test cases here
        // remember: debugging is half the battle!
        int[] testInput = {1, 2, 7, 3, 4};
        System.out.println(Arrays.toString(myFunction(testInput)));
    }
}
