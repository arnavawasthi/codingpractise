/**
 * 
 */
package codingpractise.maths;

import java.util.Arrays;

/**
 * 
 * @author arnavawasthi
 *
 */
public class RobotMoves {
	
	/*
	 * Complete the function below.
	 */

	    static String[] doesCircleExists(String[] commands) {
	        String[] output = new String[commands.length];
	        for(int i = 0; i<commands.length; i++){
	            if(solution(commands[i])){
	                output[i] = "YES";
	            }else{
	                output[i] = "NO";
	            }
	        }

	        return output;
	    }


		static enum Direction {north, south, east, west};
		
		public static boolean solution(String moves) {
			System.out.println(String.format("=========%s========", moves));
			Direction facing = Direction.north;
			int[] position = { 0, 0 };
			

			String[] movesArr = moves.split("");
			int itr = 5;
			double distances[] = new double[itr];
			
			int i = 0;
			while (i < itr) {

				for (String move : movesArr) {
					if (move.equals("G")) {
						switch (facing) {
						case north:
							position[1] = position[1] + 1;
							break;
						case south:
							position[1] = position[1] - 1;
							break;
						case east:
							position[0] = position[0] + 1;
							break;
						case west:
							position[0] = position[0] - 1;
							break;
						default:
							break;
						}
					} else if (move.equals("L")) {
						switch (facing) {
						case north:
							facing = Direction.west;
							break;
						case south:
							facing = Direction.east;
							break;
						case east:
							facing = Direction.north;
							break;
						case west:
							facing = Direction.south;
							break;
						default:
							break;
						}

					} else if (move.equals("R")) {
						switch (facing) {
						case north:
							facing = Direction.east;
							break;
						case south:
							facing = Direction.west;
							break;
						case east:
							facing = Direction.south;
							break;
						case west:
							facing = Direction.north;
							break;
						default:
							break;
						}
					}

					double distance = Math.sqrt(position[0]*position[0] + position[1]*position[1]);
					if(distance == 0.0 && i > 0){
						System.out.println(String.format("Looped:: Position: %s, Distance: %f",  Arrays.toString(position), distance));
////						return true;
					}
					
					
				}
				
				distances[i] = Math.sqrt(position[0]*position[0] + position[1]*position[1]);
				System.out.println(String.format("Position: %s, Distance: %f",  Arrays.toString(position), distances[i]));
				
				i++;

			}

			for(int j  = 0; j < distances.length - 1; j++){
				if(distances[j+1] <= distances[j]) return true;
			}
			return false;
		}


	
	public static void main(String[] args) {
		System.out.println(solution("GLRG"));
		
		System.out.println(solution("GLRGR"));
		
		System.out.println(solution("GLRGRLGGGGR"));
		
		System.out.println(solution("GLRGRGLRGRGGLRGGRGGLRGGR"));
	}

}
