/**
 * 
 */
package codingpractise.graph;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * @author arnavawasthi
 *
 */
public class KargerMinCut {
	
	Random r = null;
	//Keep this value high enough to get correct MinCut value
	int iteration = 100;
	
	public int solution(){
		int min = Integer.MAX_VALUE;
		
		for(int i = 0; i < iteration; i++){
			Graph g = buildGraph();
			r = new Random(System.currentTimeMillis());
			int tempMin = findMinCut(g);
			System.out.println("TempMin: "+ tempMin+", Min: "+min);
			if(tempMin < min){
				min = tempMin;
			}
		}
		
		return min;
	}
	
	public int findMinCut(Graph g){
		if(g.vertices.size() == 2){
			return g.edges.size();
		}
		
		int edgeToRemove = r.nextInt(g.edges.size());
		g.removeEdge(g.edges.get(edgeToRemove));
//		System.out.println(g.vertices.size());
		return findMinCut(g);
		
	}
	
	private Graph buildGraph(){
		Scanner s = null;
		try {
			s = new Scanner(new File("/Users/arnavawasthi/Downloads/kargerMinCut.txt"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Graph g = new Graph();
		while(s.hasNext()){
			String line = s.nextLine();
			String[] temp = line.split("\t");
//			System.out.println(Arrays.toString(temp));
			Integer vertex = Integer.parseInt(temp[0]);
			g.addVertex(vertex);
			
			for(int i = 1; i < temp.length; i++){
				g.addEdge(vertex, Integer.parseInt(temp[i]));
			}
			
		}
		s.close();
		
//		System.out.println(g.vertices.get(1));
		
		return g;
	}
	
	public static void main(String[] args) throws FileNotFoundException {

		KargerMinCut minCut = new KargerMinCut();
		System.out.println("MinCut: "+minCut.solution());
		
	}
}
