/**
 * 
 */
package codingpractise.graph;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

/**
 * 
 Week 4:
 
 The file contains the edges of a directed graph. Vertices are labeled as
 * positive integers from 1 to 875714. Every row indicates an edge, the vertex
 * label in first column is the tail and the vertex label in second column is
 * the head (recall the graph is directed, and the edges are directed from the
 * first column vertex to the second column vertex). So for example, the 11th
 * row looks liks : "2 47646". This just means that the vertex with label 2 has
 * an outgoing edge to the vertex with label 47646
 * 
 * Your task is to code up the algorithm from the video lectures for computing
 * strongly connected components (SCCs), and to run this algorithm on the given
 * graph.
 * 
 * Output Format: You should output the sizes of the 5 largest SCCs in the given
 * graph, in decreasing order of sizes, separated by commas (avoid any spaces).
 * So if your algorithm computes the sizes of the five largest SCCs to be 500,
 * 400, 300, 200 and 100, then your answer should be "500,400,300,200,100"
 * (without the quotes). If your algorithm finds less than 5 SCCs, then write 0
 * for the remaining terms. Thus, if your algorithm computes only 3 SCCs whose
 * sizes are 400, 300, and 100, then your answer should be "400,300,100,0,0"
 * (without the quotes). (Note also that your answer should not have any spaces
 * in it.)
 * 
 * WARNING: This is the most challenging programming assignment of the course.
 * Because of the size of the graph you may have to manage memory carefully. The
 * best way to do this depends on your programming language and environment, and
 * we strongly suggest that you exchange tips for doing this on the discussion
 * forums.
 * 
 * @author arnavawasthi
 * 
 */
public class StronglyConnectedComponents {

	private Map<Integer, Boolean> visitedMap = new HashMap<Integer, Boolean>();
	private Map<Integer, Integer> priorityMap = new HashMap<Integer, Integer>();
	private Integer priority = 1, counter = 0;
	private boolean searchOn = false;
	private List<Integer> list = new ArrayList<Integer>();
	private List<Integer> adjCounts = new ArrayList<Integer>();
	
	public void solution(Graph g){
		System.out.println("Reversing Graph");
		Graph gr = reverseGraph(g);

		System.out.println("First DFS started");
		//dfs to get traversal priority map
		dfs(gr);
		
		System.out.println("Second DFS started");
		visitedMap.clear();
		searchOn = true;
		//dfs to get strongly connected components
		dfs(g);
		
	}
	
	private Graph reverseGraph(Graph g){
		
		Graph gr = new Graph();
		for(Vertex v: g.vertices.values()){
			for(Vertex adj: v.adjacents){
//				System.out.println("Reversed: Vertex: "+adj.value+", Adj: "+v.value);
				gr.addEdge(adj.value, v.value);
			}
		}
		
		return gr;
	}
	
	private void dfs(Graph g){
		if(searchOn){
			int gSize = g.vertices.size();
			for(int i = gSize; i > 0; i--){
				Vertex v = g.vertices.get(priorityMap.get(i));
//			if(visitedMap.containsKey(v.value)) continue;
				counter = 0;
//				System.out.println("Leader: "+v.value);
				dfsHelper(g, v);
				list.add(counter);
				
			}
			
		}else{
			for(Vertex v: g.vertices.values()){
				dfsHelper(g, v);
			}
		}
		
	}
	
	private void dfsHelper(Graph g, Vertex v){
		if(visitedMap.containsKey(v.value)) return;
		visitedMap.put(v.value, true);
		
		for(Vertex adj: v.adjacents){
//			System.out.println("Vertex: "+v.value+", Adj: "+adj.value);
			dfsHelper(g, adj);
		}
		counter++;
		if(!searchOn){
			priorityMap.put(priority++, v.value);
		}
		
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		System.out.println("Started ..");
		Graph g = getGraphFromFile();
//		Graph g = getTestGraph();
		StronglyConnectedComponents scc = new StronglyConnectedComponents();
		scc.solution(g);
	
		Collections.sort(scc.list, Collections.reverseOrder());
		System.out.println("Top 5:");
		System.out.println(scc.list.get(0));
		System.out.println(scc.list.get(1));
		System.out.println(scc.list.get(2));
		System.out.println(scc.list.get(3));
		System.out.println(scc.list.get(4));
		
	}
	
	private static Graph getTestGraph(){
		Graph g = new Graph();
		g.addEdge(1, 2);
		g.addEdge(2, 3);
		g.addEdge(3, 1);
		g.addEdge(3, 5);
		g.addEdge(4, 6);
		g.addEdge(5, 4);
		g.addEdge(6, 5);
		g.addEdge(8, 6);
		g.addEdge(8, 9);
		g.addEdge(9, 10);
		g.addEdge(10, 7);
		g.addEdge(7, 8);
		
		return g;
	}
	
	/*
	 * Top 5:
600498
318
287
182
178

Top 5:
434821
968
459
313
211

	 */
	private static Graph getGraphFromFile() throws FileNotFoundException{
		Scanner s = new Scanner(new File("/Users/arnavawasthi/Downloads/SCC.txt"));
		Graph g = new Graph();
		long start = System.currentTimeMillis();
		int t = 0;
		while(s.hasNext()){
			String[] temp = s.nextLine().split(" ");
			int v1 = Integer.parseInt(temp[0]);
			int v2 = Integer.parseInt(temp[1]);
			g.addEdge(v1, v2);
			
		}
		long timeTaken = System.currentTimeMillis() - start;
		System.out.println("Time in building graph(millis): "+timeTaken);
		s.close();
		
		return g;
	}
	
	
}
