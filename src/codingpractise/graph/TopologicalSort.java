/**
 * 
 */
package codingpractise.graph;

import java.util.HashMap;
import java.util.Map;

/**
 * @author arnavawasthi
 *
 */
public class TopologicalSort {

	static Map<Integer, Integer> visited = new HashMap<Integer, Integer>();
	static Map<Integer, Integer> output = new HashMap<Integer, Integer>();
	static int order;
	
	public static void solution(Graph g){
		order = g.vertices.size();
		for(Vertex v: g.vertices.values()){
			dfs(g, v);
		}
	}
	
	
	private static void dfs(Graph g, Vertex v){
		if(visited.containsKey(v.value)) return;
		visited.put(v.value, 1);
		
		for(Vertex adj: v.adjacents){
			dfs(g, adj);
		}
		
		output.put(v.value, order);
		order--;
		
	}
	
	
	public static void main(String[] args) {
		Graph g = new Graph();
		g.addEdge(1, 2);
		g.addEdge(1, 5);
		g.addEdge(2, 3);
		g.addEdge(2, 5);
//		g.addEdge(2, 1);
//		g.addEdge(3, 2);
		g.addEdge(3, 4);
//		g.addEdge(4, 3);
//		g.addEdge(4, 5);
		g.addEdge(5, 4);
//		g.addEdge(5, 2);
//		g.addEdge(5, 1);
		
		solution(g);
		System.out.println(output);
	}
}
