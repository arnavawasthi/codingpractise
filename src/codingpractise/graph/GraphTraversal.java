/**
 * 
 */
package codingpractise.graph;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;

/**
 * http://www.geeksforgeeks.org/depth-first-traversal-for-a-graph/
 * @author arnavawasthi
 *
 */
public class GraphTraversal {

	public static void bfsUndirected(Graph g, Vertex v){
		System.out.println("bfsUndirected");
		//Keeping visted nodes, so that same node is not processed more than once
		Map<Integer, Boolean> visitedMap = new HashMap<Integer, Boolean>();
		Queue<Vertex> q = new LinkedList<Vertex>();
		q.add(v);
		visitedMap.put(v.value, true);
		
		while(!q.isEmpty()){
			Vertex toProcess = q.poll();
			System.out.print(toProcess.value+ " ");
			List<Vertex> adjs = toProcess.adjacents;
			for(Vertex adj: adjs){
				if(!visitedMap.containsKey(adj.value)){
					q.add(adj);
					visitedMap.put(adj.value, true);
				}
			}
//			System.out.println(q);
		}
		
		System.out.println();
	}
	
	/*
	 * You can use Set instead of Map
	 */
	static Map<Integer, Boolean> visitedMap = new HashMap<Integer, Boolean>();
	
	public static void dfsUndirectedRecursive(Graph d, Vertex v){

		if(visitedMap.containsKey(v.value)) return;	
		visitedMap.put(v.value, true);
		System.out.print(v.value+" ");
		for(Vertex adj: v.adjacents){
			dfsUndirectedRecursive(d, adj);
		}
//		System.out.println(v.value);
	}
	

	/*
	 * It's not necessary to print (process) node, after visiting all its neighbors.
	 * 
	 */
	public static void dfsUndirectedStack(Graph d, Vertex v){
		System.out.println("dfsUndirectedStack");
		Map<Integer, Boolean> visitedMap = new HashMap<Integer, Boolean>();
		Stack<Integer> s = new Stack<Integer>();
		
//		s.push(v.value);
//		visitedMap.put(v.value, true);
//		
//		while(!s.isEmpty()){
//			Integer toProcess = s.peek();
//			System.out.print(toProcess + " ");
//			
//			List<Vertex> adjacents = d.vertices.get(toProcess).adjacents;
//			boolean processed = false;
//			for(Vertex adj: adjacents){
//				if(visitedMap.containsKey(adj.value)){
//					continue;
//				}
//				processed = true;
//				visitedMap.put(adj.value, true);
//				s.push(adj.value);
//			}
//			
//			if(!processed){
//				s.pop();
//			}
//				
//		}
		
		while(!s.isEmpty() || !visitedMap.containsKey(v.value)){
			
			if(!visitedMap.containsKey(v.value)){
				
				visitedMap.put(v.value, true);
				System.out.print(v.value + " ");
				
				for(Vertex adj: v.adjacents){
					s.add(adj.value);
				}
				
			}
			
			
			v = d.vertices.get(s.pop());
		}
		
		System.out.println();
	}
	
	
	public static void main(String[] args) {
		Graph g = new Graph();
		g.addEdge(1, 2);
		g.addEdge(1, 5);
		g.addEdge(2, 3);
		g.addEdge(2, 5);
		g.addEdge(2, 1);
		g.addEdge(3, 2);
		g.addEdge(3, 4);
		g.addEdge(4, 3);
		g.addEdge(4, 5);
		g.addEdge(5, 4);
		g.addEdge(5, 2);
		g.addEdge(5, 1);
		
//		System.out.println(g.vertices);
		
		bfsUndirected(g, g.vertices.get(1));
//		System.out.println("============");
//		bfsUndirected(g, g.vertices.get(2));
//		System.out.println("============");
//		bfsUndirected(g, g.vertices.get(3));
//		System.out.println("============");
//		bfsUndirected(g, g.vertices.get(4));
//		System.out.println("============");
//		bfsUndirected(g, g.vertices.get(5));
		
		dfsUndirectedRecursive(g, g.vertices.get(1));
		System.out.println();
		dfsUndirectedStack(g,  g.vertices.get(1));
		System.out.println();
		
	}
}
