/**
 * 
 */
package codingpractise.graph;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

/**
 * Week 5:
 * In this programming problem you'll code up Dijkstra's shortest-path
 * algorithm.
 * 
 * Download the following text file:
 * 
 * The file contains an adjacency list representation of an
 * undirected weighted graph with 200 vertices labeled 1 to 200. Each row
 * consists of the node tuples that are adjacent to that particular vertex along
 * with the length of that edge. For example, the 6th row has 6 as the first
 * entry indicating that this row corresponds to the vertex labeled 6. The next
 * entry of this row "141,8200" indicates that there is an edge between vertex 6
 * and vertex 141 that has length 8200. The rest of the pairs of this row
 * indicate the other vertices adjacent to vertex 6 and the lengths of the
 * corresponding edges.
 * 
 * Your task is to run Dijkstra's shortest-path algorithm on this graph, using 1
 * (the first vertex) as the source vertex, and to compute the shortest-path
 * distances between 1 and every other vertex of the graph. If there is no path
 * between a vertex v and vertex 1, we'll define the shortest-path distance
 * between 1 and v to be 1000000.
 * 
 * You should report the shortest-path distances to the following ten vertices,
 * in order: 7,37,59,82,99,115,133,165,188,197. You should encode the distances
 * as a comma-separated string of integers. So if you find that all ten of these
 * vertices except 115 are at distance 1000 away from vertex 1 and 115 is 2000
 * distance away, then your answer should be
 * 1000,1000,1000,1000,1000,2000,1000,1000,1000,1000. Remember the order of
 * reporting DOES MATTER, and the string should be in the same order in which
 * the above ten vertices are given. The string should not contain any spaces.
 * Please type your answer in the space provided.
 * 
 * IMPLEMENTATION NOTES: This graph is small enough that the straightforward
 * O(mn) time implementation of Dijkstra's algorithm should work fine. OPTIONAL:
 * For those of you seeking an additional challenge, try implementing the
 * heap-based version. Note this requires a heap that supports deletions, and
 * you'll probably need to maintain some kind of mapping between vertices and
 * their positions in the heap.
 * 
 * @author arnavawasthi
 * 
 */
public class DijkstraShortestPath {
	
	public Map<Vertex, Integer> minPathLength;
	public Map<Vertex, Vertex> viaVertex;
	
	public void shortestPaths(Graph g, Vertex v){
		minPathLength = new HashMap<Vertex, Integer>();
		viaVertex = new HashMap<Vertex, Vertex>();
		
		minPathLength.put(v, 0);
		HeapMap tempMap = new HeapMap(g, v);
		
		while(!tempMap.isEmpty()){
			Vertex minV = tempMap.extractMin();
//			System.out.println("Min Vertex: "+minV.value);
			for(Edge e: minV.edges){
				if(tempMap.contains(e.vertex2)){
					Integer newDistance = minPathLength.get(minV) + e.weight;
//					System.out.println("Vertex: "+e.vertex2.value+", New Distance: "+newDistance);
					if(tempMap.get(e.vertex2) > newDistance){
						tempMap.put(e.vertex2, newDistance);
						minPathLength.put(e.vertex2, newDistance);
						viaVertex.put(e.vertex2, minV);
					}
//					System.out.println(tempMap);
				}
			}
		}
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		Graph  g = new Graph();
		g.addEdge(1, 2, 3);
		g.addEdge(1, 3, 1);
		g.addEdge(1, 4, 6);
		g.addEdge(2, 1, 3);
		g.addEdge(2, 3, 1);
		g.addEdge(3, 2, 1);
		g.addEdge(3, 1, 1);
		g.addEdge(3, 4, 1);
		g.addEdge(4, 1, 6);
		g.addEdge(4, 3, 1);
		
		DijkstraShortestPath sp = new DijkstraShortestPath();
		sp.shortestPaths(g, g.vertices.get(1));
		System.out.println(sp.minPathLength);
		
		Graph  dg = new Graph();
		dg.addEdge(1, 2, 3);
		dg.addEdge(1, 4, 6);
		dg.addEdge(2, 3, 1);
		dg.addEdge(3, 1, 1);
		dg.addEdge(3, 4, 1);
		
		sp.shortestPaths(dg, dg.vertices.get(1));
		System.out.println(sp.minPathLength);
		
//		Graph tg = getGraphFromFile();
//		sp.shortestPaths(tg, tg.vertices.get(1));
//		int[] ov = {7,37,59,82,99,115,133,165,188,197};
//		Integer[] output = new Integer[ov.length];
//		StringBuilder sb = new StringBuilder();
//		for(int i = 0; i< ov.length; i++){
//			Vertex  v = tg.vertices.get(ov[i]);
//			if(sp.minPathLength.containsKey(v)){
//				output[i] = sp.minPathLength.get(v);
//			}else{
//				output[i] = 1000000;
//			}
//			sb.append(output[i]);
//			if(i != ov.length - 1){
//				sb.append(",");
//			}
//		}
//		
//		
//		System.out.println(Arrays.toString(output));
//		System.out.println(sb);
		
	}

	/**
	 * @return
	 * @throws FileNotFoundException 
	 */
	private static Graph getGraphFromFile() throws FileNotFoundException {
		Scanner s = new Scanner(new File("/Users/arnavawasthi/Downloads/dijkstraData.txt"));
		Graph g = new Graph();
		while(s.hasNextLine()){
			String line = s.nextLine();
			String[] temp = line.split("\t");
			Integer v1 = Integer.parseInt(temp[0]);
			for(int i = 1; i < temp.length; i++){
				String[] temp1 = temp[i].split(",");
				g.addEdge(v1, Integer.parseInt(temp1[0]), Integer.parseInt(temp1[1]));
			}
		}
		s.close();
		return g;
	}

}

class HeapMap{

	private Map<Vertex, Integer> map;
	
	public HeapMap(Graph g, Vertex v) {
		map = new HashMap<Vertex, Integer>();
		for(Vertex gv: g.vertices.values()){
			map.put(gv, Integer.MAX_VALUE);
		}
		map.put(v, 0);
	}
	
	public boolean isEmpty(){
		return map.isEmpty();
	}
	
	public Vertex extractMin(){
		Vertex minVertex = null;
		Integer minValue = Integer.MAX_VALUE;
		for(Entry<Vertex, Integer> entry: map.entrySet()){
			if(entry.getValue() < minValue){
				minValue = entry.getValue();
				minVertex = entry.getKey();
			}
		}
		map.remove(minVertex);
		return minVertex;
	}
	
	public void put(Vertex v, Integer d){
		map.put(v, d);
	}
	
	public Integer get(Vertex v){
		return map.get(v);
	}
	
	public boolean contains(Vertex v){
		return map.containsKey(v);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "HeapMap [map=" + map + "]";
	}
	
	
	
	
	
}
