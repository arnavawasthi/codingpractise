/**
 * 
 */
package codingpractise.graph;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

/**
 * @author arnavawasthi
 *
 */
public class ConnectedComponents {

	public static void connectedComponents(Graph g){
		Map<Integer, Boolean> visitedNodes = new HashMap<Integer, Boolean>();
		Queue<Integer> q = new LinkedList<Integer>();
		
		for(Vertex v: g.vertices.values()){
			if(visitedNodes.containsKey(v.value)) continue;
			
			q.add(v.value);
			visitedNodes.put(v.value, true);
			System.out.println("Connected component: ");
			while(!q.isEmpty()){
				Integer toProcess = q.poll();
				System.out.println(toProcess);
				
				for(Vertex adj: g.vertices.get(toProcess).adjacents){
					if(visitedNodes.containsKey(adj.value)) continue;
					
					q.add(adj.value);
					visitedNodes.put(adj.value, true);
				}
			}
			
		}
		
	}
	
	public static void main(String[] args) {
		Graph g = new Graph();
		g.addEdge(1, 2);
		g.addEdge(1, 5);
//		g.addEdge(2, 3);
		g.addEdge(2, 5);
		g.addEdge(2, 1);
//		g.addEdge(3, 2);
		g.addEdge(3, 4);
		g.addEdge(4, 3);
//		g.addEdge(4, 5);
//		g.addEdge(5, 4);
		g.addEdge(5, 2);
		g.addEdge(5, 1);
		
		System.out.println("BFS starting from 1");
		GraphTraversal.bfsUndirected(g, g.vertices.get(1));
		System.out.println("BFS starting from 4");
		GraphTraversal.bfsUndirected(g, g.vertices.get(4));
		
		connectedComponents(g);
		
		
	}
}
