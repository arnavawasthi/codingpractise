/**
 * 
 */
package codingpractise.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Same Graph data structure works for both directed and undirected graphs (Confirmed?). 
 * Adjacency matrices have all the adjacent nodes for a given node. So it can maintain this relation.
 * Example: 1 > 2 (in directed graph). 1 will have 2 in adjacent matrices. But 2 will not have 1.
 * @author arnavawasthi
 *
 */
public class Graph {

	Map<Integer, Vertex> vertices = new HashMap<Integer, Vertex>();
	@Deprecated
	//Not maintaining list of edges. For a big graph this was taking a lot to time to build graph itself.
	List<Edge> edges = new ArrayList<Edge>();
	
	public void addVertex(Integer value){
		Vertex v = new Vertex(value);
		vertices.put(value, v);
	}
	
	public boolean addEdge(Integer v1, Integer v2){
		return addEdge(v1, v2, 1);
	}
	
	public boolean addEdge(Integer v1, Integer v2, int weight){
		Vertex vertex1 = vertices.get(v1);
		if(vertex1 == null){
			vertex1 = new Vertex(v1);
			vertices.put(v1, vertex1);
		}
		Vertex vertex2 = vertices.get(v2);
		if(vertex2 == null){
			vertex2 = new Vertex(v2);
			vertices.put(v2, vertex2);
		}
		
		Edge e = new Edge(vertex1, vertex2, weight);
//		if(!vertex1.adjacents.contains(vertex2)){
		vertex1.adjacents.add(vertex2);
		vertex1.edges.add(e);
//		}
//		if(!edges.contains(e)){
//			edges.add(e);
//			
//			return true;
//		}
		
		return true;
		
	}
	
	public void removeEdge(Edge e){
		Vertex vertex1 = e.vertex1;
		Vertex vertex2 = e.vertex2;
		
		
		vertices.remove(vertex1.value);
		
		Iterator<Edge> edgeIterator = edges.iterator();
		
		while(edgeIterator.hasNext()){
			Edge temp = edgeIterator.next();
			if(temp.equals(e)){
				edgeIterator.remove();
			}else{
				if(vertex1.equals(temp.vertex1)){
					temp.vertex1 = vertex2;
				}else if(vertex1.equals(temp.vertex2)){
					temp.vertex2 = vertex2;
				}
			}
		}
		
		
	}
	
	public static void main(String[] args) {
		Integer a = 100;
		Integer b = 101;
		Integer c = 102;
		Integer d = 103;
		
		Graph g = new Graph();
		g.addVertex(a);
		g.addVertex(b);
		g.addVertex(c);
		g.addVertex(d);
		
		System.out.println(g.vertices);

		g.addEdge(a, b);
		g.addEdge(a, c);
		System.out.println(g.edges);
		
		g.addEdge(b, a);
		g.addEdge(b, c);
		g.addEdge(b, d);
		System.out.println(g.edges);
		
		g.addEdge(c, a);
		g.addEdge(c, b);
		g.addEdge(c, d);
		System.out.println(g.edges);
		
		g.addEdge(d, c);
		g.addEdge(d, b);
		System.out.println(g.edges);
		
		System.out.println("Total edges: "+g.edges.size());
		
		g.removeEdge(g.edges.get(0));
		System.out.println(g.vertices);
		System.out.println(g.edges);
		
		g.removeEdge(g.edges.get(0));
		System.out.println(g.vertices);
		System.out.println(g.edges);
		
	}
	
}

class Vertex{
	Integer value;
	List<Vertex> adjacents = new ArrayList<Vertex>();
	List<Edge> edges = new ArrayList<Edge>();
	
	public Vertex(Integer value){
		this.value = value;
	}
	
	@Override
	public boolean equals(Object o){
		Vertex v = (Vertex)o;
		if(this.value.equals(v.value)){
			return true;
		}
		
		return false;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		List<Integer> l = new ArrayList<Integer>();
		for(Vertex a: adjacents){
			l.add(a.value);
		}
		
		return value+l.toString();
	}
	
}

class Edge{
	Vertex vertex1;
	Vertex vertex2;
	int weight;
	
	public Edge(Vertex vertex1, Vertex vertex2){
		this.vertex1 = vertex1;
		this.vertex2 = vertex2;
	}
	
	public Edge(Vertex vertex1, Vertex vertex2, int weight){
		this.vertex1 = vertex1;
		this.vertex2 = vertex2;
		this.weight = weight;
	}
	
	@Override
	public boolean equals(Object o){
		Edge e = (Edge)o;
		
		if((this.vertex1.equals(e.vertex1) && this.vertex2.equals(e.vertex2))
				|| (this.vertex2.equals(e.vertex1) && this.vertex1.equals(e.vertex2))){
			return true;
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		//This is done to have same hashCode for EB and BE (and other similar cases).
		return (1000*vertex1.value + vertex2.value) + (1000*vertex2.value + vertex1.value);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[" + vertex1 + ", " + vertex2 + "]";
	}
	
}