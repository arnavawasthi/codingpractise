/**
 * 
 */
package codingpractise.dynamic;

import java.util.Arrays;

/**
 * Video has better solution
 * https://www.youtube.com/watch?v=99ssGWhLPUE&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr&index=26
 * @author arnavawasthi
 *
 */
public class MaxSumIncreasingSubSeq {

	static int[] path;
	 
	static int itr;
	public static int solution(int[] a){
		System.out.println(" ============== Recursive ==============");
		itr = 0;
		
		path = new int[a.length];
		path[0] = -1;
		
		int max = helper(a, -1, 0);
		System.out.println("itr: "+itr);
		return max;
	}
	
	private static int helper(int[] a, int prevIndex, int curIndex){
		if(curIndex >= a.length) return 0;
		
		itr++;
		
		int max1 = 0, max2 = 0;
		
		if(prevIndex == -1 || a[curIndex] > a[prevIndex]){
			max1 = a[curIndex] + helper(a, curIndex, curIndex+1);
		}
		
		max2 = helper(a, prevIndex, curIndex+1);
		
		return Math.max(max1, max2);
		
	}
	
	static int subMax[][];
	public static int solutionDP(int a[]){
		System.out.println(" ============== Dynamic ==============");
		subMax = new int[a.length][a.length];
		
		itr = 0;
		int max = helperDP(a, -1, 0);
		
		System.out.println("itr: "+itr);
		for(int[] temp: subMax){
			System.out.println(Arrays.toString(temp));
		}
		return max;
	}
	
	private static int helperDP(int[] a, int prevIndex, int curIndex){
		if(curIndex >= a.length) return 0;
		if(prevIndex == -1 && subMax[curIndex][curIndex] > 0){
			return subMax[curIndex][curIndex];
		}else if(prevIndex != -1 && subMax[prevIndex][curIndex] > 0){
			return subMax[prevIndex][curIndex];
		}
		
		
		itr++;
		
		int max1 = 0, max2 = 0;
		if(prevIndex == -1 || a[curIndex] > a[prevIndex]){
			max1 = a[curIndex] + helperDP(a, curIndex, curIndex+1);
		}
		
		max2 = helperDP(a, prevIndex, curIndex+1);
		
		int max = Math.max(max1, max2);
		
		if(prevIndex == -1){
			prevIndex = curIndex;
		}

		subMax[prevIndex][curIndex] = max;
		
		return max;
	}
	
	
	
	public static void main(String[] args) {
		{
			int a[] = {4, 6, 1, 3, 8, 4, 6};
			System.out.println(solution(a));
			System.out.println(solutionDP(a));
		}
		
		{
			int a[] = {4, 6, 1, 2, 3, 4, 5, 8, 4, 6, 7, 10};
			System.out.println(solution(a));
			System.out.println(solutionDP(a));
		}
		
		{
			int a[] = {9,8,7,6,5,4,3,2,1};
			System.out.println(solution(a));
			System.out.println(solutionDP(a));
		}
		
		{
			int a[] = {1,2,3,4,5,6,7,8,9,10};
			System.out.println(solution(a));
			System.out.println(solutionDP(a));
		}
		
		{
			int max = 20;
			int a[] = new int[max];
			for(int i = 0; i < max; i++){
				a[i] = i;
			}
			
			System.out.println(solution(a));
			System.out.println(solutionDP(a));
		}
	}
}
