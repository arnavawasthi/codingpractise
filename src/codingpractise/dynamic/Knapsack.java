/**
 * 
 */
package codingpractise.dynamic;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author arnavawasthi
 *
 */
public class Knapsack {
	
	static int itrRec = 0;
	static int itrDP = 0;

	public static int solutionWithoutDP(int[] s, int[] v, int maxS){
		itrRec = 0;
		return helperWithoutDP(s, v, 0, maxS);
	}

	/**
	 * @param s
	 * @param v
	 * @param i
	 * @param maxS
	 * @return
	 */
	private static int helperWithoutDP(int[] s, int[] v, int i, int maxS) {
		if(maxS == 0) return 0;
		if(i >= s.length) return 0;
		
		itrRec++;
		
		int v1 = helperWithoutDP(s, v, i+1, maxS);
		int v2 = 0;
		int residual = maxS-s[i];
		if(residual >= 0){
			v2 = v[i] + helperWithoutDP(s, v, i+1, residual);
			
		}
		int max = Math.max(v1, v2);
//		System.out.println(String.format("s[i]: %d, v[i]: %d, v1: %d, v2: %d", s[i], v[i], v1, v2));
//		System.out.println(String.format("Size: %d, Value: %d", maxS, max));
		return max;
	}
	
	/*
	 * I am using Map for memoization (keeping answers for smaller subproblems). And creating string key for that.
	 * Better is to keep 2D array, looks cleaner. (Thoughts?)
	 */
	private static Map<String, Integer> map = new HashMap<String, Integer>();
	private static Integer solutionWithDP(int[] s, int[] v, int maxS) {
		map.clear();
		itrDP = 0;
		return helperWithDP(s, v, 0, maxS);
	}
	
	private static Integer helperWithDP(int[] s, int[] v, int i, int maxS) {
		if(maxS == 0 || i >= s.length) return 0;
//		System.out.println(map);
		String key = maxS+""+s[i];
		if(map.containsKey(key)) return map.get(key);
		
		itrDP++;
		
		int v1 = helperWithDP(s, v, i+1, maxS);
		int v2 = 0;
		if(s[i] <= maxS){
			v2 = v[i] + helperWithDP(s, v, i+1, maxS-s[i]);
		}
		int maxV = Math.max(v1, v2);
		map.put(key, maxV);
//		if(maxS < 6)
//			System.out.println(String.format("s[i]: %d, v[i]: %d, v1: %d, v2: %d", s[i], v[i], v1, v2));
//		System.out.println(String.format("maxS: %d, maxV: %d", maxS, maxV));
		
		return maxV;
	}

	/*
	 * It's not required for knapsack problem;
	 * 
	 */
	public static void reverse(int[] a){
		int i = 0, j = a.length-1;
		while(i < j){
			int temp = a[i];
			a[i] = a[j];
			a[j] = temp;
			i++;
			j--;
		}
		
	}
	
	public static void main(String[] args) {
		{
//			int[] s = {2, 3, 5, 10, 15, 20};
//			int[] v = {3, 3, 4, 6, 8, 20};
			
			int len = 100;
			int[] s= new int[len];
			int[] v = new int[len];
			for(int i = 0; i<len; i++){
				s[i] = i;
				v[i] = 2*i;
			}
			
			
			reverse(s);reverse(v);
//			int k = 30;
//			System.out.println(String.format("Knapsack: %d, withoutDP: %d, DP: %d", k, solutionWithoutDP(s, v, k), solutionWithDP(s, v, k)));
//			System.exit(0);
//			ShuffleArray.solution(s);
//			ShuffleArray.solution(v);
			System.out.println(Arrays.toString(s));
			System.out.println(Arrays.toString(v));
			
			for(int i = 1; i< len; i++){
				System.out.println(String.format("Knapsack: %d", i));
				long start = System.currentTimeMillis();
				int maxRec = solutionWithoutDP(s, v, i);
				long endRecur = System.currentTimeMillis();
				System.out.println(String.format("MaxValue Recursive: %d, Time: %d, Iterations Recursive: %d", maxRec, endRecur - start, itrRec));
				int maxDP = solutionWithDP(s, v, i);
			
				long endDP = System.currentTimeMillis();
				System.out.println(String.format("MaxValue DP: %d, Time: %d, Iterations Recursive: %d", maxDP, endDP - endRecur, itrDP));
				
			}
		}
	}


}
