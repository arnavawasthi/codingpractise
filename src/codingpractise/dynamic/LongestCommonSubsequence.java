/**
 * 
 */
package codingpractise.dynamic;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author arnavawasthi
 *
 */
public class LongestCommonSubsequence {

	static int counter = 0;
	private static Map<String, Integer> map;
	private static StringBuilder sbo;
	
	public static int solution(String s, String t, boolean dynamic){
		/*
		 * Debugging
		 */
		long start = System.currentTimeMillis();
		counter = 0;
		/*
		 * Debugging
		 */
		
		
		int output = 0;
		
		sbo = new StringBuilder();
		if(dynamic){
			map = new HashMap<String, Integer>();
			
			output = helperDynamic(s.toCharArray(), 0, t.toCharArray(), 0);
		}else{
			output = helper(s.toCharArray(), 0, t.toCharArray(), 0);
		}
		
		/*
		 * Debugging
		 */
		System.out.println("Iterations: "+counter);
		long time = System.currentTimeMillis() - start;
		System.out.println("Time: "+time);
		if(map != null){
			System.out.println("MapSize: "+map.size());
		}
		System.out.println("Matched: "+sbo.toString());
		/*
		 * Debugging
		 */
		
		return output;
	}
	

	/*
	 * Note: helperDynamic has 2 more lines than helper. putting in map and returning from map.
	 * There is no need to put into map immediately after calculating it ie. in if/else. 
	 * Because every value will be returned, so put just before returning it.
	 */
	private static int helperDynamic(char[] s, int si, char[] t, int ti) {
		if(si >= s.length || ti >= t.length) return 0;
		counter++;
		String key = si+""+ti;

		if(map.containsKey(key)) return map.get(key);

		int output = 0;
		if(s[si] == t[ti]){
			int temp = helperDynamic(s, si+1, t, ti+1);
			//TODO: Incorrect approach for matched sub sequence
			sbo.append(s[si]);
//			String tempKey = String.valueOf(si+1)+String.valueOf(ti+1);
//			map.put(tempKey, temp);
			output = 1+temp;
		}else{
			int temp1 = helperDynamic(s, si+1, t, ti);
//			String key1 = String.valueOf(si+1)+ti;
//			map.put(key1, temp1);
			
			int temp2= helperDynamic(s, si, t, ti+1);
//			String key2 = si + String.valueOf(ti+1);
//			map.put(key2, temp2);
			output = Math.max(temp1, temp2);
		}
		
		map.put(key, output);
		
		
		return output;
		
	}

	private static int helper(char[] s, int si, char[] t, int ti) {
		if(si >= s.length || ti >= t.length) return 0;
		
		counter++;
		
		if(s[si] == t[ti]){
			return 1+helper(s, si+1, t, ti+1);
		}
		
		return Math.max(helper(s, si+1, t, ti), helper(s, si, t, ti+1));
		
	}
	
	



	public static void main(String[] args) {
		System.out.println("Without dynamic programming:");
		System.out.println(solution("abc", "acdbc", false));
		System.out.println(solution("xmaxzbxcx", "adefcyuibuiybnhluc", false));
		System.out.println(solution("xmahyelsdbaxzblxcx", "adefcyudibbnhluc", false));
		System.out.println(solution("shri narayan jai narayan", "shri narayan shri laxmi jai laxmi narayan", false));
//		
		System.out.println();
		System.out.println("With dynamic programming:");
		System.out.println(solution("abc", "acdbc", true));
		System.out.println(solution("abc", "xyz", true));
		System.out.println(solution("xmaxzbxcx", "adefcyuibuiybnhluc", true));
		System.out.println(solution("xmahyelsdbaxzblxcx", "adefcyudibbnhluc", true));
		System.out.println(solution("shri narayan jai narayan", "shri narayan shri laxmi jai laxmi narayan", true));
//		System.out.println(solution("xmLJDdkdndudlhdhgyewowhdndlgyelsdaxzbxcx", "adefcyushdydyoalshsgalslshdyqoqlksgstqosalxibuiybnhluc"));
	}
}
