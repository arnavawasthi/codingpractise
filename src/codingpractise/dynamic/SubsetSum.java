/**
 * 
 */
package codingpractise.dynamic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import codingpractise.stringarray.ShuffleArray;


/**
 * https://www.youtube.com/watch?v=s6FhG--P7z0&index=4&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr
 * TIP: Ignore diagnostics lines of code.
 * @author arnavawasthi
 *
 */
public class SubsetSum {

	static int iterations;
	public static boolean solutionR(int[] a, int k){
		iterations = 0;
		boolean ans = helperR(a, 0, k);
		System.out.println("Recursive iterations: "+iterations);
		return ans;
	}
	
	public static boolean helperR(int[] a, int i, int k){
		if(i >= a.length || k < 0) return false;
		if(k == 0) return true;
		
		iterations++;
		
		return helperR(a, i+1, k) || helperR(a, i+1, k-a[i]);
		
	}
	
	static List<Map<Integer, Boolean>> dpStore;
	public static boolean solutionDP(int[] a, int k){
		iterations = 0;
		
		dpStore = new ArrayList<Map<Integer, Boolean>>(a.length);
		for(int i = 0; i < a.length; i++)
		{
			dpStore.add(new HashMap<Integer, Boolean>());
		}
		
		boolean ans = helperDP(a, 0, k);
//		System.out.println(dpStore);
		System.out.println("DP iterations: "+iterations);
		return ans;
	}
	
	public static boolean helperDP(int[] a, int i, int k){
		if(i >= a.length || k < 0) return false;
		if(k == 0) return true;
		if(dpStore.get(i).containsKey(k)) return dpStore.get(i).get(k);
		
		iterations++;
		
		 boolean t1 = helperDP(a, i+1, k);
		 boolean t2 = helperDP(a, i+1, k-a[i]);
		
		 boolean ans = t1 || t2;
//		 dpStore[i] = ans;
		 dpStore.get(i).put(k, ans);
		 
		 return ans;
	}
	
	public static void main(String[] args) {
		{
			int a[] = {10, 7, 2, 5, 8, 14, 2, 13, 8, 10};
			int k = 38;
			
			System.out.println(solutionR(a, k));
			System.out.println(solutionDP(a, k));
			
		}
		
		//TODO: Not sure why DP iterations are higher in this case.
		{
			int len = 20;
			int a[] = new int[len];
			for(int i = 0; i < len; i++){
				a[i] = i*2;
			}
			ShuffleArray.solution(a);
			int k = 100;
			
			System.out.println(solutionR(a, k));
			System.out.println(solutionDP(a, k));
			
		}
		
		{
			int len = 20;
			int a[] = new int[len];
			for(int i = 0; i < len; i++){
				a[i] = i*2;
			}
			ShuffleArray.solution(a);
			int k = 1000;
			
			System.out.println(solutionR(a, k));
			System.out.println(solutionDP(a, k));
			
		}
	}
}
