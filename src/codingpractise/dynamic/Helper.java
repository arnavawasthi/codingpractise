/**
 * 
 */
package codingpractise.dynamic;

import java.util.Arrays;

/**
 * @author arnavawasthi
 *
 */
public class Helper {

	public static void printMatrix(int[][] m){
		//debugging
		System.out.println("=======================");
		for(int[] store: m){
			System.out.println(Arrays.toString(store));
		}		
	}
}
