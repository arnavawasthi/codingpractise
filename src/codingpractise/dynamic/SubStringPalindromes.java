package codingpractise.dynamic;
/**
 * 
 */

/**
 * Return count of all substr palindromes in the given string.
 * It doesn't look like a DP problem
 * @author arnavawasthi
 *
 */
public class SubStringPalindromes {

	//N^3 solution
	public static int solution1(String s){
		char[] cs = s.toCharArray();
		int count = cs.length;
		int len = cs.length;
		
		int itr = 0;
		//most basic solution. process every substring to check if it is palindrome.
		for(int i = 0; i < len-1; i++){
			for(int j = i+1; j < len; j++){
				boolean palindrome = true;
				for(int k = i, l = j; k < l; k++, l--){
					itr++;
					if(cs[k] != cs[l]){
						palindrome = false;
					}
				}
				
				if(palindrome){
					count++;
				}
			}
		}
		
		System.out.println("N^3 iterations: "+ itr);
		return count;
	}
	
	//N^2 solution
	//There is one problem with this solution. When we move from center to both sides, we have 
	//assumed that palindrome of pattern "aba". 
	//Solution: keep two inner loops. One for each pattern.
	public static int solution2(String s){
		char[] cs = s.toCharArray();
		int len = cs.length;
		int count = 0;
		
		int itr = 0;
		//for every char in string, move both ways to find palindromes
		for(int i = 0; i < len; i++){
			
			//for patterns: aba
			for(int j = i, k = i; j >= 0 && k < len; j--,k++){
				itr++;
				if(cs[j] == cs[k]){
					count++;
				}else{
					break;
				}
			}
			
			//for patterns: abba
			for(int j=i, k=i+1; j>=0 && k < len; j--, k++){
				itr++;
				if(cs[j] == cs[k]){
					count++;
				}else{
					break;
				}
			}
		}
		System.out.println("N^2 iterations: "+ itr);
		return count;
		
	}
	
	public static void main(String[] args) {
		{
			String s = "abcdcb";
			System.out.println(solution1(s));
			System.out.println(solution2(s));
		}
		
		{
			String s = "abba";
			System.out.println(solution1(s));
			System.out.println(solution2(s));
		}
		{
			String s = "babaa";
			System.out.println(solution1(s));
			System.out.println(solution2(s));
		}
		
		{
			String s = "bananabananaa";
			System.out.println(solution1(s));
			System.out.println(solution2(s));
		}
		
		{
			String s = "abcdcbadsfwddsseegsddgsgdsdffsdabcdcbadsfwddsseegsddgsgdsdffsdabcdcbadsfwddsseegsddgsgdsdffsdabcdcbadsfwddsseegsddgsgdsdffsd";
			System.out.println(solution1(s));
			System.out.println(solution2(s));
		}
	}
	
}
