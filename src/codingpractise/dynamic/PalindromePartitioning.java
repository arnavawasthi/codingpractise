/**
 * 
 */
package codingpractise.dynamic;

/**
 * https://www.youtube.com/watch?v=lDYIvtBVmgo&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr&index=24
 * http://www.programcreek.com/2014/04/leetcode-palindrome-partitioning-ii-java/
 * @author arnavawasthi
 *
 */
public class PalindromePartitioning {

	
	//Your solution doesn't work. Find out why we can't have recursive solution for this problem.
	static int[][] dpStore;
	public static int solutionRec(String str){
		
		dpStore = new int[str.length()][str.length()];
		return helperRec(str, 0, str.length()-1, 1);
	}

	/**
	 * @param str
	 * @param i
	 * @param j
	 * @param k
	 * @return
	 */
	private static int helperRec(String str, int i, int j, int k) {
		if(k >= str.length()) return 0;
		if(isPalindrome(str, i, j)) return 0;
		
		//do partition at k
		boolean left = isPalindrome(str, i, k-1);
		boolean right = isPalindrome(str, k, j);
		if(right && left){
			return 1;
		}
		int leftX = j-i;
		if(left){
			leftX = 1 + helperRec(str, k, j, k+1); 
		}
		
		int rightX = j-i;
		if(right){
			rightX = 1 + helperRec(str, i, k-1, i+1);
		}

		int min1 = Math.min(leftX, rightX);
		
		int both = 1 + helperRec(str, i, k-1, i+1) + helperRec(str, k, j, k+1);
		
		System.out.println(String.format("i: %d, j: %d, splits: %d", i, j, Math.min(min1, both)));
		
		return Math.min(min1, both);
//		
//		//do not partition at k
//		boolean full = isPalindrome(str, i, j);
//		int y = j-i;
//		if(full){
//			y = 0;
//		}else{
////			y = helperRec(str, i, j, k+1);
//		}
		
//		System.out.println(String.format("i: %d, j: %d, splits: %d", i, j, Math.min(x, y)));
		
//		return Math.min(x, y);
	}
	
	private static boolean isPalindrome(String str, int i, int j){
		
		while(i < j){
			if(str.charAt(i) != str.charAt(j)){
//				System.out.println(String.format("i: %d, j: %d, Palindrome: %s", i, j, false));
				return false;
			}
			i++;
			j--;
		}
		
//		System.out.println(String.format("i: %d, j: %d, Palindrome: %s", i, j, true));
		return true;
	}
	
	public static void main(String[] args) {
		System.out.println(solutionRec("abcbm"));
	}
}
