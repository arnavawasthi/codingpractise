/**
 * 
 */
package codingpractise.dynamic;

import java.util.Arrays;

/**
 * 
 * https://www.youtube.com/watch?v=cETfFsSTGJI&index=25&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr
 * 
 * @author arnavawasthi
 *
 */
public class MinimumJumpToEnd {

	//TODO: I have not tracked the actual path. I have just calculated total min jumps.
	//To keep track of path, you need to create one more array. That will keep last index for every given index.
	static int min[];
	public static int solutionRec(int[] a){
		
		min = new int[a.length];
		//Note: first element is kept as 0 in the min array. That is known and that's the starting point of calculation.
		
		for(int i = 1; i < min.length; i++){
			min[i] = Integer.MAX_VALUE;
		}
		
		for(int i = 0; i < a.length; i++){
			for(int j = 1; j <= a[i]; j++){
				if(i+j >= a.length) continue;
				
				min[i+j] = Math.min(min[i+j], 1+min[i]);
			}
			
		}
		
		System.out.println(Arrays.toString(min));
		return min[min.length-1];
	}

	
	
	public static void main(String[] args) {
		{
			int a[] = {2, 3, 1, 1, 2, 4, 2, 0, 1, 1};
			System.out.println(solutionRec(a));
		}
	}
}
