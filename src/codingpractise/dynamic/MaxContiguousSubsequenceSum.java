/**
 * 
 */
package codingpractise.dynamic;

import java.util.Arrays;

/**
 * DP: Problem 6
 * Refer dpStorage array printed out in console after execution. My approach is: dpStorage[i] keeps max value from i to n.
 * We can also solve this problem with dpStorage[i] having max value from 0 to i.
 * @author arnavawasthi
 *
 */
public class MaxContiguousSubsequenceSum {
	
	
	public static int solutionDPItr(int[] a){
		System.out.println("DP Iterative");
		int max = Integer.MIN_VALUE;
		int[] dpStorage = new int[a.length];
		
		for(int i=0; i<a.length; i++){
			dpStorage[i] = Integer.MIN_VALUE;
		}
		
		dpStorage[a.length-1] = a[a.length-1];
		for(int j = a.length-2; j >= 0; j--){
			dpStorage[j] = Math.max(a[j], a[j] + dpStorage[j+1]);
			max = Math.max(dpStorage[j], max);
		}
		
		System.out.println(Arrays.toString(dpStorage));
		return max;
	}

	private static int[] dpStorage;
	private static int max = Integer.MIN_VALUE;
	
	//You can do it iteratively as well with DP
	//There is one more way of solving this, without DP and extra storage. ie. Time: O(N), Space: O(1)
	public static int solutionDPRecur(int[] a){
		System.out.println("DP Recursive");
		
		dpStorage = new int[a.length];
		for(int i = 0; i < dpStorage.length; i++){
			dpStorage[i] = Integer.MIN_VALUE;
		}
		
		return helper(a, 0);
	}

	private static int helper(int[] a, int i) {
		if(i == a.length) return 0;

		if(dpStorage[i] != Integer.MIN_VALUE) return dpStorage[i];
		
		int temp = Math.max(a[i], a[i] + helper(a, i+1));
		dpStorage[i] = temp;
		max = Math.max(max, temp);
		
		
		return temp;
	}
	
	public static void main(String[] args) {
		{
			int[] a = {-2, 11, -4, 13, -5, 2};
			solutionDPRecur(a);
			System.out.println(max);
			System.out.println(Arrays.toString(dpStorage));
			
			System.out.println(solutionDPItr(a));
		}
	}
}
