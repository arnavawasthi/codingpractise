/**
 * 
 */
package codingpractise.dynamic;

/**
 * https://www.youtube.com/watch?v=3ZDZ-N0EPV0&index=33&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr
 * @author arnavawasthi
 *
 */
public class PatternMatching {
	
	public static boolean solutionDP(String s, String p){
		char[] sc = s.toCharArray();
		char[] pc = p.toCharArray();
		
		Boolean[][] dpStorage = new Boolean[sc.length][pc.length];
		
		return helperDP(sc, 0, pc, 0, dpStorage);
	}
	
	
	private static boolean helperDP(char[] sc, int i, char[] pc, int j,
			Boolean[][] dpStorage) {

		if(i == sc.length && j == pc.length) return true;
		if(i == sc.length || j == pc.length) return false;
		
		if(dpStorage[i][j] != null) return dpStorage[i][j];
		
		boolean output = false;
		if(sc[i] == pc[j] || pc[j] == '?'){
			output = true && helperDP(sc, i+1, pc, j+1, dpStorage);
		}else if(pc[j] == '*'){
			output = true && (helperDP(sc, i+1, pc, j, dpStorage) || helperDP(sc, i, pc, j+1, dpStorage));
		}
		dpStorage[i][j] = output;
		
		return output;
	}



	public static boolean solution(String s, String p){
		char[] sc = s.toCharArray();
		char[] pc = p.toCharArray();
		
		return helper(sc, 0, pc, 0);
		
	}

	private static boolean helper(char[] sc, int i, char[] pc, int j) {
		if(i >= sc.length && j >= pc.length) return true;
		if(i >= sc.length || j >= pc.length) return false;
		
		boolean output = false;
		if(sc[i] == pc[j] || pc[j] == '?'){
			output = true && helper(sc, i+1, pc, j+1);
		}else if(pc[j] == '*'){
			output = true && (helper(sc, i+1, pc, j) || helper(sc, i, pc, j+1));
		}else{
			output = false;
		}
		
		return output;
	}
	
	public static void main(String[] args) {
		{
			String s = "abc";
			String p = "a?c";
			System.out.println(solution(s, p));
			System.out.println(solutionDP(s, p));
		}
		
		{

			String s = "abc";
			String p = "a*c";
			System.out.println(solution(s, p));
			System.out.println(solutionDP(s, p));
		}
		
		{

			String s = "abcc";
			String p = "a?c";
			System.out.println(solution(s, p));
			System.out.println(solutionDP(s, p));
			
		}
		{
			String s = "ac";
			String p = "a*c";
			System.out.println(solution(s, p));
			System.out.println(solutionDP(s, p));
		}
		{

			String s = "abcccssdeecc";
			String p = "a*c";
			System.out.println(solution(s, p));
			System.out.println(solutionDP(s, p));
		}
	}

}
