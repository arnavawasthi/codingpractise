/**
 * 
 */
package codingpractise.dynamic;

import java.util.Arrays;

/**
 * https://www.youtube.com/watch?v=CE2b_-XfVDk&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr&index=6
 * TODO: There is a nLog(n) method as well at: https://www.youtube.com/watch?v=S9oUiVYEq7E
 * @author arnavawasthi
 *
 */
public class LongestIncreasingSubSeq {

	public static int solutionDP(int a[]){
		
		int len = a.length;
		//This array keeps the length of increasing subseq ending at each index
		int[] temp = new int[len];
		for(int i = 0; i < len; i++){
			temp[i] = 1;
		}
		
		for(int i = 1; i < len; i++){
			for(int j = 0; j < i; j++){
				if(a[j] < a[i]){
					temp[i] = Math.max(temp[i], temp[j]+1);
				}
			}
		}
		System.out.println(Arrays.toString(temp));
		int max = temp[0];
		
		for(int i = 1; i < len; i++){
			max = Math.max(max, temp[i]);
		}
		
		return max;
	}
	
	
	public static void main(String[] args) {
		{
			int[] a = {4, 2, 1, 3, 4, 5, 9, 5, 6, 7, 8};
			System.out.println(solutionDP(a));
		}
	}
}
