/**
 * 
 */
package codingpractise.dynamic;

/**
 * TODO: Write simple recursive version and compare performance by counting iterations. 
 * TODO: Write iterative version of DP
 * Note: Jobs must be sorted by their end time.
 * https://www.youtube.com/watch?v=cr6Ip0J9izc&index=13&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr
 * @author arnavawasthi
 *
 */
public class WeightedJobScheduling {
	
	
	//Ax3 array. 0 > start, 1 > end, 2 > weight
	//Target is to maximize the total weight
	static int[][] dpStorage;
	public static int solutionDPRec(int[][] jobs){
		int maxEnd = jobs[jobs.length-1][1];
		dpStorage = new int[jobs.length][maxEnd];
		return helperDPRec(jobs, 0, 0);
	}

	/*
	 * 1. If I keep value calculated for each "i" in dpStorage, it won't work. Because it's possible that max value
	 * for that index is overwritten by subsequent calculation.
	 * 2. If I keep max value calculated for each "i" in dpStorage, it won't work. Because once a value is calculated for "i",
	 * it will be always returned. 
	 * 3. Correct calculation is to have 2d dpStorage and keep value for "i" and "end"
	 */
	private static int helperDPRec(int[][] jobs, int i, int end) {
		
		if(i >= jobs.length) return 0;
		if(dpStorage[i][end] > 0) return dpStorage[i][end];
		
		int max1 = 0;
		if(jobs[i][0] >= end){
			max1 = jobs[i][2] + helperDPRec(jobs, i+1, jobs[i][1]);
			
		}
		
		int max2 = helperDPRec(jobs, i+1, end);
		
		
		int ret =  Math.max(max1, max2);
		System.out.println(String.format("Index: %d, End: %d, Picked: %d, Skipped: %d, Max: %d", i, end, max1, max2, ret));
		dpStorage[i][end] = Math.max(ret, dpStorage[i][end]);
		
		return ret;
	}
	
	public static void main(String[] args) {
		int[][] jobs = {{1, 3, 5}, {2, 5, 6}, {4, 6, 5}, {6, 7, 4}, {5, 8, 11}, {7, 9, 2}};
		
		System.out.println(solutionDPRec(jobs));
	}
}
