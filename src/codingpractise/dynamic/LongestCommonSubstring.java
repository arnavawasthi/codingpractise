/**
 * 
 */
package codingpractise.dynamic;

import java.util.Arrays;

/**
 * TODO: Incomplete solution
 * https://www.youtube.com/watch?v=BysNXJHzCEs&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr&index=15
 * @author arnavawasthi
 *
 */
public class LongestCommonSubstring {

	//Tried to use recursive solution from longest common subsequence problem. But can't think of 
	//a way to modify it to suit this problem.
	public static void solution(String s, String t){
		
		char[] ss = s.toCharArray();
		char[] st = t.toCharArray();
		int slen = ss.length;
		int tlen = st.length;
		
		int[][] dpStorage = new int[s.length()][t.length()];
		//initializing matrix with -1. if all are zeroes, we don't know when to return output from dpStorage
		for(int i = 0; i < slen; i++){
			for(int j = 0; j < tlen; j++){
				dpStorage[i][j] = -1;
			}
		}
		
		//This will build the matrix
		helper(ss, 0, st, 0, dpStorage);
		
		Helper.printMatrix(dpStorage);
		
		//now dpStorage has length of longest sub string. 
		
	}

	private static void helper(char[] sc, int i, char[] st, int j, int[][] dpStorage) {
		
		if(i >= sc.length || j >= st.length) return;
//		printMatrix(dpStorage);
		if(dpStorage[i][j] >=0 ) return;
		
		if(sc[i] == st[j]){
			//This is important step.
			//In recursion, data builds from end. so instead of [i-1][j-1], we should use [i+1][j+1]
			//starting with 1, if any of the string has reached end.
			dpStorage[i][j] = (i+1 >= sc.length || j+1 >= st.length)?1:dpStorage[i+1][j+1]+1;
			helper(sc, i+1, st, j+1, dpStorage);
		}else{
			dpStorage[i][j] = 0;
			helper(sc, i+1, st, j, dpStorage);
			helper(sc, i, st, j+1, dpStorage);
			
		}
	}
	
	public static void main(String[] args) {
		{
			solution("abcdaf", "zbcdf");
			solution("kanpuragradelhiludhiyana", "varanasikanpurmathurakanpuragraludhiyana");
		}
	}
	
	
}
