/**
 * 
 */
package codingpractise.dynamic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author arnavawasthi
 *
 */
public class KnapsackDuplicate {
	
	public static double solution(int[] s, double[] v, int sack){
		System.out.println("Sack size: "+sack);
		int len = s.length;
		int minSize = Integer.MAX_VALUE;
		List<Item> items = new ArrayList<Item>();
		
		for(int i = 0; i < len; i++){
			items.add(new Item(s[i], v[i], v[i]/s[i]));
			//Finding out minimum size of item, will be used in breaking out of loop
			minSize = Math.min(minSize, s[i]);
		}
		
		//Reverse sorted
		Collections.sort(items, new Comparator<Item>() {
			public int compare(Item i1, Item i2){
				
				if(i1.ratio < i2.ratio){
					return 1;
				}else if(i1.ratio > i2.ratio){
					return -1;
				}
				//If ratios are same, smaller size should go first
				else if(i1.size > i2.size){
					return 1;
				}
				
				return 0;
			}
		});
		
//		System.out.println(items);
		
		double max = 0;
		
		for(int i = 0; i < len; i++){
			int remaining = sack;
			double value = 0;
			for(int j = i; j < len; j++){
				Item item = items.get(j);
				if(remaining/item.size > 0){
					int itemCount = remaining/item.size;
					remaining = remaining - itemCount*item.size;
					value = value + itemCount*item.value;
					
					if(remaining < minSize) break;
				}
			}
			System.out.println("Current Max: "+value);
			max = Math.max(max, value);
		}
		
		
		return max;
		
	}
	
	public static void main(String[] args) {
		{
			int s[] = {15, 20, 2, 3, 5};
			double v[] = {14, 20, 1, 2, 3};
			
			System.out.println(solution(s, v, 32));
			System.out.println(solution(s, v, 31));
			System.out.println(solution(s, v, 30));
			System.out.println(solution(s, v, 29));
			System.out.println(solution(s, v, 28));
			System.out.println(solution(s, v, 27));
			
		}
	}

}

class Item{
	int size;
	double value;
	double ratio;
	
	public Item(int s, double v, double r){
		size = s;
		value = v;
		ratio = r;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Item [size=" + size + ", value=" + value + ", ratio=" + ratio
				+ "]";
	}
	
	
	
}
