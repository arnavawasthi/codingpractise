/**
 * 
 */
package codingpractise.dynamic;

import java.util.Arrays;


/**
 * https://www.youtube.com/watch?v=Y0ZqKpToTic&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr&index=5
 *  - Greedy approach doesn't work. If we start from the biggest coin and iterate towards smaller, it will not always work.
 *  - 
 * @author arnavawasthi
 *
 */
public class CoinChangeMin {

	//TODO: Try recursive solution with and without DP.
	public static int solutionDP(int[] coins, int target){
		System.out.println(String.format("================= Total: %d =================", target));
		int[] temp = new int[target + 1];
		for(int i = 1; i <= target; i++){
			temp[i] = target;
		}
		
		for(int coin: coins){
			for(int i = 1; i <= target; i++){
				if(i - coin < 0 || temp[i-coin] < 0) continue;
				
				temp[i] = Math.min(temp[i], 1+temp[i-coin]);
			}
		}
		
		
		System.out.println(Arrays.toString(temp));
		
		return temp[target];
	}
	
	public static void main(String[] args) {
		{
			int[] coins = {8, 20, 1, 16};
			for(int i = 10; i < 50; i++){
				System.out.println(solutionDP(coins, i));
			}
		}
	}
}
