/**
 * 
 */
package codingpractise.dynamic;


/**
 * https://www.youtube.com/watch?v=_nCsPn7_OgI&index=9&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr
 * @author arnavawasthi
 *
 */
public class LongestPalindromeSubSeq {

	static int iterations = 0;
	public static int solutionRec(String s){
		char[] cs = s.toCharArray();
		iterations = 0;
		int len = helperRec(cs, 0, s.length()-1);
		System.out.println(String.format("solutionRec: Input length: %s, Iterations: %s", s.length() , iterations));
		return len;
	}

	/**
	 * @param s
	 * @param i
	 * @param j
	 * @return
	 */
	private static int helperRec(char[] cs, int i, int j) {
		if(j < i) return 0;
		if(j == i) return 1;
		
		iterations++;
		
		int eq = 0;
		int diff = 0;
		if(cs[j] == cs[i]){
			eq = 2 + helperRec(cs, i+1, j-1);
		}else{
			diff = Math.max(helperRec(cs, i, j-1), helperRec(cs, i+1, j));
		}
		
		
		return Math.max(eq, diff);
	}
	
	static int[][] dpStore;
	public static int solutionDP(String s){
		iterations = 0;
		
		char[] cs = s.toCharArray();
		int len = cs.length;
		dpStore = new int[len][len];
		int output = helperDP(cs, 0, len-1);
		System.out.println(String.format("solutionDP: Input length: %s, Iterations: %s", len , iterations));
		
		Helper.printMatrix(dpStore);
		
		return output;
	}
	
	
	
	private static int helperDP(char[] cs, int i, int j) {
		if(j < i) return 0;
		if(i == j) return 1;
		if(dpStore[i][j] != 0){
//			System.out.println(dpStore[i][j]);
			return dpStore[i][j];
		}
		
		iterations++;
		
		int eq = 0, diff = 0; 
		if(cs[i] == cs[j]){
			eq = 2 + helperDP(cs, i+1, j-1);
		}else{
			diff = Math.max(helperDP(cs, i+1, j), helperDP(cs, i, j-1));
		}
		int output = Math.max(eq, diff);
		
		dpStore[i][j] = output;
		
		return output;
	}

	public static void main(String[] args) {
		{
			String s = "arnav";
			System.out.println(solutionRec(s));
			System.out.println(solutionDP(s));
		}
		
		{
			String s = "vbasnarnav";
			System.out.println(solutionRec(s));
			System.out.println(solutionDP(s));
		}
		
		{
			String s = "abcdefghijk";
			System.out.println(solutionRec(s));
			System.out.println(solutionDP(s));
		}
		
		{
			String s = "vbasnarnavvbasnarnavvbasnarnavvbasnarnavv";
			System.out.println(solutionRec(s));
			System.out.println(solutionDP(s));
		}
	}
	
	
}
