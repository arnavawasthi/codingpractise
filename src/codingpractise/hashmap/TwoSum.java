/**
 * 
 */
package codingpractise.hashmap;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Discussion thread: https://www.coursera.org/learn/algorithm-design-analysis/discussions/weeks/6/threads/WJrUB0RGEeakuhJbRt69hQ
 * 
 * Week 6:
 * Question 1:
 * The goal of this problem is to implement a variant of the 2-SUM algorithm
 * (covered in the Week 6 lecture on hash table applications).
 * 
 * The file contains 1 million integers, both positive and negative (there might
 * be some repetitions!).This is your array of integers, with the ith row of the
 * file specifying the ith entry of the array.
 * 
 * Your task is to compute the number of target values t in the interval
 * [-10000,10000] (inclusive) such that there are distinct numbers x,y in the
 * input file that satisfy x+y=t. (NOTE: ensuring distinctness requires a
 * one-line addition to the algorithm from lecture.)
 * 
 * Write your numeric answer (an integer between 0 and 20001) in the space
 * provided.
 * 
 * OPTIONAL CHALLENGE: If this problem is too easy for you, try implementing
 * your own hash table for it. For example, you could compare performance under
 * the chaining and open addressing approaches to resolving collisions.
 * 
 * @author arnavawasthi
 * 
 */
public class TwoSum {
	
	public static void solution1() throws FileNotFoundException{
		System.out.println("Started ...");
		Set<Long> input = new HashSet<Long>();
		Set<Long> output = new HashSet<Long>();
		int counter = 0;
		long[] inputArr = new long[1000000];
		Scanner s = new Scanner(new File("/Users/arnavawasthi/Downloads/algo1-programming_prob-2sum.txt"));
		int inputCounter = 0;
		while(s.hasNextLine()){
			Long i = Long.parseLong(s.nextLine());
			input.add(i);
			inputArr[inputCounter] = i;
			inputCounter++;
			
		}
		s.close();
		System.out.println("Filled ...");
		
		for(int i = -10000; i <= 10000; i++){
			for(long x: inputArr){
				long y = i - x;
				if(x!=y && input.contains(y)){
					System.out.println("Found: "+x+"+"+y+"="+i);
					counter++;
					break;
				}
			}
		}
		//427
		System.out.println(counter);		
	}
	
	//Not working yet.
	public static void solution2() throws FileNotFoundException{
		System.out.println("Started ...");
		Set<Long> output = new HashSet<Long>();
		int counter = 0;
		long[] inputArr = new long[1000000];
		Scanner s = new Scanner(new File("/Users/arnavawasthi/Downloads/algo1-programming_prob-2sum.txt"));
		int inputCounter = 0;
		while(s.hasNextLine()){
			Long i = Long.parseLong(s.nextLine());
			inputArr[inputCounter] = i;
			inputCounter++;
			
		}
		s.close();
		System.out.println("Filled ...");
		
//		for(int i = -10000; i <= 10000; i++){
//			for(long x: inputArr){
//				long y = i - x;
//				if(x!=y && input.contains(y)){
//					System.out.println("Found: "+x+"+"+y+"="+i);
//					counter++;
//					break;
//				}
//			}
//		}
		//427
		System.out.println(counter);
	}

	public static void main(String[] args) throws FileNotFoundException {
		solution1();
		
		solution2();
	}
}
