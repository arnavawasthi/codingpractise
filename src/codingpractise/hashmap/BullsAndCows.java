package codingpractise.hashmap;
import java.util.Map;
import java.util.HashMap;

/**
 * http://www.programcreek.com/2014/05/leetcode-bulls-and-cows-java/
 * 
 * @author arnavawasthi
 *
 */
public class BullsAndCows{

  private Map<Integer, Integer> digitPositionsMap = new HashMap<Integer, Integer>();

  //Extracting digits routine is written twice in this code. One way is to move this logic to a method.
  // And return an array of digits.
  public BullsAndCows(Integer number){
      int position = 0;
      while(number != 0){
        int digit = number%10;
        digitPositionsMap.put(digit, position);
        position++;

        number = number/10;
      }
      System.out.println(digitPositionsMap);
  }

  //This can also be implemented using an array[10]. Max size 10 will be required,
  // because we just have to store position of digits(0-9 = 10)
  //With this there is also an assumption: number will have unique digits.
  public String solution(Integer guess){
    int position = 0;
    int acounter = 0;
    int bcounter = 0;
    while(guess != 0){
      int digit = guess%10;
      if(digitPositionsMap.containsKey(digit)){
        if(digitPositionsMap.get(digit) == position){
          acounter++;
        }else{
          bcounter++;
        }
      }
      position++;
      guess = guess/10;
    }
    return acounter+"A"+bcounter+"B";
  }

  public static void main(String args[]){
    BullsAndCows bnc = new BullsAndCows(1807);
    System.out.println(bnc.solution(7810));
    System.out.println(bnc.solution(1840));
    System.out.println(bnc.solution(1845));

  }
}
