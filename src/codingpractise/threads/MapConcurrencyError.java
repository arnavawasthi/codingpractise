/**
 * 
 */
package codingpractise.threads;

import java.util.HashMap;
import java.util.Map;

/**
 * @author arnavawasthi
 *
 */
public class MapConcurrencyError {

	public Map<String, String> map = new HashMap<String, String>();
	
	public void printMap(){
		for(String key: map.keySet()){
			System.out.println(key+": "+map.get(key));
		}
	}
	
	public void deleteEntry(String key){
		map.remove(key);
	}
	
	public static void main(String[] args) {
		final Integer count = 500;
		final MapConcurrencyError m = new MapConcurrencyError();
		for(Integer i = 0; i< count; i++){
			m.map.put(i.toString(), i.toString());
		}
		
		Runnable r1 = new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				m.printMap();
			}
		};
		Thread t1 = new Thread(r1);
		t1.start();
		System.out.println("t1 thread started");
		
		Runnable r2 = new Runnable() {
			
			@Override
			public void run() {
				for(Integer i = 0; i< count; i++){
//					System.out.println("Deleting key: "+ i);
					m.deleteEntry(i.toString());
				}
			}
		};
		Thread t2 = new Thread(r2);
		t2.start();
		System.out.println("t2 thread started");
		
//		m.printMap();
	}
	
}
