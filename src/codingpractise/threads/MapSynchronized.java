/**
 * 
 */
package codingpractise.threads;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author arnavawasthi
 *
 */
public class MapSynchronized {

	public Map<String, String> map = Collections.synchronizedMap(new HashMap<String, String>());
	
	synchronized public void printMap(){
		for(String key: map.keySet()){
			System.out.println(key+": "+map.get(key));
		}
	}
	
	synchronized public void deleteEntry(String key){
		map.remove(key);
	}
	
	public Boolean isEmpty(){
		return map.isEmpty();
	}
	public static void main(String[] args) {
		final Integer count = 50;
		final MapSynchronized m = new MapSynchronized();
		for(Integer i = 0; i< count; i++){
			m.map.put(i.toString(), i.toString());
		}
		
		Runnable r1 = new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				m.printMap();
			}
		};
		Thread t1 = new Thread(r1);
		t1.start();
		System.out.println("t1 thread started");
		
		
		Runnable r2 = new Runnable() {
			
			@Override
			public void run() {
				for(Integer i = 0; i< count; i++){
//					System.out.println("Deleting key: "+ i);
					m.deleteEntry(i.toString());
				}
			}
		};
		Thread t2 = new Thread(r2);
		t2.start();
		System.out.println("t2 thread started");
		
		Runnable r3 = new Runnable() {
			
			@Override
			public void run() {
				for(Integer i = 0; i< count; i++){
					System.out.println(m.isEmpty());
				}
				
			}
		};
		Thread t3 = new Thread(r3);
		t3.start();
	}
	
}
