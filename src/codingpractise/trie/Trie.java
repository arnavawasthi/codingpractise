/**
 * 
 */
package codingpractise.trie;

import java.util.HashMap;
import java.util.Map.Entry;

/**
 * @author arnavawasthi
 *
 */
public class Trie {
	TrieNode root;
	
	public Trie(){
		root = new TrieNode(new HashMap<Character, TrieNode>(), false);
	}
	
	public void addString(String str){
		
		TrieNode cur = root;
		for(int i = 0; i < str.length(); i++){
			
			char ch = str.charAt(i);
			TrieNode temp = cur.getNext(ch);
			
			if(temp == null){
				temp = new TrieNode(new HashMap<Character, TrieNode>(), false);
				cur.setNext(ch, temp);
			}
			cur = temp;
		}
		
		cur.setLeaf();
		
	}

	public boolean hasString(String str){
		System.out.println("hasString(): "+str);
		TrieNode cur = root;
		for(int i = 0; i < str.length(); i++){
//			System.out.println(str.charAt(i));
			cur = cur.getNext(str.charAt(i));
			if(cur == null){
				return false;
			}
		}
		
		return cur.isLeaf();
	}
	
	public boolean hasPattern(String pattern){
		System.out.println("hasPattern(): "+pattern);
		return hasPatternHelper(root, pattern, 0);
	}
	
	//Note: I tried multiple variations of return. All works. Why?
	private boolean hasPatternHelper(TrieNode node, String pattern, int i){
//		System.out.println(String.format("isLeaf: %s, char: %c", node.isLeaf()+"", pattern.charAt(i)));
		if(node == null || i > pattern.length()) return false;
		if(node.isLeaf() && i == pattern.length()) return true;

		char ch = pattern.charAt(i);
//		System.out.println("char: "+ch);
//		boolean ret = false;
		if(ch == '.'){
//			System.out.println(node.getNodes().keySet());
			for(Entry<Character, TrieNode> entry: node.getNodes().entrySet()){
//				ret = ret || hasPatternHelper(entry.getValue(), pattern, i+1);
//				return hasPatternHelper(entry.getValue(), pattern, i+1);
				if(hasPatternHelper(entry.getValue(), pattern, i+1)){
					return true;
				}
			}
			
		}else{
//			ret = hasPatternHelper(node.getNext(ch), pattern, i+1);
			return hasPatternHelper(node.getNext(ch), pattern, i+1);
		}
		
//		return ret;
		return false;
		
	}
	
	public static void main(String[] args) {
		Trie trie = new Trie();
		
		trie.addString("arnav");
		trie.addString("arn");
		trie.addString("nupur");
		trie.addString("nakul");
		trie.addString("neha");
		trie.addString("som");
		trie.addString("nidhi");
		System.out.println(trie.hasString(""));
		System.out.println(trie.hasString("a"));
		System.out.println(trie.hasString("ar"));
		System.out.println(trie.hasString("arn"));
		System.out.println(trie.hasString("arna"));
		System.out.println(trie.hasString("arnav"));
		System.out.println(trie.hasString("nidhi"));
		System.out.println(trie.hasString("neha"));
		
		System.out.println(trie.hasPattern("a.nav"));
		System.out.println(trie.hasPattern("ar......."));
		System.out.println(trie.hasPattern("n...."));
		System.out.println(trie.hasPattern("n..u."));
				
	}
}
