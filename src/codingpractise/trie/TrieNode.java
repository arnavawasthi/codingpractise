/**
 * 
 */
package codingpractise.trie;

import java.util.Map;

/**
 * @author arnavawasthi
 *
 */
class TrieNode{
	
	private Map<Character, TrieNode> nodes;
	private boolean isLeaf;
	
	public TrieNode(Map<Character, TrieNode> nodes, boolean isLeaf){
		this.nodes = nodes;
		this.isLeaf = isLeaf;
	}
	
	public boolean hasChar(char ch){
		return nodes.containsKey(ch);
	}
	
	public TrieNode getNext(char ch){
		return nodes.get(ch);
	}
	
	public void setNext(char ch, TrieNode node){
		nodes.put(ch, node);
	}
	
	public void setLeaf(){
		this.isLeaf = true;
	}
	
	public boolean isLeaf(){
		return isLeaf;
	}
	
	public Map<Character, TrieNode> getNodes(){
		return nodes;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TrieNode [nodes=" + nodes + ", isLeaf=" + isLeaf + "]";
	}
	
	
}