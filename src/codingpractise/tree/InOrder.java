/**
 * 
 */
package codingpractise.tree;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * http://www.programcreek.com/2012/12/leetcode-solution-of-binary-tree-inorder-traversal-in-java/
 * @author arnavawasthi
 *
 */
public class InOrder {

	public static void inOrderRecursive(TreeNode node){
		if(node == null) return;
		
		inOrderRecursive(node.left);
		System.out.print(node.data+"->");
		inOrderRecursive(node.right);
	}
	
	public static void inOrderIterativeSuboptimal(TreeNode root){
		Map<TreeNode, Boolean> seen = new HashMap<TreeNode, Boolean>();
		Stack<TreeNode> stack = new Stack<TreeNode>();
		
		stack.add(root);
		
		while(!stack.isEmpty()){
//			System.out.println(seen);
			TreeNode cur = stack.peek();
			if(cur.left == null || seen.containsKey(cur.left)){
				stack.pop();
				//False: We can remove left pointer, if exists. So map size be constant
//				seen.remove(cur.left);
				seen.put(cur, true);
				System.out.print(cur.data+"->");
				if(cur.right != null) stack.add(cur.right);
			}else{
				if(cur.left != null) stack.add(cur.left);
			}
		}
		System.out.println();
	}
	
	/*
	 * One variable approach will not work, it goes into a loop and just keep on printing left trail nodes.
	 */
	public static void inOrderIterativeWithoutMap(TreeNode root){
		TreeNode seen = null;
		Stack<TreeNode> stack = new Stack<TreeNode>();
		
		stack.add(root);
		int counter = 0;
		while(!stack.isEmpty()){
//			System.out.println(seen);
			TreeNode cur = stack.peek();
			if(cur.left == null || seen == cur.left){
				stack.pop();
				seen = cur;
				System.out.print(cur.data+"->");
				if(cur.right != null) stack.add(cur.right);
			}else{
				if(cur.left != null) stack.add(cur.left);
			}
			
			counter++;
			if(counter > 15) break;
		}
		System.out.println();
	}
	
	public static void inOrderIterativeBest(TreeNode root){
		Stack<TreeNode> stack = new Stack<TreeNode>();
		TreeNode p = root;
		
		while(!stack.isEmpty() || p != null){
			if(p != null){
				stack.push(p);
				p = p.left;
				
			}else{
				p = stack.pop();
				System.out.print(p.data+"->");
				
				p = p.right;
			}
		}
		System.out.println();
	}
	
	
	public static void main(String[] args) {
		{
			String[] nodes = {"1", "2", "3", "4", "5", "6", "7", "8"};
			TreeNode root = TreeNode.buildTree(nodes);
			
			inOrderRecursive(root);
			System.out.println();
			inOrderIterativeSuboptimal(root);
			inOrderIterativeBest(root);
//			inOrderIterativeWithoutMap(root);
			
			
		}
		
	}
}
