/**
 * 
 */
package codingpractise.tree;

/**
 * http://www.programcreek.com/2013/01/leetcode-convert-sorted-array-to-binary-search-tree-java/
 * https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/
 * @author arnavawasthi
 *
 */
public class BSTFromArray {

	public static TreeNode solution(int[] arr){
		
		TreeNode root = new TreeNode(arr[0]);
		helper(root, arr, 0, arr.length-1);
		
		return root.right;
	}

	/**
	 * @param root
	 * @param i
	 * @param j
	 */
	private static void helper(TreeNode root, int[] arr, int l, int h) {
		if(l>h) return;
		
		int m = l + (h-l)/2;
		TreeNode node = insert(root, arr[m]);
		
		helper(node, arr, l, m-1);
		helper(node, arr, m+1, h);
		
	}

	/**
	 * @param root
	 * @param i
	 */
	private static TreeNode insert(TreeNode root, int data) {
		// TODO Auto-generated method stub
		TreeNode node = new TreeNode(data);
		if(data < root.data){
			root.left = node;
		}else{
			root.right = node;
		}
		return node;
	}
	
	public static void main(String[] args) {
		{
			int[] arr = {1, 2, 3, 4, 5, 6, 7};
			TreeNode root = solution(arr);
			LevelOrder.levelOrderPrint(root);
			InOrder.inOrderIterativeBest(root);
			System.out.println(ValidateBinarySearchTree.isValid(root));
			System.out.println(ValidateBinarySearchTree.isValidInOrder(root));
		}
	}
}
