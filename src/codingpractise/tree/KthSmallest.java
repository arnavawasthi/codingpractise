/**
 * 
 */
package codingpractise.tree;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/**
 * http://www.programcreek.com/2014/07/leetcode-kth-smallest-element-in-a-bst-java/
 * https://leetcode.com/problems/kth-smallest-element-in-a-bst/
 * @author arnavawasthi
 *
 */
public class KthSmallest {
	
	public static int solution1(TreeNode root, int k){
		if(root == null) return -1;
		
//		if(solution1(root.left, k) + 1 + solution1(root.right, k) == k){
//			return root.data;
//		}
		
		if(root.left != null) return solution1(root.left, k);
		
		
		
		return -1;
	}
	
	public static int solutionWithMap(TreeNode root, int k){
		int counter = 0;
		Set<TreeNode> seen = new HashSet<TreeNode>();
		Stack<TreeNode> stack = new Stack<TreeNode>();
		stack.add(root);
		
		while(!stack.isEmpty()){
//			System.out.println(stack);
			TreeNode cur = stack.peek();
			
			if(cur.left == null || seen.contains(cur.left) ){
				seen.add(cur);
				stack.pop();
				counter++;
//				System.out.println(cur.data);
				if(k == counter) return cur.data;

				if(cur.right != null) stack.add(cur.right);
			}else{
				if(cur.left != null) stack.add(cur.left);
				
			}
			
		}
		
		return -1;
	}
	
	public static int solutionWithOutMap(TreeNode root, int k){
		Stack<TreeNode> stack = new Stack<TreeNode>();
		TreeNode cur = root;
		
		while(!stack.isEmpty() || cur != null){
			if(cur != null){
				stack.add(cur);
				cur = cur.left;
			}else{
				cur = stack.pop();
				k--;
				if(k == 0) return cur.data;
				
				cur = cur.right;
			}
		}
		
		return -1;
	}
	
	//TODO: If we can store rank in node, we can find in Log(N) time with binary search.
	public static int solutionWithModifiedNode(TreeNode root, int k){
	
		return -1;
	}
	
	
	public static void main(String[] args) {
		{
			System.out.println("==================");
			String[] nodes = {"6", "3", "9", "2", "5", "7", "10"};
			TreeNode root = TreeNode.buildTree(nodes);
			
			InOrder.inOrderIterativeSuboptimal(root);
			System.out.println(solutionWithMap(root, 3));
			System.out.println(solutionWithOutMap(root, 3));
			System.out.println(solutionWithMap(root, 5));
			System.out.println(solutionWithOutMap(root, 5));
			System.out.println(solutionWithMap(root, 7));
			System.out.println(solutionWithOutMap(root, 7));
			System.out.println(solutionWithMap(root, 9));
			System.out.println(solutionWithOutMap(root, 9));
			
		}
		
	}
	

}
