/**
 * 
 */
package codingpractise.tree;

/**
 * 
 * http://www.programcreek.com/2013/02/leetcode-balanced-binary-tree-java/
 * https://leetcode.com/problems/balanced-binary-tree/
 * @author arnavawasthi
 *
 */

public class BalancedBST {

	//TODO: We need to check height at every node. Is that the reason solution is also called recursively?
	//Don't you think there be many duplicate calculations?
	public static boolean solution(TreeNode root){
		if(root == null) return true;
		if(Math.abs(height(root.left) - height(root.right)) > 1){
			return false;
		}
		return solution(root.left) || solution(root.right);
	}
	
	/**
	 * @param left
	 * @return
	 */
	public static int height(TreeNode root) {
		if(root == null) return 0;
		
		return Math.max(height(root.left) + 1, height(root.right) + 1);
		
		//There is no need of helper. Refer above recursive implementation.
//		return heightHelper(root, 0);
	}
	
	private static int heightHelper(TreeNode root, int height){
		if(root == null) return height;
		
		return Math.max(heightHelper(root.left, height+1), heightHelper(root.right, height+1));
	}
	

	public static void main(String[] args) {
		{
			String[] nodes = {"5", "4", "8", "11", "#", "13", "4", "7", "2", "#", "#", "#", "#", "5", "1"};
			TreeNode root = TreeNode.buildTree(nodes);
			LevelOrder.levelOrderPrint(root);
			System.out.println("Height: "+height(root));
			System.out.println("isBalanced: "+solution(root));
		}
		{
			String[] nodes = {"5", "4", "8", "11", "#", "#", "4", "7", "2", "#", "#", "#", "#", "5", "1"};
			TreeNode root = TreeNode.buildTree(nodes);
			LevelOrder.levelOrderPrint(root);
			System.out.println("Height: "+height(root));
			System.out.println("isBalanced: "+solution(root));
		}
		{
			String[] nodes = {"5", "4", "8", "11", "#", "13", "4", "7", "2", "#", "#", "#", "#", "#", "1"};
			TreeNode root = TreeNode.buildTree(nodes);
			LevelOrder.levelOrderPrint(root);
			System.out.println("Height: "+height(root));
			System.out.println("isBalanced: "+solution(root));
		}
		{
			String[] nodes = {"1", "2", "3", "4", "5", "6", "7", "8"};
			TreeNode root = TreeNode.buildTree(nodes);
			LevelOrder.levelOrderPrint(root);
			System.out.println("Height: "+height(root));
			System.out.println("isBalanced: "+solution(root));
		}
	}
}
