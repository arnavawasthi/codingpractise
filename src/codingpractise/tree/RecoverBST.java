/**
 * 
 */
package codingpractise.tree;

import java.util.Stack;

/**
 * Ask is to build solution with O(1) additional space. 
 * http://www.programcreek.com/2014/05/leetcode-recover-binary-search-tree-java/
 * https://leetcode.com/problems/recover-binary-search-tree/
 * @author arnavawasthi
 *
 */
public class RecoverBST {

	/*
	 * If space used in stack for doing in-order traversal is not considered, it is an O(1) extra space implementation.
	 */
	public static void solution(TreeNode root){
		TreeNode first = null, second = null, prev = null;
		
		Stack<TreeNode> stack = new Stack<TreeNode>();
		TreeNode node = root;
		
		while(!stack.isEmpty() || node != null){
			if(node != null){
				stack.push(node);
				node = node.left;
			}else{
				node = stack.pop();
				
				
				//Core of finding two faulty nodes
				if(prev == null){
					prev = node;
				}else{
					if(prev.data > node.data){
						if(first == null){
							first = prev;
						}else{
							second = node;
							break;
						}
					}
				}
				
				prev = node;
				node = node.right;
			}
		}
		
//		System.out.println(first);
//		System.out.println(second);
		
		//correct the faulty nodes.
		int temp = first.data;
		first.data = second.data;
		second.data = temp;
		
		
	}
	
	public static void main(String[] args) {
		{
			String[] nodes = {"6", "3", "9", "2", "10", "7", "5"};
			TreeNode root = TreeNode.buildTree(nodes);
			
			InOrder.inOrderRecursive(root);
			System.out.println();
			
			solution(root);
			
			InOrder.inOrderRecursive(root);
			
			
			
			
		}
	}
}
