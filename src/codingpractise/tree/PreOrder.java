/**
 * 
 */
package codingpractise.tree;

import java.util.Stack;

/**
 * http://www.programcreek.com/2012/12/leetcode-solution-for-binary-tree-preorder-traversal-in-java/
 * @author arnavawasthi
 *
 */
public class PreOrder {

	
	public static void preOrderRecursive(TreeNode root){
		if(root == null) return;
		
		System.out.print(root.data+"->");
		
		preOrderRecursive(root.left);
		preOrderRecursive(root.right);
	}
	
	public static void preOrderIterative(TreeNode node){
		Stack<TreeNode> s = new Stack<TreeNode>();
		s.add(node);
		while(!s.isEmpty()){
			TreeNode temp = s.pop();
			System.out.print(temp.data+"->");
			
			if(temp.right != null) s.add(temp.right);
			if(temp.left != null) s.add(temp.left);
		}
		
		System.out.println();
	}
	
	
	public static void main(String[] args) {
		{
			String[] nodes = {"1", "2", "3", "4", "5", "6", "7", "8"};
			TreeNode root = TreeNode.buildTree(nodes);
			
			preOrderRecursive(root);
			System.out.println();
			preOrderIterative(root);
			
			
		}
		
	}
}
