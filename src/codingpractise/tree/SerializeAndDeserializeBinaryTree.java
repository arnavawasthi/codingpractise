/**
 * 
 */
package codingpractise.tree;

import java.util.LinkedList;
import java.util.Queue;

/**
 * http://www.programcreek.com/2014/05/leetcode-serialize-and-deserialize-binary-tree-java/
 * https://leetcode.com/problems/serialize-and-deserialize-binary-tree/
 * @author arnavawasthi
 *
 */
public class SerializeAndDeserializeBinaryTree {
	
	//TODO: It appends many #s for leaf nodes. Even though deserialize works on it, serialization takes ~2x space and can be avoided.
	public static String levelOrderSerialize(TreeNode root){
		
		Queue<TreeNode> q = new LinkedList<TreeNode>();
		q.add(root);

		StringBuilder output = new StringBuilder();
		while(!q.isEmpty()){
			TreeNode node = q.remove();
			if(node == null){
				output.append("#,");
				continue;
			}
			
			output.append(node.data+",");
			
			q.add(node.left);
			q.add(node.right);
		}
		output.deleteCharAt(output.length()-1);
		return output.toString();
	}
	
	/*
	 * Approach: First build array of TreeNode for given level order. Iterate through this array and build Tree.
	 */
	public static TreeNode levelOrderDeserialize(String levelOrder){
		String[] nodes = levelOrder.split(",");
		
		
		TreeNode[] treeNodes = new TreeNode[nodes.length];
		for(int i = 0; i < nodes.length; i++){
			treeNodes[i] = getNode(nodes[i]);
		}
		
		TreeNode root = treeNodes[0];
		int nullCount = 0;
		for(int i = 0; i < treeNodes.length; i++){
			int li = 2*(i - nullCount) + 1;
			int ri = 2*(i - nullCount) + 2;
			
			TreeNode node = treeNodes[i];
			if(node == null){
				nullCount++;
				continue;
			}
			
			if(li >= nodes.length || ri >= nodes.length) break;
			
			node.left = treeNodes[li];
			node.right = treeNodes[ri];
		}
		
		return root;
		
	}

	/**
	 * @param string
	 * @return
	 */
	private static TreeNode getNode(String string) {
		if(!string.equals("#")){
			return new TreeNode(Integer.parseInt(string));
		}
		return null;
	}
	
	public static void main(String[] args) {
		{
			String nodes = "5,4,8,11,#,13,4,7,2,#,#,5,1,#,#,#,#,#,#,#,#";
			TreeNode root = levelOrderDeserialize(nodes);
			LevelOrder.levelOrderPrint(root);
			PreOrder.preOrderIterative(root);
			String serialize = levelOrderSerialize(root);
			System.out.println(serialize);
			System.out.println(serialize.equals(nodes));
			
		}
	}

}
