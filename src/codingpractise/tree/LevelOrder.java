/**
 * 
 */
package codingpractise.tree;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * http://www.programcreek.com/2014/04/leetcode-binary-tree-level-order-traversal-java/
 * http://www.programcreek.com/2014/04/leetcode-binary-tree-level-order-traversal-ii-java/
 * @author arnavawasthi
 *
 */
public class LevelOrder {

	public static void levelOrderIterative(TreeNode root){
		Queue<TreeNode> q = new LinkedList<TreeNode>();
		q.add(root);
		
		while(!q.isEmpty()){
			TreeNode temp = q.remove();
			System.out.print(temp.data+"->");
			if(temp.left != null)
				q.add(temp.left);
			
			if(temp.right != null)
				q.add(temp.right);
			
		}
		System.out.println();
		
	}
	
	public static void levelOrderPrint(TreeNode root){
		Queue<TreeNode> q = new LinkedList<TreeNode>();
		q.add(root);
		q.add(null);
		
		while(!q.isEmpty()){
			TreeNode temp = q.remove();
			if(temp != null){
				System.out.print(temp.data+"->");
				if(temp.left!=null) q.add(temp.left);
				if(temp.right!=null) q.add(temp.right);
				
			}else{
				if(!q.isEmpty()){
					System.out.println("");
					q.add(null);
				}
			}
		}
		System.out.println();
	}
	
	public static void reverseLevelOrderPrint(TreeNode root){
		Queue<TreeNode> q = new LinkedList<TreeNode>();
		Stack<TreeNode> s = new Stack<TreeNode>();
		
		q.add(root);
		q.add(null);
		
		while(!q.isEmpty()){
			TreeNode cur = q.remove();
			if(cur == null){
				if(!q.isEmpty()) q.add(cur);
			}else{
				if(cur.right != null) q.add(cur.right);
				if(cur.left != null) q.add(cur.left);
			}
			s.add(cur);
		}
		
		while(!s.isEmpty()){
			TreeNode cur = s.pop();
			if(cur == null){
				System.out.println();
			}else{
				System.out.print(cur.data+"->");
			}
		}
	}
	
	public static void main(String[] args) {
		{
			String[] nodes = {"1", "2", "3", "4", "5", "6", "7", "8"};
			TreeNode root = TreeNode.buildTree(nodes);
			
			levelOrderIterative(root);
			levelOrderPrint(root);
			reverseLevelOrderPrint(root);
			
			
		}
		
	}
}
