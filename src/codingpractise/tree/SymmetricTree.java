/**
 * 
 */
package codingpractise.tree;

/**
 * http://www.programcreek.com/2014/03/leetcode-symmetric-tree-java/
 * https://leetcode.com/problems/symmetric-tree/
 * @author arnavawasthi
 *
 */
public class SymmetricTree {

	public static boolean solution(TreeNode root){
		return helper(root, root);
	}

	/**
	 * @param root
	 * @param root2
	 * @return
	 */
	private static boolean helper(TreeNode left, TreeNode right) {
		// TODO Auto-generated method stub
		if(left == null && right == null) return true;
		if(left == null || right == null) return false;
		
		
		if(left.data != right.data){
			return false;
		}
		
		/*
		 * There is no need going down one level and check values. See above condition, 
		 * we can just check values on the same level and recursive calls will check values
		 * in the next level and so.
		 */
//		if(left.left != null && left.right != null && right.left != null && right.right != null){
//			if(left.left.data == right.right.data && left.right.data == right.left.data){
//				return true;
//			}else{
//				return false;
//			}
//			
//		}
		
		return helper(left.left, right.right) && helper(left.right, right.left);
	}
	
	public static void main(String[] args) {
		{
			String[] nodes = {"5", "4", "8", "11", "#", "13", "4", "7", "2", "#", "#", "#", "#", "5", "1"};
			TreeNode root = TreeNode.buildTree(nodes);
			LevelOrder.levelOrderPrint(root);
			System.out.println(solution(root));
			
		}
		
		{
			String[] nodes = {"5", "4", "4", "3", "1", "1", "3", "6", "2", "5", "9", "9", "5", "2", "6"};
			TreeNode root = TreeNode.buildTree(nodes);
			LevelOrder.levelOrderPrint(root);
			System.out.println(solution(root));
			
		}
	}
}
