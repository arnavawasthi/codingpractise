/**
 * 
 */
package codingpractise.tree;

/**
 * http://www.programcreek.com/2012/12/leetcode-validate-binary-search-tree-java/
 * @author arnavawasthi
 *
 */
public class ValidateBinarySearchTree {
	
	
	//Tricky solution
	public static boolean isValid(TreeNode root){
		
		return helper(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
		
	}
	
	/**
	 * @param root
	 * @param data
	 * @param data2
	 * @return
	 */
	private static boolean helper(TreeNode root, int min, int max) {
		if(root == null) return true;
		
		return root.data > min && root.data < max && helper(root.left, min, root.data) && helper(root.right, root.data, max);
		
	}
	
	/*
	 * In order traversal outputs nodes in sorted order. Utilize this property.
	 * Spoiler alert: Logically it looks correct, but it doesn't work. 
	 * Reason: In the call stack, prev call returns true/false to next call. But next call requires prev value.
	 * For while backtracking, it works with Integer.MIN_VALUE as prev. Hence it fails.
	 * 
	 * Proof: Uncomment sysout in inOrderHelper.
	 * 
	 * TODO: Thought: If recursive doesn't work, iterative in-order should work.
	 */
	public static boolean isValidInOrder(TreeNode root){
		return inOrderHelper(root, Integer.MIN_VALUE);
	}
	
	private static boolean inOrderHelper(TreeNode root, int prev){
		if(root == null) return true;
		if(!inOrderHelper(root.left, prev)) return false;
		
//		System.out.println(String.format("Root: %d :: Prev: %d", root.data, prev));
		if(root.data < prev){
			return false;
		}
		prev = root.data;
		
		if(!inOrderHelper(root.right, prev)) return false;
		
		return true;
		
		
	}

	public static void main(String[] args) {
		{
			System.out.println("==================");
			String[] nodes = {"6", "3", "9", "2", "8", "7", "10"};
			TreeNode root = TreeNode.buildTree(nodes);
			
			System.out.println(isValid(root));
			System.out.println(isValidInOrder(root));
		
		}
		
		{
			System.out.println("==================");
			String[] nodes = {"6", "3", "9", "2", "5", "7", "10"};
			TreeNode root = TreeNode.buildTree(nodes);
			
			System.out.println(isValid(root));
			System.out.println(isValidInOrder(root));
		}
		
		
		{
			System.out.println("==================");
			String[] nodes = {"6", "3", "9", "2", "4", "7", "10"};
			TreeNode root = TreeNode.buildTree(nodes);
			
			System.out.println(isValid(root));
			System.out.println(isValidInOrder(root));
		}
		
		
		{
			System.out.println("==================");
			String[] nodes = {"6", "3", "9", "2", "4", "7", "10", "1", "4"};
			TreeNode root = TreeNode.buildTree(nodes);
			
			System.out.println(isValid(root));
			System.out.println(isValidInOrder(root));
		}
			
			
	}

}
