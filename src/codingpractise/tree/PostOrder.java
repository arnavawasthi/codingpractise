/**
 * 
 */
package codingpractise.tree;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * http://www.programcreek.com/2012/12/leetcode-solution-of-iterative-binary-tree-postorder-traversal-in-java/
 * @author arnavawasthi
 *
 */
public class PostOrder {

	
	public static void postOrderRecursive(TreeNode root){
		if(root == null){
			return;
		}
		
		postOrderRecursive(root.left);
		postOrderRecursive(root.right);
		
		System.out.print(root.data+"->");
		
	}
	
	public static void postOrderIterative(TreeNode root){
		Map<TreeNode, Boolean> seen = new HashMap<TreeNode, Boolean>();
		Stack<TreeNode> stack = new Stack<TreeNode>();
		
		stack.add(root);
		Integer counter = 0;
		while(!stack.isEmpty()){
			TreeNode temp = stack.peek();
			if((temp.right == null || seen.containsKey(temp.right)) && (temp.left == null || seen.containsKey(temp.left))){
				stack.pop();
				seen.put(temp, true);
				System.out.print(temp.data+"->");
				
				if(counter > 20) break;
				counter++;
			}else{
				if(temp.right != null) stack.add(temp.right);
				
				if(temp.left != null) stack.add(temp.left);
				
				if(counter > 20) break;
				counter++;
			}
			
		}
		
		System.out.println();
	}
	

	
	public static void main(String[] args) {
		{
			String[] nodes = {"1", "2", "3", "4", "5", "6", "7", "8"};
			TreeNode root = TreeNode.buildTree(nodes);
			
			postOrderRecursive(root);
			System.out.println();
			postOrderIterative(root);
			
			
		}
		
	}
}
