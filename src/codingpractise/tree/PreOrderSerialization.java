/**
 * 
 */
package codingpractise.tree;

import java.util.Stack;

/**
 * http://www.programcreek.com/2015/01/leetcode-verify-preorder-serialization-of-a-binary-tree-java/
 * https://leetcode.com/problems/verify-preorder-serialization-of-a-binary-tree/
 * @author arnavawasthi
 *
 */
public class PreOrderSerialization {

	
	public static boolean solution(String preorder){
		System.out.println(preorder);
		
		String[] nodes = preorder.split(",");
		Stack<String> stack = new Stack<String>();
		
		for(String node: nodes){
			stack.push(node);
		}

		Stack<String> stack2 = new Stack<String>();
		
		while(stack.size() > 1){
			String node = stack.pop();

			if("#".equals(node)){
				stack2.push(node);
			}else{
				if(stack2.size() < 2){
					return false;
				}else{
					stack.push("#");
					stack2.pop();
					stack2.pop();
				}
			}
			
			/*
			 * 1st method: keep while condition as !stack.isEmpty() and keep checking it.
			 */
//			if(stack.size() == 1 && stack.peek().equals("#") && stack2.isEmpty()){
//				return true;
//			}
		}
		
		/*
		 * 2nd method: keep while considtion as stack.size() > 1
		 */
		if(!stack.peek().equals("#") && stack2.size() == 2){
			return true;
		}
		
		
		return false;
	}
	
	public static void main(String[] args) {
		System.out.println(solution("9,3,4,#,#,1,#,#,2,#,6,#,#"));
		System.out.println(solution("1,#"));
		System.out.println(solution("9,#,#,1"));
		System.out.println(solution("9,#,#,#"));
		System.out.println(solution("9,#,#"));
	}
}
