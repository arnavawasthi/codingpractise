/**
 * 
 */
package codingpractise.tree;


/**
 * @author arnavawasthi
 *
 */
public class TreeNode {

	int data;
	TreeNode left;
	TreeNode right;
	
	TreeNode(int data){
		this.data = data;
	}
	
	public String toString(){
		return data+"";
	}
	
	public static TreeNode buildTree(String[] nodes){
		
		TreeNode[] treeNodes = new TreeNode[nodes.length];
		for(int i = 0; i < nodes.length; i++){
			if("#".equals(nodes[i])){
				treeNodes[i] = null;
			}else{
				treeNodes[i] = new TreeNode(parseInt(nodes[i]));
			}
		}
		
		TreeNode root = treeNodes[0];
		for(int i = 0; i < treeNodes.length/2; i++){
			int li = 2*i + 1;
			int ri = 2*i + 2;
			
			TreeNode node = treeNodes[i];
			if(node == null) continue;
			
			if(li < nodes.length){
				node.left = treeNodes[li];
			}
			if(ri < nodes.length){
				node.right = treeNodes[ri];
			}
		}
		
		return root;
	}
	
	/**
	 * @param string
	 * @return
	 */
	private static int parseInt(String string) {
		return Integer.parseInt(string);
	}
	
	public static void main(String[] args) {
		{
			String[] nodes = {"1", "2", "3", "4", "5", "6", "7", "8"};
			TreeNode root = TreeNode.buildTree(nodes);
			
			
			
		}
		
	}
}
