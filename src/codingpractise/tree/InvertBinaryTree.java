/**
 * 
 */
package codingpractise.tree;

/**
 * https://leetcode.com/problems/invert-binary-tree/
 * http://www.programcreek.com/2014/06/leetcode-invert-binary-tree-java/
 * @author arnavawasthi
 *
 */
public class InvertBinaryTree {

	public static void solution(TreeNode root){
		if(root == null) return;
		
		TreeNode temp = root.right;
		root.right = root.left;
		root.left = temp;
		
		solution(root.right);
		solution(root.left);
	}
	
	public static void solutionBottomUp(TreeNode root){
		if(root == null) return;
		solution(root.right);
		solution(root.left);
		
		TreeNode temp = root.right;
		root.right = root.left;
		root.left = temp;
		
	}
	
	//TODO: 
	public static void solutionIterative(TreeNode root){
		
	}
	
	public static void main(String[] args) {
		{
			String[] nodes = {"1", "2", "3", "4", "5", "6", "7", "8"};
			TreeNode root = TreeNode.buildTree(nodes);
			
			LevelOrder.levelOrderPrint(root);
			solution(root);
			LevelOrder.levelOrderPrint(root);
			solutionBottomUp(root);
			LevelOrder.levelOrderPrint(root);
			
		}
		
	}
}
