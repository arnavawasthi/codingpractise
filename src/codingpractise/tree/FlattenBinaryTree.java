/**
 * 
 */
package codingpractise.tree;

import java.util.Stack;

/**
 * http://www.programcreek.com/2013/01/leetcode-flatten-binary-tree-to-linked-list/
 * https://leetcode.com/problems/flatten-binary-tree-to-linked-list/
 * @author arnavawasthi
 *
 */
public class FlattenBinaryTree {

	public static void solution(TreeNode root){
		Stack<TreeNode> stack = new Stack<TreeNode>();
		
		TreeNode prev = null;
		stack.add(root);
		while(!stack.isEmpty()){
			TreeNode cur = stack.pop();
			
			if(cur.right != null) stack.add(cur.right);
			
			if(cur.left != null) stack.add(cur.left);
			
			if(prev == null){
				prev = cur;
			}else{
				prev.right = cur;
				//Setting left is also required and equally important. 
				//Comment it out and see the effect
				prev.left = null;
				prev = cur;
			}
			
		}
		prev.right = null;
	}
	
	public static void main(String[] args) {
		{
			String[] nodes = {"1", "2", "3", "4", "5", "6", "7", "8"};
			TreeNode root = TreeNode.buildTree(nodes);
			
			PreOrder.preOrderRecursive(root);
			System.out.println();
			PreOrder.preOrderIterative(root);
			solution(root);
			PreOrder.preOrderRecursive(root);
			System.out.println();
			PreOrder.preOrderIterative(root);
			
			
		}
	}
}
