/**
 * 
 */
package codingpractise.tree;

import java.util.LinkedList;
import java.util.Queue;

/**
 * http://www.programcreek.com/2013/01/leetcode-path-sum/
 * @author arnavawasthi
 *
 */
public class PathSum {

	public static boolean solution(TreeNode root, Integer target){
		
		System.out.print(String.format("Sum path for %d exists: ", target));
		Queue<TreeNode> nodes = new LinkedList<TreeNode>();
		Queue<Integer> sums = new LinkedList<Integer>();
		
		nodes.add(root);
		sums.add(root.data);
		
		while(!nodes.isEmpty()){
			TreeNode cur = nodes.poll();
			Integer sum = sums.poll();
			
			if(cur.left == null && cur.right == null && sum == target){
				return true;
			}
			
			if(cur.left != null){
				nodes.add(cur.left);
				sums.add(sum + cur.left.data);
			}
			
			if(cur.right != null){
				nodes.add(cur.right);
				sums.add(sum + cur.right.data);
			}
		}
		
		return false;
	}
	
	public static boolean solutionRecursive(TreeNode root, Integer target){
		
		if(root == null){
			if(target == 0)
				return true; 
			else
				return false;
		}
		
		Integer residual = target - root.data;
		
		if(solutionRecursive(root.left, residual)) return true;
			
		if(solutionRecursive(root.right, residual)) return true;

		
		return false;
	}
	
	
	public static boolean solutionRecursive2(TreeNode root, Integer target){
		
		if(root == null) return false;

		//leaf node
		if(root.data == target && root.left == null && root.right == null) return true;
		
		Integer residual = target - root.data;
		
		return solutionRecursive2(root.left, residual) || solutionRecursive2(root.right, residual);
		
	}
	
	public static void main(String[] args) {
		{
			String[] nodes = {"1", "2", "3", "4", "5", "6", "7", "8"};
			TreeNode root = TreeNode.buildTree(nodes);
			
			System.out.println(solution(root, 8));
			System.out.println(solutionRecursive(root, 8));
			System.out.println(solutionRecursive2(root, 8));
			System.out.println(solution(root, 10));
			System.out.println(solutionRecursive(root, 10));
			System.out.println(solutionRecursive2(root, 10));
			System.out.println(solution(root, 11));
			System.out.println(solutionRecursive(root, 11));
			System.out.println(solutionRecursive2(root, 11));
			System.out.println(solution(root, 15));
			System.out.println(solutionRecursive(root, 15));
			System.out.println(solutionRecursive2(root, 15));
			System.out.println(solution(root, 18));
			System.out.println(solutionRecursive(root, 18));
			System.out.println(solutionRecursive2(root, 18));
			System.out.println(solution(root, 9));
			System.out.println(solutionRecursive(root, 9));
			System.out.println(solutionRecursive2(root, 9));
			System.out.println(solution(root, 12));
			System.out.println(solutionRecursive(root, 12));
			System.out.println(solutionRecursive2(root, 12));
			System.out.println(solution(root, 13));
			System.out.println(solutionRecursive(root, 13));
			System.out.println(solutionRecursive2(root, 13));
		}
	}
	
	
}
