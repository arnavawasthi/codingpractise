/**
 * 
 */
package codingpractise.tree;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Programcreek has a recursive solution. My solution uses iterative solution implemented in @PathSum.java
 * http://www.programcreek.com/2014/05/leetcode-path-sum-ii-java/
 * @author arnavawasthi
 *
 */
public class PathSum2 {

	public static List<List<TreeNode>> solution(TreeNode root, int target){
		Queue<List<TreeNode>> paths = new LinkedList<List<TreeNode>>();
		Queue<Integer> qsum = new LinkedList<Integer>();
		Queue<TreeNode> q = new LinkedList<TreeNode>();
		
		
		List<TreeNode> path = new LinkedList<TreeNode>();
		path.add(root);
		paths.add(path);
		q.add(root);
		qsum.add(root.data);
		List<TreeNode> list = new LinkedList<TreeNode>();
		list.add(root);
		
		List<List<TreeNode>> output = new LinkedList<List<TreeNode>>();
		
		while(!q.isEmpty()){
			TreeNode cur = q.remove();
			Integer sum = qsum.remove();
			List<TreeNode> curPath = paths.remove();
//			System.out.println(String.format("CurSum: %d, CurData: %d", sum, cur.data));
//			System.out.println(String.format("Node: %s, Left: %s, Right: %s", cur, cur.left, cur.right));
			if(cur.left == null && cur.right == null){
				if(sum == target){
					output.add(curPath);
				}
			}
			
			if(cur.left != null){
				List<TreeNode> leftList = new LinkedList<TreeNode>(curPath);
				leftList.add(cur.left);
				paths.add(leftList);
				q.add(cur.left);
				qsum.add(sum + cur.left.data);
				
			}
			
			if(cur.right != null){
				List<TreeNode> rightList = new LinkedList<TreeNode>(curPath);
				rightList.add(cur.right);
				paths.add(rightList);
				q.add(cur.right);
				qsum.add(sum + cur.right.data);
			}
			
			
		}
		
		
		return output;
	}
	
	public static void main(String[] args) {
		{
			String[] nodes = {"5", "4", "8", "11", "#", "13", "4", "7", "2", "#", "#", "#", "#", "5", "1"};
			TreeNode root = TreeNode.buildTree(nodes);
			
//			LevelOrder.levelOrderPrint(root);
			
			System.out.println(solution(root, 22));
		}
	}
}
