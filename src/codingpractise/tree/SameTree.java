/**
 * 
 */
package codingpractise.tree;

/**
 * http://www.programcreek.com/2012/12/check-if-two-trees-are-same-or-not/
 * https://leetcode.com/problems/same-tree/
 * @author arnavawasthi
 *
 */
public class SameTree {
	
	public static boolean solution(TreeNode s, TreeNode t){
		if(s == null && t == null) return true;
		if(s == null || t == null) return false;
		
		if(s.data != t.data) return false;
		
		return solution(s.right, t.right) && solution(s.left, t.left);
	}
	
	public static void main(String[] args) {
		{
			String[] nodes = {"5", "4", "8", "11", "#", "13", "4", "7", "2", "#", "#", "#", "#", "5", "1"};
			TreeNode root = TreeNode.buildTree(nodes);
			TreeNode root1 = TreeNode.buildTree(nodes);
//			LevelOrder.levelOrderPrint(root);
			System.out.println(solution(root, root1));
			
		}
		
		{
			String[] nodes = {"5", "4", "8", "11", "#", "13", "4", "7", "2", "#", "#", "#", "#", "5", "1"};
			TreeNode root = TreeNode.buildTree(nodes);
			nodes[5] = "1";
			TreeNode root1 = TreeNode.buildTree(nodes);
//			LevelOrder.levelOrderPrint(root);
			System.out.println(solution(root, root1));
			
		}
	}

}
