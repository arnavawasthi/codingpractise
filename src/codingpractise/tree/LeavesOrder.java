/**
 * 
 */
package codingpractise.tree;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * http://www.programcreek.com/2014/07/leetcode-find-leaves-of-binary-tree-java/
 * @author arnavawasthi
 *
 */
public class LeavesOrder {
	
	public static List<List<Integer>> solution(TreeNode root){
		List<List<Integer>> list = new ArrayList<List<Integer>>();
		helper(list, root);
		return list;
	}
	
	public static int helper(List<List<Integer>> list, TreeNode node){
		if(node == null) return -1;
		
		int left = helper(list, node.left);
		int right = helper(list, node.right);
		int curr = Math.max(left, right) + 1;
		
		/*
		 * Short version of same is below. Remember list.add(index, value) doesn't replace value at index. It shifts current value.
		 * 
		 */
//		List<Integer> l = null;
//		if(list.size() > curr){
//			l = list.get(curr);
//			l.add(node.data);
//			list.set(curr, l);
//		}else{
//			l = new ArrayList<Integer>();
//			l.add(node.data);
//			list.add(l);
//		}
		
		// the first time this code is reached is when curr==0,
	    //since the tree is bottom-up processed.
	    if(list.size()<=curr){
	        list.add(new ArrayList<Integer>());
	    }
	 
	    list.get(curr).add(node.data);
		
		return curr;
	}
	
	public static void main(String[] args) {
		{
			String[] nodes = {"5", "4", "8", "11", "#", "13", "4", "7", "2", "#", "#", "#", "#", "5", "1"};
			TreeNode root = TreeNode.buildTree(nodes);
			LevelOrder.levelOrderPrint(root);
			System.out.println(solution(root));
			
		}
	}

}
