/**
 * 
 */
package codingpractise.tree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * http://www.programcreek.com/2014/05/leetcode-sum-root-to-leaf-numbers-java/
 * https://leetcode.com/problems/sum-root-to-leaf-numbers/
 * @author arnavawasthi
 *
 */
public class SumRootToLeaf {

	/*
	 * Approach is to first find all paths and then calculate sum
	 * Instead of all Queue of paths, you can build a string and finally parse and add
	 */
	public static int solution(TreeNode root){
		Queue<TreeNode> q = new LinkedList<TreeNode>();
		Queue<List<Integer>> paths = new LinkedList<List<Integer>>();
		
		q.add(root);
		List<Integer> p = new ArrayList<Integer>();
		p.add(root.data);
		paths.add(p);
		
		while(!q.isEmpty()){
			TreeNode node = q.remove();
			List<Integer> path = paths.remove();
			
			if(node.left != null){
				List<Integer> leftPath = new ArrayList<Integer>(path);
				leftPath.add(node.left.data);
				paths.add(leftPath);
			}
			
			if(node.right != null){
				List<Integer> rightPath = new ArrayList<Integer>(path);
				rightPath.add(node.right.data);
				paths.add(rightPath);
			}
		}
		System.out.println(paths);
		int sum = 0;
		for(List<Integer> path: paths){
			int l = path.size();
			for(int i = 0; i < l; i++){
				sum += path.get(i)*Math.pow(10, l-i-1);
			}
		}
		return sum;
	}
	
	public static int solution1(TreeNode root){
		Queue<TreeNode> q = new LinkedList<TreeNode>();
		Queue<StringBuilder> paths = new LinkedList<StringBuilder>();
		
		q.add(root);
		StringBuilder sb = new StringBuilder();
		sb.append(root.data);
		paths.add(sb);
		
		while(!q.isEmpty()){
			TreeNode node = q.remove();
			StringBuilder path = paths.remove();
			
			if(node.left != null){
				StringBuilder leftPath = new StringBuilder(path);
				leftPath.append(node.left.data);
				paths.add(leftPath);
			}
			
			if(node.right != null){
				StringBuilder rightPath = new StringBuilder(path);
				rightPath.append(node.right.data);
				paths.add(rightPath);
			}
		}
		System.out.println(paths);
		int sum = 0;
		for(StringBuilder path: paths){
			sum += Integer.parseInt(path.toString());
		}
		return sum;
	}
	
	public static void main(String[] args) {
		{
			String[] nodes = {"1", "2", "3"};
			TreeNode root = TreeNode.buildTree(nodes);
			LevelOrder.levelOrderPrint(root);
			System.out.println(solution(root));
			System.out.println(solution1(root));
			
		}
	}
	
}
