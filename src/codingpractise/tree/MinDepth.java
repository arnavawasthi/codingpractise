/**
 * 
 */
package codingpractise.tree;

/**
 * http://www.programcreek.com/2013/02/leetcode-minimum-depth-of-binary-tree-java/
 * https://leetcode.com/problems/minimum-depth-of-binary-tree/
 * @author arnavawasthi
 *
 */
public class MinDepth {
	
	public static int minDepthRecursive(TreeNode root){
		if(root.left == null && root.right == null) return 1;
		
		int leftDepth = Integer.MAX_VALUE;
		if(root.left != null){
			leftDepth = minDepthRecursive(root.left) + 1;
		}
		int rightDepth = Integer.MAX_VALUE;
		if(root.right != null){
			rightDepth = minDepthRecursive(root.right) + 1;
		}
		
		return Math.min(leftDepth, rightDepth);
	}
	
	//TODO
	public static int minDepthIterative(TreeNode root){
		
		return -1;
	}
	
	/*
	 * It gives wrong result, because it doesn't calculate root to leaf height. If for any node 
	 * right node is null, it will terminate right sub-tree height there itself. 
	 * 
	 */
	public static int minDepthWrong(TreeNode root){
		if(root == null) return 0;
		
		return Math.min(minDepthWrong(root.left) + 1, minDepthWrong(root.right) + 1);
	}
	
	public static void main(String[] args) {
		{
			String[] nodes = {"5", "4", "8", "11", "#", "13", "4", "7", "2", "#", "#", "#", "#", "5", "1"};
			TreeNode root = TreeNode.buildTree(nodes);
			LevelOrder.levelOrderPrint(root);
			System.out.println("MinHeightWrong: "+minDepthWrong(root));
			System.out.println("MinHeight: "+minDepthRecursive(root));
			System.out.println("MaxHeight: "+BalancedBST.height(root));
			
			
		}
	}

}
