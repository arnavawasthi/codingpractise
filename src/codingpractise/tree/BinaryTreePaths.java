/**
 * 
 */
package codingpractise.tree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * http://www.programcreek.com/2014/05/leetcode-binary-tree-paths-java/
 * https://leetcode.com/problems/binary-tree-paths/
 * @author arnavawasthi
 *
 */
public class BinaryTreePaths {

	public static List<List<TreeNode>> solutionIterative(TreeNode root){
		Queue<TreeNode> q = new LinkedList<TreeNode>();
		Queue<List<TreeNode>> paths = new LinkedList<List<TreeNode>>();
		
		q.add(root);
		List<TreeNode> path = new LinkedList<TreeNode>();
		path.add(root);
		paths.add(path);
		
		List<List<TreeNode>> output = new LinkedList<List<TreeNode>>();
		while(!q.isEmpty()){
			TreeNode cur = q.remove();
			List<TreeNode> curPath = paths.remove();
			
			if(cur.left == null && cur.right == null){
				output.add(curPath);
				continue;
			}
			
			if(cur.left != null){
				q.add(cur.left);
				List<TreeNode> leftPath = new LinkedList<TreeNode>(curPath);
				leftPath.add(cur.left);
				
				paths.add(leftPath);
			}
			
			if(cur.right != null){
				q.add(cur.right);
				List<TreeNode> rightPath = new LinkedList<TreeNode>(curPath);
				rightPath.add(cur.right);
				
				paths.add(rightPath);
			}
			
		}
		
		
		return output;
		
	}
	
	public static void solutionRecursive(TreeNode root){
		List<TreeNode> path = new ArrayList<TreeNode>();
		path.add(root);
		recursiveHelper(root, path);
		
	}
	
	/**
	 * path.remove is magic line here.
	 * @param root
	 * @param path
	 */
	private static void recursiveHelper(TreeNode root, List<TreeNode> path) {
		if(root.left == null && root.right == null){
			System.out.println(path);
			//When path is printed, remove the last node.
			path.remove(path.size()-1);
			return;
		}
		
		if(root.left != null){
			path.add(root.left);
			recursiveHelper(root.left, path);
		}
		
		if(root.right != null){
			path.add(root.right);
			recursiveHelper(root.right, path);
		}
		//at this point, we are going back. For this node, there is no right node. So when going back, remove the last node.
		path.remove(path.size()-1);
	}

	public static void main(String[] args) {
		{
			String[] nodes = {"5", "4", "8", "11", "9", "13", "4", "7", "2", "#", "#", "#", "#", "5", "1"};
			TreeNode root = TreeNode.buildTree(nodes);
			LevelOrder.levelOrderPrint(root);
			System.out.println(solutionIterative(root));
			solutionRecursive(root);
		}
	}
	
}
