package codingpractise.interview;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 */

/**http://collabedit.com/m4nsx
 * @author arnavawasthi
 *
 */
public class LinkedIN {

}

/* This class will be given a list of words (such as might be tokenized
 * from a paragraph of text), and will provide a method that takes two
 * words and returns the shortest distance (in words) between those two
 * words in the provided text.
 * Example:
 *   WordDistanceFinder finder = new WordDistanceFinder(Arrays.asList("the", "quick", "brown", "fox", "quick"));
 *   assert(finder.distance("fox","the") == 3);
 *   assert(finder.distance("quick", "fox") == 1);
 *
 * "quick" appears twice in the input. There are two possible distance values for "quick" and "fox":
 *     (3 - 1) = 2 and (4 - 3) = 1.
 * Since we have to return the shortest distance between the two words we return 1.
 */
class WordDistanceFinder {
    Map<String, List<Integer>> map = new HashMap<String, List<Integer>>();
    
    public WordDistanceFinder (List<String> words) {
        // implementation here
        
        int counter = 0;
        for(String s: words){
            counter++;
            if(map.containsKey(s)){
                List<Integer> l = map.get(s);
                l.add(counter);
                map.put(s, l);
            }else{
                List<Integer> l = new ArrayList<Integer>();
                l.add(counter);
                map.put(s, l);
            }
        }
    }
    public int distance (String wordOne, String wordTwo) {
        // implementation here
        List<Integer> i = map.get(wordOne);
        List<Integer> j = map.get(wordTwo);
        if(i == null || j == null) return -1;
        
        //1,
        //10, 20        
        
        int il = i.size();
        int jl = j.size();
        int min = Integer.MAX_VALUE;
        int a = 0, b = 0;
        
        while(a < il && b < jl){
            int t1 = i.get(a);
            int t2 = j.get(b);    
            if(t1 < t2){
                  
                  a++;
                 
                 
            } else if(t2 < t1){
                  //min = Math.min(min, t1 - t2);
                  b++;
                 
            }
            
            min = Math.min(min, Math.abs(t2 - t1));
            
        }
        
        int temp = Math.abs(i.get(il - 1) - j.get(jl - 1));
        min = Math.min(min, temp);
        
        return min;
        
        
    }
    
}
