/**
 * 
 */
package codingpractise.interview;

/**
 * @author arnavawasthi
 *
 */
/*
flatten([1, [2, 3], [4, [5, 6, 7], 8], 9, 0]) => [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
 */

import java.util.*;

public class FlattenList {

	public List<Integer> flatten(List list) {

		List<Integer> output = new ArrayList<Integer>();
		helper(list, output);

		return output;
	}

	private void helper(List list, List<Integer> output) {
		if (list == null)
			return;

		for (Object obj : list) {
			if (obj instanceof Integer) {
				output.add((Integer) obj);
			} else {
				helper((List) obj, output);
			}
		}

	}

	public static void main(String[] args) {

		FlattenList sol = new FlattenList();

		List l = new ArrayList();
		l.add(1);

		List l1 = new ArrayList();
		l1.add(2);
		l1.add(3);

		l.add(l1);
		List l2 = new ArrayList();
		l2.add(4);
		l2.add(8);
		List l3 = new ArrayList();
		l3.add(5);
		l3.add(6);
		l3.add(7);
		l2.add(l3);
		l.add(l2);

		System.out.println(sol.flatten(l));

	}

}
