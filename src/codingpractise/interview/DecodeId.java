/**
 * 
 */
package codingpractise.interview;

/**
 * @author arnavawasthi
 *
 */

/*
Often, we want to encode raw IDs in our database by hiding them behind some 2-way decodeable hash. So, a URL which would have at one time been:

https://www.twitch.com/users/848662

becomes

https://www.twitch.com/users/kljJJ324hjkS_

We decode the ID kljJJ324hjkS_ to 848662 on our backend and serve the relevant content. At some point, we start getting 404 errors from clients requesting a certain URL of the form

https://www.twitch.com/users/kljJJ324hjkS_

This can happen if certain clients, email services, or url shorteners "sanitize" the url. Unfortunately, this change breaks decoding and the resource cannot be found.
To assess how big of a deal this is, we may want to recover the IDs of the targets that were 404ing.

Your task:
Given a method decode(testEncStr) which will return the decoded int id if testEncStr is decodeable or will throw an exception or return null (depending on the language) if not, implement a new method decodeFind(String badEncStr) which takes a string and returns the decoded int id.
*/

class DecodeId {
  public static class BadIdException extends Exception {
    private static final long serialVersionUID = 1;
    public BadIdException(String message) {
      super(message);
    }
  }
  
  
  public static void main(String[] args) {
    System.out.println("Decoded to id: " + decodeFind("a_b_C_d!!JKF"));
  }
 
  // Black box. Can't modify. You get this.
  private static int decode(String testEncStr) throws BadIdException {
    //System.out.println("Testing " + testEncStr);
    if (testEncStr.equals("A_b_c_d!!jkf")) {
      return 848662;
    } else {
      throw new BadIdException(testEncStr + " not found");
    }
  }

  // you implement this
  public static int decodeFind(String badEncStr) 
  {
      
      char[] cs = badEncStr.toCharArray();
      
        
            
      return decodeFindHelper(cs, 0);
  }
  
  private static int decodeFindHelper(char[] str, int index){
    
    if(index >= str.length) return 0;
    
    int lower = 0;
    int upper = 0;
    
    str[index] = Character.toLowerCase(str[index]);
    try{
      
      lower = decode(String.valueOf(str));
      
    }catch(BadIdException e){
  
      lower = decodeFindHelper(str, index+1);
      
    }
    
    if(lower > 0){
    	return lower;
    }

    str[index] = Character.toUpperCase(str[index]);
    try{
      
      upper = decode(String.valueOf(str));
    }catch(BadIdException e){
      upper = decodeFindHelper(str, index+1);
    }
    
//    if(lower > 0){
//      return lower; 
//    }
    
    if(upper > 0){
      return upper;
    }
    return 0;
    
    
      
    
    
  }
}