package codingpractise.interview;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * 
 */

/**
 * @author arnavawasthi
 *
 */
public class Solution {

    static String[] layout(String[] lines) {
        if(lines == null || lines.length == 0) return null;
    	
    	Map<Integer, StringBuilder>  output = new TreeMap<Integer, StringBuilder>();
    	Map<Integer, Integer> lineStartsMap = new HashMap<Integer, Integer>();
    	
    	int boxes = lines.length;//Integer.parseInt(lines[0]);
    	
    	
    	//if(boxes != lines.length - 1){
    		//return null;
    	//}
    	
    	
    	for(int i = 0; i < boxes; i++){
    		String line = lines[i];
    		String[] tokens = line.split(" ");
    		int width = Integer.parseInt(tokens[0]);
    		int x = Integer.parseInt(tokens[1]);
    		StringBuilder offset = new StringBuilder();
    		for(int k = 0; k < x; k++){
    			offset.append(" ");
    		}
    		int y = Integer.parseInt(tokens[2]);
    		boolean newLine = false;
    		
    		for(int j = 3; j < tokens.length; j++){
    			Integer lineStarts = lineStartsMap.get(y);
    			StringBuilder outputLine = output.get(y);
    			String token = tokens[j];
    			if(outputLine == null){
    				outputLine = new StringBuilder();
    				outputLine.append(offset);
    				lineStartsMap.put(y, x);
    			}else{
    				if(lineStarts != null && lineStarts > x + token.length() && newLine){
    					outputLine.replace(x, x+token.length(), token);
    					outputLine.replace(x + token.length(), x + token.length() + 1, " ");
    				}
    			}
    			
    			
    			
    			if(token.length() <= width + x - outputLine.length()){
    				newLine = false;
    				outputLine.append(token);
                    
                    if(j == tokens.length - 1){
                    	for(int l = outputLine.length(); l < width + x; l++){
        					outputLine.append(" ");
        				}
                    }else if(width + x - outputLine.length() > 0){
                    	outputLine.append(" ");
                    }
    				output.put(y, outputLine);
    			}else{
    				for(int l = outputLine.length(); l < width + x; l++){
    					outputLine.append(" ");
    				}
    				output.put(y, outputLine);
    				y = y+1;
    				newLine = true;
    				j = j-1;
    			}
    		}
    	}
    	
    	String[] outputStr = new String[output.keySet().size()];
    	for(Integer key: output.keySet()){
    		outputStr[key] = output.get(key).toString();
    	}
    	
    	return outputStr;

    }
    
    
    public static void main(String[] args) {
		String[] input = {"10 4 1 I love monkeys.", "20 0 0 It was the best of times it was the worst of times."};
		String[]  output = layout(input);
		for(String str: output){
			System.out.println(str);
		}
	}
}
