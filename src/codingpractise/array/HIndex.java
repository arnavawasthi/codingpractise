/**
 * 
 */
package codingpractise.array;

import codingpractise.heap.MaxHeap;

/**
 * https://leetcode.com/problems/h-index/
 * https://skywalkerdavid.gitbooks.io/algorithm-questions/content/sort/h-index.html
 * TODO: Find O(N) time approach, which takes O(N) extra space
 * @author arnavawasthi
 *
 */
public class HIndex {
	
	/*
	 * This solution is similar to sorting the array and finding H-Index.
	 * 1. Heap: O(N) heapify, N*Log(N) for extractMin.
	 * 2. Sort: N*Log(N), N for calculating H-Index
	 */
	public static int solution(int[] citations){
		int hIndex = 0;
		int counter = 0;
		MaxHeap heap = new MaxHeap(citations); 
		while(heap.size != 0){
			counter++;
			int max = heap.extractMax();
			if(counter == max && max > heap.getMax()){
				hIndex = counter;
				System.out.println("H Index: "+hIndex);
			}
		}
		
		return hIndex;
	}
	
	public static void main(String[] args) {
		{
			int[] citations = {3, 0, 6, 1, 5};
			System.out.println(solution(citations));
		}
		
		{
			int[] citations = {5, 4, 3, 2, 1};
			System.out.println(solution(citations));
		}
	}

}
