/**
 * 
 */
package codingpractise.array;

import java.util.Arrays;

/**
 * Given an array with 0 to N numbers in increasing order. Only one number is missing. Find it out.
 * Easiest it to iterate the array and find the missing number - O(N).
 * Try Binary Search.
 * @author arnavawasthi
 *
 */
public class MissingNumber {

	public static int counter = 0;
	
	public static int solution(int[] a){
		if(a[0] == 1) return 0;
			
		return helper(a, 0, a.length - 1);
	}
	
	private static int helper(int[] a, int l, int h){
		counter++;
		
		if(l > h) return -1;
		
		int m = l + (h-l)/2;
		
		if(a[m] != m && a[m-1] == m-1) return m;
		
		if(a[m] == m){
			return helper(a, m+1, h);
		}else{
			return helper(a, l, m-1);
		}
	}
	
	public static void main(String[] args) {
		{
			int x = 5999;
			int[] a = new int[100000];
			
			for(int i = 0; i < a.length; i++){
				int y = i;
				if (i >= x){
					y = i+1;
				}
				a[i] = y;
				
			}
			
			System.out.println(solution(a));
			System.out.println("Iterations: "+ counter);
		}
		
		{
			counter = 0;
			int x = 1889099;
			int[] a = new int[100000000];
			
			for(int i = 0; i < a.length; i++){
				int y = i;
				if (i >= x){
					y = i+1;
				}
				a[i] = y;
				
			}
			
			System.out.println(solution(a));
			System.out.println("Iterations: "+ counter);
		}
		
		{
			counter = 0;
			int x = 0;
			int[] a = new int[10];
			
			for(int i = 0; i < a.length; i++){
				int y = i;
				if (i >= x){
					y = i+1;
				}
				a[i] = y;
				
			}
			System.out.println(Arrays.toString(a));
			
			System.out.println(solution(a));
			System.out.println("Iterations: "+ counter);
		}
	}
}
