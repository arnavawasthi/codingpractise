/**
 * 
 */
package codingpractise.array;

/**
 * It should be O(logN) algo
 * http://www.programcreek.com/2014/02/leetcode-find-peak-element/
 * https://www.youtube.com/watch?v=a7D77DdhlFc
 * @author arnavawasthi
 *
 */
public class PeakElement {
	
	public static int solution(int[] a){
		return helper(a, 0, a.length -1);
	}

	/**
	 * @param a
	 * @param i
	 * @param j
	 * @return
	 */
	private static int helper(int[] a, int l, int h) {
		if(l > h) return -1;
		
		int m = l + (h-l)/2;
		
		if(a[m] >= a[m-1] && a[m] >= a[m+1]){
			return m;
		}else if(a[m-1] > a[m+1]){
			return helper(a, l, m-1);
		}else{
			return helper(a, m+1, h);
		}
		
	}
	
	public static void main(String[] args) {
		{
			int a[] = {1,2,3,4,5,4,3,2,1};
			System.out.println(solution(a));
		}
		
		{
			int a[] = {1,2,6,4,2,4,1,2,1};
			System.out.println(solution(a));
		}
		
		{
			int a[] = {1,2,6,4,2,3,1,2,1};
			System.out.println(solution(a));
		}
	}

}
