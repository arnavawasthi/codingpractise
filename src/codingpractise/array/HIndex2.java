/**
 * 
 */
package codingpractise.array;

/**
 * https://leetcode.com/problems/h-index-ii/
 * Target: O(logN) solution.
 * @author arnavawasthi
 *
 */
public class HIndex2 {
	
	public static int solution(int[] citations){
		
		return helper(citations, 0, citations.length-1, citations.length);
	}

	/**
	 * @param citations
	 * @param l
	 * @param h
	 * @return
	 */
	private static int helper(int[] citations, int l, int h, int len) {
		if(l > h) return 0;
		
		int m = l + (h-l)/2;
		int tempH = len - m;
		if(tempH <= citations[m] && (m > 0 && tempH > citations[m-1])){
			return tempH;
		}else if(m > 0 && tempH < citations[m-1]){
			return helper(citations, l, m-1, len);
		}else{
			return helper(citations, m+1, h, len);
		}
		
	}
	
	public static void main(String[] args) {
		
		{
			int[] citations = {1, 4, 5, 10, 20};
			System.out.println(solution(citations));
		}
		
		{
			int[] citations = {1, 4, 5, 8, 9, 10, 20};
			System.out.println(solution(citations));
		}

		{
			int[] citations = {1, 2, 3, 4, 5};
			System.out.println(solution(citations));
		}
		
		{
			int[] citations = {1, 2, 3, 4, 5, 6};
			System.out.println(solution(citations));
		}
		
		{
			int[] citations = {1, 2, 3, 4, 5, 6, 7};
			System.out.println(solution(citations));
		}
		
	}
	
}
