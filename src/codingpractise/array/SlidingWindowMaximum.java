/**
 * 
 */
package codingpractise.array;

import java.util.Arrays;

import codingpractise.heap.MaxHeap;

/**
 * https://leetcode.com/problems/sliding-window-maximum/
 * http://www.programcreek.com/2014/05/leetcode-sliding-window-maximum-java/
 * TODO: Solve it in linear time?
 * @author arnavawasthi
 *
 */
public class SlidingWindowMaximum {
	
	/*
	 * This method tries to keep two max values for K window. So when window moves and max element is moved out of window,
	 * 2nd max can be used for comparison. But this method doesn't work, because we won't just need 2 max. For example:
	 * 5, 4, 3, 2, 1 and k = 3
	 * 
	 * Target complexity was: O(N)
	 */
	public static int[] wrongSolution(int[] a, int k){
		
		int window = 1;
		int winMax = Integer.MIN_VALUE;
		int winMax2 = Integer.MIN_VALUE;
		int[] output = new int[a.length - k - 1];
		int outputCounter = 0;
		
		for(int i = 0; i < a.length; i++){
			if(window > k){
				output[outputCounter] = winMax;
				window = 1;
			}
			
			window++;
		}
		
		
		return null;
	}
	
	/*
	 * This method tries to keep a Max-Heap of size k. Extract max and push a new variable. But this solution also
	 * doesn't work, because we remove max. Ideally we should remove 0th element and still maintain max of 1 - k+1.
	 */
	public static int[] wrongSolutionHeap(int[] a, int k){
		MaxHeap heap = new MaxHeap(k);
		int[] output = new int[a.length - k + 1];
		                       
		for(int i = 0; i < a.length; i++){
			if(i <= k-1){
				heap.add(a[i]);
			}else{
				output[i - k] = heap.getMax();
				heap.replaceMax(a[i]);
			}
		}
		
		return output;
	}
	
	

	public static void main(String[] args) {
		{
			int a[] = {1,3,-1,-3,5,3,6,7};
			int k = 3;
			System.out.println(Arrays.toString(wrongSolutionHeap(a, k)));
		}
	}
}
