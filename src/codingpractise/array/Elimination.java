package codingpractise.array;
/**
 * 
 */

/**
 * https://leetcode.com/contest/2/problems/elimination-game/
 * @author arnavawasthi
 * 
 */
public class Elimination {

	public static int lastRemaining(int n){
		
		int start = 1, end = n, distance = 1, total = n;
		while(total > 1){
// 			System.out.println(String.format("Total: %d Distance: %d", total, distance));
			//Forward traversal
			if(distance > 0){
				start = start + distance;
				//Didn't think of this earlier. But if new start goes beyond older end. End should be the result
				if(start > end) return end;
				
				if(total%2 == 1){
					end = end - distance;
				}
				
			}
			//backward traversal
			else{
				end = end + distance;
				//Didn't think of this earlier. But if new start goes beyond older end. End should be the result
				if(end < start) return start;
				if(total%2 == 1){
					start = start - distance;
				}
			}
// 			System.out.println(String.format("Start: %d End: %d", start, end));
			distance = distance*(-2);
			total = (total)/2;
		}
		
		return start;
	}
	
	public static int lastRemainingMemoryLimitExceeded(int n) {
		int a[] = new int[n];
		for (int i = 0; i < n; i++) {
			a[i] = i + 1;
		}
		int b[] = new int[n / 2 + 1];
		int output = 0;
		int start = 0, end = n - 1;
		while (true) {
			int counter = 0;
			if (start == end) {
				output = a[start];
				break;
			}
			for (int i = start; i < end; i = i + 2) {
				b[counter++] = a[i + 1];
			}
			counter = counter - 1;
			end = counter;
			if (counter == 0) {
				output = b[counter];
				break;
			}

			for (int j = end; j > 0; j = j - 2) {
				a[counter--] = b[j - 1];
			}
			start = counter + 1;

		}

		return output;
	}
	
	public static void main(String[] args) {
		System.out.println(lastRemaining(11));
		
//		System.out.println(lastRemaining(12));
	}
}