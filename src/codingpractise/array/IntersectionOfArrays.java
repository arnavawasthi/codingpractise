/**
 * 
 */
package codingpractise.array;

/**
 * https://leetcode.com/problems/intersection-of-two-arrays-ii/
 * Note: While creating Map of values from one array, check which one is smaller and create map of smaller array (saves space)
 * Implementing follow up questions.
 * @author arnavawasthi
 *
 */
public class IntersectionOfArrays {
	
	//Arrays are already sorted.
	public static void solution(int[] a, int[] b){
		int ai =0, bi=0;
		
		//TODO: This is not optimal.
		//1,2,3 and 4,5,6 (it will still iterate arrays till the end.)
		while(ai < a.length && bi < b.length){
			if(a[ai] == b[bi]){
				System.out.println(a[ai]);
				ai++;
				bi++;
			}else if(a[ai] < b[bi]){
				ai++;
			}else{
				bi++;
			}
		}
		
	}
	
	public static void main(String[] args) {
		{
			int[] a = {1, 1, 2, 2};
			int[] b = {2, 2};
			solution(a, b);
			
		}
	}
	
	
}
