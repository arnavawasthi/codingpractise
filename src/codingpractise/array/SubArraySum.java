/**
 * 
 */
package codingpractise.array;

import java.util.Scanner;

/**
 * TODO: Need to optimize it.
 * 
 * @SubArraySum.png
 * @author arnavawasthi
 * 
 */
public class SubArraySum {

	public static long solution1(int[] a, int k) {
		System.out.println("====================== solution1 ====================");
		long start = System.currentTimeMillis();

		int len = a.length;
		long count = 0;
		for (int i = 0; i < len; i++) {
			int temp = 0;
			for (int j = i; j < len; j++) {
				temp += a[j];
				if (temp % k == 0) {
					count++;
				}
			}
		}

		long time = System.currentTimeMillis() - start;
		System.out.println("Time: " + time);

		return count;
	}

	// public static long solution2(int[] a, int k){
	// long start = System.currentTimeMillis();
	// int len = a.length;
	// long count = 0;
	// for(int i = 0; i < len; i++){
	// int temp = 0;
	// for(int j = i; j < len; j++){
	// temp = temp + a[j];
	//
	// if(temp%k == 0){
	// count++;
	// }
	// }
	// }
	//
	// long time = System.currentTimeMillis() - start;
	// System.out.println("Time: "+time);
	//
	// return count;
	// }

	public static long solution3(int[] a, int k) {
		System.out.println("====================== solution3 ====================");
		long start = System.currentTimeMillis();

		long count = 0;
		int len = a.length;
		for (int i = 0; i < len; i++) {
			a[i] = a[i] % k;
		}

		for (int i = 0; i < len; i++) {
			int sum = 0;
			for (int j = i; j < len; j++) {
				sum = sum + a[j];
				if (sum == 0 || sum == k) {
					sum = 0;
					count++;
				} else if (sum > k) {
					sum = sum - k;
				}

			}
		}

		long time = System.currentTimeMillis() - start;
		System.out.println("Time: " + time);

		return count;
	}

	/**
	 * TODO: Copied solution, doesn't work correctly. But idea is worth noting.
	 * Sub-Routine to count numnber of sub-sequnces
	 * 
	 * @param d
	 *            : the given divisor
	 */
	public static int solution4(int[] arr, int d) {
		System.out.println("====================== solution4 ====================");
		long start = System.currentTimeMillis();

		Object o;
		int Answer = 0;
		int[] hash = new int[d];

		int sum = 0;
		int val;
		int num;

		for (int i = 0; i < arr.length; i++) {
			num = arr[i];

			if (num % d == 0) // count numbers which are divisible by divisor
				Answer++;

			sum += num;
			val = sum % d;

			if (val < 0) // handle negative numbers
				val = val * (-1);

			hash[val] = hash[val] + 1;
		}

		int size = hash.length;
		for (int i = 0; i < size; i++) {
			int count = hash[i];

			if (count > 1)
				Answer = Answer + count * (count - 1) / 2;
		}
//		System.out.println(Answer + hash[0]);
		long time = System.currentTimeMillis() - start;
		System.out.println("Time: " + time);
		return Answer + hash[0];
	}

	public static void main(String[] args) {
		{
			int x = 10000;
			int k = 5;

			int[] a = new int[x];
			for (int i = 0; i < x; i++) {
				a[i] = i;
			}

			System.out.println(solution1(a, k));
			System.out.println(solution3(a, k));
			System.out.println(solution4(a, k));
			// System.out.println(solution2(a, k));
		}

		{
			int x = 100000;
			int k = 5;

			int[] a = new int[x];
			for (int i = 0; i < x; i++) {
				a[i] = i;
			}

			System.out.println(solution1(a, k));
			System.out.println(solution3(a, k));
			System.out.println(solution4(a, k));
		}
	}
	
	public static void main1(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner s = new Scanner(System.in);
        
        s.nextLine();
        
        while(s.hasNext()){
            String[] ak = s.nextLine().split(" ");
            String[] astr = s.nextLine().split(" ");
            
            int k = Integer.parseInt(ak[1]);
            int len = astr.length;
            int[] a = new int[len];
            for(int i = 0; i < len; i++){
                a[i] = Integer.parseInt(astr[i]);
            }
            
            System.out.println(solution3(a, k));
        }
    }
}
