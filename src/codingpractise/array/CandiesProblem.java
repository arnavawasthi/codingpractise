/**
 * 
 */
package codingpractise.array;

import java.util.Arrays;

/**
 * @author arnavawasthi
 *
 */
public class CandiesProblem {

	
	/*
	 * Complete the function below.
	 */

	    static int distributeCandy(int[] score) {

	        int candies[] = new int[score.length];
	        
	        candies[0] = 1;
	        for(int i = 1; i < score.length; i++){
	            if(score[i-1] < score[i]){
	                candies[i] = candies[i-1] + 1;
	            }else{
	                //int temp = candies[i-1] - 1;
	                //if(temp <= 0){
	                //    temp = 1;
	                //}
	                candies[i] = 1;//temp;
	            }
	            
	        }
	        System.out.println(Arrays.toString(candies));
	        //int inc = 0;
	        for(int j = candies.length-1 ; j >= 1; j--){
	            if(score[j-1] > score[j] && candies[j-1] <= candies[j]){
	                candies[j-1] = candies[j] + 1;
	            }
	        }
	        System.out.println(Arrays.toString(candies));
	        int output = 0;
	        for(int candy: candies){
	            output += candy;
	        }
	       
	        return output;

	    }



}
