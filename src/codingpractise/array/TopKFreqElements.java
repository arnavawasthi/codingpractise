/**
 * 
 */
package codingpractise.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import codingpractise.heap.MaxHeap;
import codingpractise.heap.MaxHeapList;

/**
 * https://leetcode.com/problems/top-k-frequent-elements/
 * {@link http://www.programcreek.com/2014/05/leetcode-top-k-frequent-elements-java/}
 * @author arnavawasthi
 *
 */
public class TopKFreqElements {

	public static int[] solution(int[] a, int k){
		
		//build count map
		Map<Integer, Integer> countMap = new HashMap<Integer, Integer>();
		for(Integer i: a){
			Integer count = (countMap.containsKey(i))?countMap.get(i)+1:1;
			countMap.put(i, count);
		}
		
		//invert count map
		List<Integer> counts = new ArrayList<Integer>();
		Map<Integer, List<Integer>> invertedCountMap = new HashMap<Integer, List<Integer>>();
		for(Entry<Integer, Integer> entry: countMap.entrySet()){
			Integer value = entry.getKey();
			Integer count = entry.getValue();
			List<Integer> values = invertedCountMap.get(count);
			if(values == null){
				values = new ArrayList<Integer>();
				//count seen for the first time. So counts List will have unique counts.
				counts.add(count);
			}
			values.add(value);
			
			invertedCountMap.put(count, values);
		}
		
		/*
		 * This is a sub-optimal solution. Ideally I should heapify the given array inplace to make it a max heap.
		 */
//		MaxHeap heap = buildMaxHeap(counts);
		MaxHeapList heap = new MaxHeapList(counts);
		
		int[] output = new int[k];
		int counter = 0;
		while(counter < k){
			int max = heap.extractMax();
			List<Integer> values = invertedCountMap.get(max);
			for(Integer value: values){
				output[counter++] = value;
			}
		}
		return output;
	}
	
	/*
	 * TODO: This is a sub-optimal solution. Ideally I should heapify the given array inplace to make it a max heap.
	 */
	static MaxHeap buildMaxHeap(List<Integer> counts){
		MaxHeap heap = new MaxHeap(counts.size());
		for(Integer count: counts){
			heap.add(count);
		}
		
		return heap;
	}
	
	public static void main(String[] args) {
		int[] a = new int[]{1,2,2,3,3,3,3,4,4,4};
		System.out.println(Arrays.toString(solution(a, 2)));
	}
}
