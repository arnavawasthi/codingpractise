/**
 * 
 */
package codingpractise.todo;

/**
 * If I merge using merge step of MergeSort:
 * 1. I will create an array Sigma(k) = n. 
 * 2. To fill each element, I will smallest of k elements - O(k)
 * 3. Total will O(n*k). if k = 100 and n = k*100. It will become - O(n*n/100) ~ O(n*n)
 * 
 * Another solution is to:
 * 1. create an array, copy all k arrays into this array - O(n)
 * 2. Max heapify this array - O(n)
 * 3. extraxtMax n times and array is sorted again O(nLog(n))
 * http://www.programcreek.com/2014/05/merge-k-sorted-arrays-in-java/
 * @author arnavawasthi
 *
 */
public class MergeKSortedArrays {

}
