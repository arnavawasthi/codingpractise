/**
 * 
 */
package codingpractise.todo;

/**
 * @author arnavawasthi
 *
 */
public class Median {

	public static float median(float[] a, float[] b){
		float median = 0;
		if(a.length == 2 && b.length == 2){
			median = (Math.max(a[0], b[0]) + Math.min(a[1], b[1]))/2;
		}
		
		return median;
	}
	
	
	
	public static void main(String[] args) {
		float[] a = {1, 3};
		float[] b = {4, 6};
		System.out.println(median(a, b));
	}
}
