/**
 * 
 */
package codingpractise.todo;

import java.util.LinkedList;
import java.util.Queue;

/**
 * The first 12 digits of pi are 314159265358. We can make these digits into an expression evaluating to 27182 (first 5 digits of e) as follows:

3141 * 5 / 9 * 26 / 5 * 3 - 5 * 8 = 27182
or
3 + 1 - 415 * 92 + 65358 = 27182

Notice that the order of the input digits is not changed. Operators (+,-,/, or *) are simply inserted to create the expression.

Write a function to take a list of numbers and a target, and return all the ways that those numbers can be formed into expressions evaluating to the target

For example:
f("314159265358", 27182) should print:

3 + 1 - 415 * 92 + 65358 = 27182
3 * 1 + 4 * 159 + 26535 + 8 = 27182
3 / 1 + 4 * 159 + 26535 + 8 = 27182
3 * 14 * 15 + 9 + 26535 + 8 = 27182
3141 * 5 / 9 * 26 / 5 * 3 - 5 * 8 = 27182 


 * @author arnavawasthi
 *
 */
public class BuildExpressions {

	
	public static String[] solution(String str, int target){
		
		String[] ops = {"", "+", "-", "/", "*"};
		char[] cs = str.toCharArray();
		
		Queue<LinkedList<String>> queue = new LinkedList<LinkedList<String>>();
		LinkedList<String> list = new LinkedList<String>();
		list.add(cs[0]+"");
		queue.add(list);
		
		while(!queue.isEmpty()){
			LinkedList<String> temp = queue.remove();
			for(String tempStr: temp){
				for(String op: ops){
					
				}
			}
			
		}
		
		
		LinkedList<String> temp = queue.remove();
		for(String tempStr: temp){
			for(String op: ops){
				
			}
		}
		
		
		return null;
	}
}
