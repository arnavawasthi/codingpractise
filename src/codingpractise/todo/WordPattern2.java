/**
 * 
 */
package codingpractise.todo;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * http://www.programcreek.com/2014/07/leetcode-word-pattern-ii-java/
 * @author arnavawasthi
 *
 */
public class WordPattern2 {
	
	static int itr = 0;
    static int wordpattern(String pattern, String input) {

        Map<Character, String> mapping = new HashMap<Character, String>();
        Set<String> set = new HashSet<String>();
        
        int output = helper(pattern, input, 0, 0, mapping, set);
        System.out.println("Iterations: "+ itr);
        return output;
        

    }

    static int helper(String pattern, String input, int pi, int ii, Map<Character, String> mapping, Set<String> set){
        if(pi == pattern.length() && ii == input.length()){
            return 1;
        }
        
        if(pi >= pattern.length() || ii >= input.length()){
            return 0;
        }
        
        itr++;
        
        char pc = pattern.charAt(pi);
        for(int i = ii+1; i <= input.length(); i++){
            String temp = input.substring(ii, i);
            
            if(!mapping.containsKey(pc) && !set.contains(temp)){
                mapping.put(pc, temp);
                set.add(temp);
                
                if(helper(pattern, input, pi+1, i, mapping, set) == 1){
                    return 1;
                }
                
                mapping.remove(pc);
                set.remove(temp);
            }else if(mapping.containsKey(pc) && mapping.get(pc).equals(temp)){
                if(helper(pattern, input, pi+1, i, mapping, set) == 1){
                    return 1;
                }
            }
        }
        
        return 0;
    }
    
    public static void main(String[] args) {
    	
    	{
    		System.out.println(wordpattern("abba", "redredredred"));
    		
    	}
    	
    	{
    		System.out.println(wordpattern("aaaa", "bbbb"));
    		
    	}
    	
    	{
    		System.out.println(wordpattern("aaaababa", "ssssssssrrssrrss"));
    		
    	}
    	
    	{
    		System.out.println(wordpattern("aaaababa", "redredredredgreengreenredredgreengreenredred"));
    		
    	}
	}
}
