/**
 * 
 */
package codingpractise.todo;

import java.util.Arrays;
import java.util.Comparator;

import codingpractise.sorting.Interval;

/**
 * 
 * http://www.programcreek.com/2014/05/leetcode-meeting-rooms-ii-java/
 * @author arnavawasthi
 *
 */
public class MeetingRooms2 {

	public static int solution(Interval[] meetings){
		
		int count = 1;
		
		//sorting (TODO: try to implement this using Heap as well)
		Arrays.sort(meetings, new Comparator<Interval>(){
			
			public int compare(Interval a, Interval b){
				
				//Don't just sort by startTime. If startTimes are equal, sort by endTime
				return -1;
			}
			
		});
		
		return 1;
		
	}
}
