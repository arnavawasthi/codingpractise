/**
 * 
 */
package codingpractise.sorting;

/**
 * @author arnavawasthi
 * 
 */
public class Interval {
	int start;
	int end;

	public Interval(int start, int end) {
		this.start = start;
		this.end = end;
	}
}