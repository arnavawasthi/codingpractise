/**
 * 
 */
package codingpractise.sorting;

import java.util.Arrays;
import java.util.Comparator;

/**
 * http://www.programcreek.com/2014/07/leetcode-meeting-rooms-java/
 * @author arnavawasthi
 *
 */
public class MeetingRooms {

	public static boolean isOverlapping(Interval[] meetings){
	    Arrays.sort(meetings, new Comparator<Interval>(){
	        public int compare(Interval a, Interval b){
	            return a.start-b.start;
	        }
	    });
		
	    
	    for(int i = 0; i < meetings.length - 1; i++){
	    	if(meetings[i].end > meetings[i+1].start){
	    		return true;
	    	}
	    }
		return false;
	}
	
	
	public static void main(String[] args) {
		{
			Interval[] meetings = new Interval[5];
			meetings[0] = new Interval(10, 20);
			meetings[1] = new Interval(0, 5);
			meetings[2] = new Interval(8, 15);
			meetings[3] = new Interval(20, 30);
			meetings[4] = new Interval(25, 40);
			
			System.out.println(isOverlapping(meetings));
		}
		
		{
			Interval[] meetings = new Interval[5];
			meetings[0] = new Interval(0, 10);
			meetings[1] = new Interval(10, 15);
			meetings[2] = new Interval(15, 25);
			meetings[3] = new Interval(28, 30);
			meetings[4] = new Interval(34, 40);
			
			System.out.println(isOverlapping(meetings));
		}
	}
}

