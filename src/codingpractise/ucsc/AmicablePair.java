package codingpractise.ucsc;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 */

/**
 * @author nupurdixit
 * 
 */
public class AmicablePair {
	int num;
	int count = 0;
	int[] factSum; 
	List<Integer> primes = new ArrayList<Integer>();
	int maxPrimeNumber;
	
	AmicablePair(int n) {
		factSum = new int[n+1];
		maxPrimeNumber = (int)Math.ceil(Math.sqrt(n));
		
		System.out.println("The following are amicable numbers");
		for(int j = 2; j <= n; j++){
			factSum[j] = getFactorsSum(j); 
		}		
		
		for (int i = 2; i <= n; i++) {
			
			int sum = factSum[i];
			
			if(sum != -1 && sum <= n && i != sum && factSum[sum] == i){
				System.out.println(count + " :" + i + " and " + sum);
				count++;
				factSum[sum] = -1;
			}

		}

	}
	
	private int getFactorsSum(int num){
		
		int sqrt1 = (int)Math.sqrt(num);
		int sum1 = 1;
		for(Integer prime: primes){
			if(prime == 0 || prime > sqrt1) break;
			
			if(num%prime == 0){
				int d = num/prime;
				
				//18 is processed in if. because 2 divides 18. Ans 9 is not divisible by 2 (similarly 6, 10, 14)
				if(d%prime != 0){
					sum1 = 1 + prime + d + (factSum[d] - 1) +  (prime*factSum[d] - prime);
					//This if/else is not needed. Because prime != d always. We came to this part
					// because d (9) is not divisible by prime (2)
//					if(prime != d){
//						sum1 = 1 + prime + d + (factSum[d] - 1) +  (prime*factSum[d] - prime);
//					}else{
//						sum1 = 1 + prime + (factSum[d] - 1) +  (prime*factSum[d] - prime);
//					}
					return sum1;
				}
				//12, 4 are processed in else. 
				//6: 1, 2, 3
				//so 12: 6+factSum(6)+4 (here factSum(3) is invisible ie = 1)
				//Now consider 36:
				//36 = 2x18
				//18: 1, 2, 3, 6, 9
				//36: 18 + factSum(18) + 4*factSum(9)
				else{
//					int counter = 1;
					sum1 = d+factSum[d];
					int temp1 = d;
					int temp = prime;
					while(temp1%prime == 0){
//						counter++;
						temp*=prime;
						temp1 = temp1/prime;
					}
//					int temp = (int)Math.pow(prime, counter);
					sum1 += temp*factSum[temp1];
					return sum1;
					
				}
			}
		}
		
		//If output is not returned in above snippet, it means we have found a prime number
		if(num <= maxPrimeNumber){
			primes.add(num);
		}
				
		//Factors sum for a prime number will be always 1
		return 1;
	}
	
	public static double timeInSec(long endTime, long startTime) {
		long duration = (endTime - startTime);
		if (duration > 0) {
			double dm = (duration / 1000000.0); // Milliseconds
			double d = dm / 1000.0; // seconds

			return d;
		}
		return 0.0;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		long startTime = System.nanoTime();
		int n = 100000000;
		AmicablePair a = new AmicablePair(n);

		long EndTime = System.nanoTime();
		double duration = AmicablePair.timeInSec(EndTime, startTime);
		System.out.println("Run time for n = " + n +" is "+ duration + " secs");
		
	}

}
