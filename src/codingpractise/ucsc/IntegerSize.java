package codingpractise.ucsc;
/**
 * 
 */

/**
 * @author arnavawasthi
 *
 */
public class IntegerSize {

	public static void main(String[] args) {
		System.out.println(Integer.MAX_VALUE);
		System.out.println((long)Math.pow(2, 32));
		System.out.println((int)Math.pow(2, 31));
		System.out.println((int)Math.pow(2, 32));
		System.out.println((int)Math.pow(2, 33));
		System.out.println(2147483647+2147483647+2);
		System.out.println("4294967295");
		
		int a = 0 - Integer.MAX_VALUE;
		System.out.println(a);
		System.out.println(a+Integer.MAX_VALUE);
	}
}
