package codingpractise.ucsc;

import codingpractise.ucsc.lib.IntUtil;
/**
 * 
 */

/**
 * @author arnavawasthi
 *
 */

//package Homework_Day3;

//import Homework_Day3.IntUtil;

public class Letter {
	private static final IntUtil u = new IntUtil();
	
	
	public static void main(String[] args) {
//		int s[] = {5,1,0,4,2,3} ;
//		System.out.println(length(s, 0));
//		System.out.println(length(s, 1));
//		System.out.println(length(s, 2));
//		System.out.println(length(s, 3));
//		System.out.println(length(s, 4));
//		System.out.println(length(s, 5));
		// TODO Auto-generated method stub
		System.out.println("Length.java");
		testbed() ;
		testbed1() ;
	}
	
	
	private static int length_easy(int [] s, int x) {
		int l = 0 ;
		int gx = x ;
		while (true) {
		if (s[x] == gx) {
		return l ;
		}
		x = s[x] ;
		++l ;
		}
		}
	
	
	
	private static int length3(int [] s, int x) {
//		return hops(s, x, x);
		//Solution 2 with changing array (setting visited values to -1)
		if(x == -1) return x;
		int t = s[x];
		s[x]=-1;
		return length3(s, t)+1;
		

	}
	
	//With this solution limitation was: Array length can't be > 1000
	public static int length1(int[] s, int x){
		//solution 3 with adding 1000
		Integer a = 1000;//Integer.MAX_VALUE;
		//in the second round, when we are deducting 1000 from the elements.
		//last step will have x > 1000 and value in index (x-1000) would be less than 1000
		//this will be exit for recursion
		if(x >= a && s[x-a] < a){
			return -1;
		}
		//second round, when original value is found but we don't know yet.
		//Now we start deducting 1000 from the values at index
		//second round starts when: either x > 1000 or value found at index x is > 1000
		else if(x >= a || s[x] >= a){
			//In the first step of 2nd round, x will be < 1000. This will be the first time when we will see
			//value > 1000
			x = (x >= a)?x-a:x;
			s[x] = s[x] - a;
			return length1(s, s[x]+a);
		}else{
			//first round: add 1000 to the elements visited
			s[x]+=a;
			return length1(s, s[x]-a)+1;
		}
	}
	
	/**
	 * 
	 * @param s
	 * @param x
	 * @return
	 */
	//This solution works for array of any length (within Integer MAX value limit)
	public static int length(int[] s, int x){
		//solution 3 with adding Integer.MAX_VALUE
		Integer a = Integer.MAX_VALUE;
		//Exit case for recursion: last step of 2nd round when index is < 0 but value is > 0
		if(x < 0 && s[x+a] >= 0) return -1;
		//2nd round of restoring array values stars when we see first value < 0, not in this round we are not incrementing steps
		if(x < 0 || s[x] < 0){
			//index calculated by adding Integer.MAX_VALUE to value x (except first step in 2nd round)
			x = (x < 0)?x+a:x;
			s[x] = s[x] + a;
			return length(s, s[x]-a);
		}
		//1st round: add Integer.MAX_VALUE to every value seen and increment step counter
		s[x] = s[x]-a;
		return length(s, s[x]+a)+1;
		
	}
	
	/*
	 * Solution 1 with subroutine 
	 */
	private static int hops(int[] a, int i, int t){
		if(a[i] == t) return 0;
		return hops(a, a[i], t) + 1;
	}
	
	public static void testbed() {
		int s[] = {5,1,0,4,2,3} ;
		int y = length_easy(s,3) ;
		System.out.println("length_easy y = " + y);
		u.myassert(y == 4) ;
		int b[] = {5,1,0,4,2,3} ;
		int x = length(s,3) ;
		System.out.println("length x = " + x);
		u.myassert(x == y) ;
		for (int i = 0; i < s.length; ++i) {
		u.myassert(s[i] == b[i]);
		}
		System.out.println("Assert passed");
	}
	
	public static void testbed1() {
		int s[] = {5,1,0,4,2,3} ;
		int b[] = {5,1,0,4,2,3} ;
		int l = s.length ;
		for (int j = 0; j < l ; ++j) {
		int y = length_easy(s,j) ;
		System.out.println("length_easy y = " + y);
		int x = length(s,j) ;
		System.out.println("length x = " + x);
		u.myassert(x == y) ;
		for (int i = 0; i < s.length; ++i) {
		u.myassert(s[i] == b[i]);
		}
		System.out.println("Assert passed");
		}
		}

}
