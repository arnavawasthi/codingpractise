package codingpractise.ucsc;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 */

/**
 * @author arnavawasthi
 *
 */
public class PrimeNumbers {

	static List<Integer> primes = new ArrayList<Integer>();
	static int maxPrimes;
	
	static {
		primes.add(2);
	}
	
	public static int getPrimeCount(int max){
		
		int primesCount = 1;
		for(int i = 3; i <= max; i++){
			int sqrt = (int)Math.sqrt(i);
			int maxPrime = primes.get(primes.size() - 1);
			boolean isPrime = true;
			for(Integer prime: primes){
				if(prime > sqrt){
					break;
				}
				if(i%prime == 0){
					isPrime = false;
					break;
				}
			}
			
			if(!isPrime){
				continue;
			}else{
				if(maxPrime < sqrt){
					for(int j = 2; j<=sqrt; j++){
						if(i%j == 0){
							isPrime = false;
						}
					}
					
					if(isPrime){
						primesCount++;
						if(i <= maxPrimes)
							primes.add(i);
					}
				}else{
					primesCount++;
					if(i <= maxPrimes)
						primes.add(i);
				}
			}
			
		}

		
		
		
		return primesCount;
	}
	
	public static void main(String[] args) {
		int max = 100000000;
		maxPrimes = (int)Math.sqrt(max);
		for(int i = 100; i<= max; i = i*10){
			long start = System.currentTimeMillis();
			System.out.println("Size: "+ i+", Primes: " + getPrimeCount(i));
			long timeTaken = System.currentTimeMillis() - start;
			System.out.println("Millis: "+timeTaken);
//			System.out.println(primes);
		}
	}
}
