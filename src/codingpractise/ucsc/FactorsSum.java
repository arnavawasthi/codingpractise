package codingpractise.ucsc;
import java.util.HashSet;
import java.util.LinkedHashSet;

/**
 * 
 */

/**
 * @author arnavawasthi
 *
 */
public class FactorsSum {

	
	int[] factSum;
	int[] factSumModified;
	HashSet<Integer> primes = new LinkedHashSet<Integer>();
	int maxPrimeNumber;
	
	public FactorsSum(int n){
		factSum = new int[n+1];
		factSumModified = new int[n+1];
		
		maxPrimeNumber = (int)Math.ceil(Math.sqrt(n));
		
		System.out.println("Started .... ");
		for(int j = 2; j <= n; j++){
			factSum[j] = getFactorsSum(j); 
			factSumModified[j] = getFactorsSumModified(j);
		}
		
		System.out.println("Filled ....");
		
		for(int i = 2; i <= n; i++){
			if(factSum[i] != factSumModified[i])
				System.out.println(i+": Sum: "+ factSum[i]+", SumModified: "+factSumModified[i]);
		}
	}
	
	private int getFactorsSum(int num){
		
		int sqrt1 = (int)Math.sqrt(num);
		int sum1 = 1;

		
		for (int i = 2; i <= sqrt1; i++) {
			if (num % i == 0) {
				int d = num/i;
				sum1+=i;
				if(d != i){
					sum1 += d;
				}
				
			}
		}
		
		if(sum1 == 1){
			if(num < maxPrimeNumber){
				primes.add(num);
			}
				
		}
		
		return sum1;
	}
	
	private int getFactorsSumModified(int num){
//		if(num%4 == 0){
//			return 3*factSum[num/2] - 2*factSum[num/4];
//		}else if(num%9 == 0){
//			return 4*factSum[num/3] - 3*factSum[num/9];
//		}else if(num%25 == 0){
//			return 6*factSum[num/5] - 5*factSum[num/25];
//		}else if(num%49 == 0){
//			return 8*factSum[num/7] - 7*factSum[num/49];
//		}
		
		int sqrt1 = (int)Math.sqrt(num);
		int sum1 = 1;

//		int sum2 = 1;
		for(Integer prime: primes){
			if(prime == 0 || prime > sqrt1) break;
			
			if(num%prime == 0){
				int d = num/prime;
				if(primes.contains(d)){
					if(prime != d){
						sum1 = 1 + prime + d;
					}else{
						sum1 = 1 + prime;
					}
					
					return sum1;
//					break;
				}else if(d%prime != 0){
					sum1 = 1 + prime + d + (factSum[d] - 1) +  (prime*factSum[d] - prime);
					
					return sum1;
				}else{
					int counter = 1;
					sum1 = d+factSum[d];
					int temp1 = d;
					while(temp1%prime == 0){
						counter++;
						temp1 = temp1/prime;
					}
					int temp = (int)Math.pow(prime, counter);
					
					if(temp1 == 1 || primes.contains(temp1)){
						if(temp < num){
							sum1 += temp;
						}
						return sum1;
					}else{
						sum1 += temp*factSum[temp1];
						return sum1;
					}
					
				}
			}
		}
		
		return 1;
	}
	
	public static void main(String[] args) {
		new FactorsSum(1000);
	}
	
	
	/**
36: Sum: 55, SumModified: 43
60: Sum: 108, SumModified: 76
72: Sum: 123, SumModified: 99
84: Sum: 140, SumModified: 100
100: Sum: 117, SumModified: 97
108: Sum: 172, SumModified: 124
120: Sum: 240, SumModified: 176
132: Sum: 204, SumModified: 148
140: Sum: 196, SumModified: 148
144: Sum: 259, SumModified: 211
156: Sum: 236, SumModified: 172
168: Sum: 312, SumModified: 232
180: Sum: 366, SumModified: 238
196: Sum: 203, SumModified: 175
200: Sum: 265, SumModified: 225
204: Sum: 300, SumModified: 220
216: Sum: 384, SumModified: 288
220: Sum: 284, SumModified: 220
225: Sum: 178, SumModified: 133
228: Sum: 332, SumModified: 244
240: Sum: 504, SumModified: 376
252: Sum: 476, SumModified: 316
260: Sum: 328, SumModified: 256
264: Sum: 456, SumModified: 344
276: Sum: 396, SumModified: 292
280: Sum: 440, SumModified: 344
288: Sum: 531, SumModified: 435
300: Sum: 568, SumModified: 376

	 */
	
}
