/**
 * 
 */
package codingpractise.generic;

import java.util.Arrays;

/**
 * @author arnavawasthi
 *
 */
public class BubbleSort {

	
	/**
	 * TODO: Improvement: If no swap happens in a cycle, we can stop executing and array will be sorted?
	 * Example: 3, 6, 2, 5
	 * @param a
	 */
	public static void solution(int a[]){
		
		for(int i = 0; i < a.length; i++){
			
			for(int k = 0; k < a.length - i - 1; k++){
				if(a[k] > a[k+1]){
					int temp = a[k];
					a[k] = a[k+1];
					a[k+1] = temp;
				}
			}
		}
	}
	
	public static void main(String[] args) {
		int[] a = {3, 6, 2, 5};
		solution(a);
		System.out.println(Arrays.toString(a));
		
		int[] b = {1,2, 3, 4, 5, 6};
		solution(b);
		System.out.println(Arrays.toString(b));
		
		int[] c = {5, 4, 3, 2, 1, 0};
		solution(c);
		System.out.println(Arrays.toString(c));
		
	}
}
