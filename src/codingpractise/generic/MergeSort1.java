/**
 * 
 */
package codingpractise.generic;

import java.util.Arrays;

/**
 * @author arnavawasthi
 *
 */
public class MergeSort1 {
	
	int[] a;
	int[] r;
	
	public MergeSort1(int[] a){
		this.a = a;
		r = new int[a.length];
	}

	public void solution(){
		divide(0, a.length-1);
	}
	
	public void divide(int l, int h){
		
		if(h-l <= 0) return;
		
		int m = (h+l)/2;
		
		divide(l, m);
		divide(m+1, h);
		
		merge(l, m, h);
	}
	
	public void merge(int l, int m, int h){
		int i = l, j = m+1, k = l;
		
		while(i <= m && j <= h){
			if(a[i] < a[j]){
				r[k] = a[i];
				i++;
			}else{
				r[k] = a[j];
				j++;
			}
			k++;
		}
		
		while(i <= m){
			r[k] = a[i];
			i++;
			k++;
		}
		
		while(j <= h){
			r[k] = a[j];
			j++;
			k++;
		}
		
		for(int x = l; x <= h; x++){
			a[x] = r[x];
		}
	}
	
	public static void main(String[] args) {
		int[] a = {1, 2, 5, 8, 10, 0, 3, 4};
		int[] b = {10, 6};
		int[] c = {6, 10};
		int[] d = {10, 6, 5, 2};
		
		MergeSort1 m1 = new MergeSort1(a);
		m1.solution();
		System.out.println(Arrays.toString(a));
		
		
		MergeSort1 m2 = new MergeSort1(b);
		m2.solution();
		System.out.println(Arrays.toString(b));
		
		
		MergeSort1 m3 = new MergeSort1(c);
		m3.solution();
		System.out.println(Arrays.toString(c));
		
		
		MergeSort1 m4 = new MergeSort1(d);
		m4.solution();
		System.out.println(Arrays.toString(d));
		
		
		
		
		
	}
}
