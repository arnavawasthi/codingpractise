/**
 * 
 */
package codingpractise.generic;

import java.util.Arrays;

/**
 * @author arnavawasthi
 *
 */
public class QuickSort1 {

	public static int swaps = 0;
	
	public static void solution(int[] a){
		partition(a, 0, a.length-1);
	}
	
	
	public static void partition(int [] a, int l, int h){
		
		if(l >= h) return;
 		
		int pivot = a[l];
		
		//TODO: Definition of i and j
		int i = l+1, j = l+1;
		boolean ib = false;
		for(; i <= h;){
			
			if(ib == false && a[j] > pivot){
				ib = true;
			}else{
				if (ib == true && a[i] < pivot){
					int temp = a[i];
					a[i] = a[j];
					a[j] = temp;
					j++;
					ib = false;
					swaps++;
				}else if(ib == false && a[i] < pivot){
					j++;
				}
				i++;
			}
			
		}
		
		
		//placing pivot in its correct position
		int temp = a[j-1];
		a[j-1] = a[l];
		a[l] = temp;
		
		int partition = j -1;
		
		System.out.println("partition:"+partition+" :: "+Arrays.toString(a));
 		
		
		partition(a, l, partition-1);
		partition(a, partition+1, h);
	}
	
	public static void main(String[] args) {
		int[] a = {5, 2, 3, 6, 8, 1, 4, 8};
		solution(a);
		System.out.println("Swaps: "+swaps);
		System.out.println(Arrays.toString(a));
		
		swaps = 0;
		int[] b = {5, 2, 3, 6, 0, 1, 4, 8};
		solution(b);
		System.out.println("Swaps: "+swaps);
		System.out.println(Arrays.toString(b));
		
		
		
	}
}
