/**
 * 
 */
package codingpractise.generic;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * 
 Week 2: Question1:
 * 
 * The file contains all of the integers between 1 and 10,000 (inclusive, with
 * no repeats) in unsorted order. The integer in the ith row of the file gives
 * you the ith entry of an input array. Your task is to compute the total number
 * of comparisons used to sort the given input file by QuickSort. As you know,
 * the number of comparisons depends on which elements are chosen as pivots, so
 * we'll ask you to explore three different pivoting rules. You should not count
 * comparisons one-by-one. Rather, when there is a recursive call on a subarray
 * of length m, you should simply add m−1 to your running total of comparisons.
 * (This is because the pivot element is compared to each of the other m−1
 * elements in the subarray in this recursive call.)
 * 
 * WARNING: The Partition subroutine can be implemented in several different
 * ways, and different implementations can give you differing numbers of
 * comparisons. For this problem, you should implement the Partition subroutine
 * exactly as it is described in the video lectures (otherwise you might get the
 * wrong answer). DIRECTIONS FOR THIS PROBLEM: For the first part of the
 * programming assignment, you should always use the first element of the array
 * as the pivot element. HOW TO GIVE US YOUR ANSWER: Type the numeric answer in
 * the space provided. So if your answer is 1198233847, then just type
 * 1198233847 in the space provided without any space / commas / other
 * punctuation marks. You have 5 attempts to get the correct answer.
 * 
 * Question 2: Compute the number of comparisons (as in Problem 1), always using
 * the final element of the given array as the pivot element. Again, be sure to
 * implement the Partition subroutine exactly as it is described in the video
 * lectures. Recall from the lectures that, just before the main Partition
 * subroutine, you should exchange the pivot element (i.e., the last element)
 * with the first element.
 * 
 * Question 3: Compute the number of comparisons (as in Problem 1), using the
 * "median-of-three" pivot rule. [The primary motivation behind this rule is to
 * do a little bit of extra work to get much better performance on input arrays
 * that are nearly sorted or reverse sorted.] In more detail, you should choose
 * the pivot as follows. Consider the first, middle, and final elements of the
 * given array. (If the array has odd length it should be clear what the
 * "middle" element is; for an array with even length 2k, use the kth element as
 * the "middle" element. So for the array 4 5 6 7, the "middle" element is the
 * second one ---- 5 and not 6!) Identify which of these three elements is the
 * median (i.e., the one whose value is in between the other two), and use this
 * as your pivot. As discussed in the first and second parts of this programming
 * assignment, be sure to implement Partition exactly as described in the video
 * lectures (including exchanging the pivot element with the first element just
 * before the main Partition subroutine).
 * 
 * EXAMPLE: For the input array 8 2 4 5 7 1 you would consider the first (8),
 * middle (4), and last (1) elements; since 4 is the median of the set {1,4,8},
 * you would use 4 as your pivot element.
 * 
 * SUBTLE POINT: A careful analysis would keep track of the comparisons made in
 * identifying the median of the three candidate elements. You should NOT do
 * this. That is, as in the previous two problems, you should simply add m−1 to
 * your running total of comparisons every time you recurse on a subarray with
 * length m.
 * 
 * 
 * Test cases previously posted by learner SzuHsien Lee.
 * 
 * https://dl.dropboxusercontent.com/u/20888180/AlgI_wk2_testcases/10.txt
 * 
 * https://dl.dropboxusercontent.com/u/20888180/AlgI_wk2_testcases/100.txt
 * 
 * https://dl.dropboxusercontent.com/u/20888180/AlgI_wk2_testcases/1000.txt
 * 
 * Answers are:
 * 
 * size first last median
 * 
 * 10 25 29 21
 * 
 * 100 615 587 518
 * 
 * 1000 10297 10184 8921
 * 
 * 
 * @author arnavawasthi
 * 
 */
public class QuickSort2 {

	private static int swaps = 0;
	private static int comparisons = 0;
	
	private static Random rd; 
	
	
	
	private enum Pivot {first, last, median, median_of_medians, random};
	
	
	public static void solution(int[] a, Pivot pivot){
		partition(a, 0, a.length-1, pivot);
	}
	
	
	private static void partition(int [] a, int l, int h, Pivot p){
		
		if(l >= h) return;
		
		comparisons += h - l;
 		
		int pivot = -1;
		int pivotIndex = -1;
		switch(p){
		case first:
			pivot = a[l];
			break;
		case last:
			pivot = a[h];
			//swap pivot with first element (l)
			a[h] = a[l];
			a[l] = pivot;
			break;
		case median:
			pivotIndex = median(a, l, h);
			
			pivot = a[pivotIndex];
			a[pivotIndex] = a[l];
			a[l] = pivot;
			
			break;
		case median_of_medians:
			
			break;
		case random:
			if(rd == null){
				rd = new Random();
			}
			int tempIndex = rd.nextInt(h-l+1); //max value is excluded
			pivotIndex = l+tempIndex;
			pivot = a[pivotIndex];
			a[pivotIndex] = a[l];
			a[l] = pivot;
			
			break;
		}
		
		//TODO: Definition of i and j
		int i = l+1, j = l+1;
		
		for(; i <= h;i++){
			
			if (a[i] < pivot){
				int temp = a[i];
				a[i] = a[j];
				a[j] = temp;
				j++;
				swaps++;
			}
			
		}
		
		
		//placing pivot in its correct position
		int temp = a[j-1];
		a[j-1] = a[l];
		a[l] = temp;
		
		int partition = j -1;
		
//		System.out.println("partition:"+partition+" :: "+Arrays.toString(a));
 		
		
		partition(a, l, partition-1, p);
		partition(a, partition+1, h, p);
	}


	/**
	 * It returns index for median of first, last and middle elements.
	 * @param a
	 * @param l
	 * @param h
	 * @param mi
	 * @return
	 */
	private static int median(int[] a, int l, int h) {
		int mi = (h + l)/2;
		
		int pivotIndex;
		//TODO: Median routine needs to be worked upon
		if(a[l] < a[h]){
			if(a[h] < a[mi]){
				pivotIndex = h;
			}else{
				if(a[l] < a[mi]){
					pivotIndex = mi;
				}else{
					pivotIndex = l;
				}
			}
				
		}else{
			if(a[l] < a[mi]){
				pivotIndex = l;
			}else{
				if(a[h] < a[mi]){
					pivotIndex = mi;
				}else{
					pivotIndex = h;
				}
			}
		}
		return pivotIndex;
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		int[] a = {5, 2, 3, 6, 8, 1, 4, 8};
		solution(a, Pivot.first);
		System.out.println("Swaps: "+QuickSort2.swaps);
		System.out.println(Arrays.toString(a));
		
		swaps = 0;
		int[] b = {5, 2, 3, 6, 0, 1, 4, 8};
		solution(b, Pivot.first);
		System.out.println("Swaps: "+QuickSort2.swaps);
		System.out.println(Arrays.toString(b));
		
		comparisons = 0;
		int[] arr = getTestData();
		solution(arr, Pivot.first);
		System.out.println("First: "+comparisons);
		
		comparisons = 0;
		int[] arr1 = getTestData();
		solution(arr1, Pivot.last);
		System.out.println("Last: "+comparisons);
		
		comparisons = 0;
		int[] arr2 = getTestData();
		solution(arr2, Pivot.median);
		System.out.println("Median: "+ comparisons);
		
		comparisons = 0;
		int[] arr3 = getTestData();
		solution(arr3, Pivot.random);
		System.out.println("Random: "+ comparisons);
		
		/*
		 * 
First: 162085
Last: 164123
Median: 151001 (159894) (138382)
Random: 159637, 180579, 146896
		 */
		
	}


	/**
	 * @return
	 * @throws FileNotFoundException
	 */
	private static int[] getTestData() throws FileNotFoundException {
		Scanner s = new Scanner(new File("/Users/arnavawasthi/Downloads/QuickSort.txt"));
		int[] arr = new int[10000];
		int i = 0;
		while(s.hasNextLine()){
			arr[i++] = Integer.parseInt(s.nextLine());
		}
		s.close();
		return arr;
	}
}
