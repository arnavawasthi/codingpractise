/**
 * 
 */
package codingpractise.generic;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * All operations in MinStack takes O(1) time. Only overhead is to maintain a stack for minimum seen so far.
 * http://www.programcreek.com/2014/02/leetcode-min-stack-java/
 * @author arnavawasthi
 *
 */
public class MinStack {

	private List<Integer> stack;
	private List<Integer> minStack;
	private int min = Integer.MAX_VALUE;
	
	public MinStack(){
		stack = new ArrayList<Integer>();
		minStack = new ArrayList<Integer>();
	}
	
	public void push(int x){
		//Keep equal as well. So that if we find duplicate of current min, we keep both instances in the minStack.
		//Reason is: In the pop, when first instance of min is deleted, it will delete it from minStack as well.
		//leaving us with incorrect min
		//Example: 
		// Stack: [25, 14, 8, 24, 13, 13, 22, 5, 27, 10, 3, 18, 8, 6, 3]
		// MinStack: [25, 14, 8, 5, 3]
		// When 3 is popped from stack, 3 from minStack will also be deleted. And getMin will say 5,
		// whereas it should still be 3.
		if(x <= min){
			min = x;
			minStack.add(min);
		}
		stack.add(x);
		//if I put min to minStack for every push, minStack will also use O(n) space
//		minStack.add(min);
	}
	
	public int top(){
		return stack.get(stack.size()-1);
	}
	
	public int pop(){
		int i = stack.size()-1;
		int minStackLast = minStack.size() - 1;
		int x = stack.get(i);
		stack.remove(i);
		//remove only if removed element from stack is the current min 
		if(x == minStack.get(minStackLast)){
			minStack.remove(minStackLast);
		}
		return x;
	}
	
	public int getMin(){
		return minStack.get(minStack.size() - 1);
	}
	
	public static void main(String[] args) {
		MinStack ms = new MinStack();
		Random r = new Random();
		for(int i = 0; i < 15; i++){
			int x = r.nextInt(30);
			ms.push(x);
		}
		System.out.println(ms.stack);
		System.out.println(ms.minStack);
		
		for(int i = 0; i < 15; i++){
			System.out.println("min: "+ms.getMin());
			System.out.println("pop:"+ms.pop());
		}
	}
	
	
	
	
	
	
}
