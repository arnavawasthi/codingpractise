/**
 * 
 */
package codingpractise.generic;

import java.util.Arrays;

/**
 * @author arnavawasthi
 *
 */
public class PerfectSquareCount {

	static int[] getMinimumUniqueSum(String[] arr) {
        int[] output = new int[arr.length];
        
        for(int i = 0; i < arr.length; i++){
            String[] temp = arr[i].split(" ");
            int i1 = Integer.parseInt(temp[0]);
            int i2 = Integer.parseInt(temp[1]);
            int count = 0;
            
            
            /*
             * Basic solution
             */
//            int j = i1;
//            int sqrt = 0;
//            for( ;j <= i2; j++){
//                sqrt = (int)Math.sqrt(j);
//                if(j == sqrt*sqrt){
//                    count++;
//                }
//            }
            
            /*
             * This approach was to find first perfect square in range,
             * then it's sqrt to as start point, increment it and check if it sqr is less then max in range
             */
//            for( ;j <= i2; j++){
//                sqrt = (int)Math.sqrt(j);
//                if(j == sqrt*sqrt){
//                    count++;
//                    break;
//                }
//            }
//            sqrt++;
//            int sqr = sqrt*sqrt;
            
            /*
             * Easier and more intuitive solution. start from first number sqrt. and run until sqr < max 
             * One imp thing: sqr might go beyond Integer.MAX_VALUE and becomes neg. Since sqr will always 
             * be positive, we need to put this sqr > 0 check as well
             * 
             */
            int sqrt1 = (int) Math.sqrt(i1);
            int sqrt2 = (int) Math.sqrt(i2);
            
            for(int sqrt = sqrt1; sqrt <= sqrt2; sqrt++){
            	int sqr = sqrt*sqrt;
            	if(sqr > 0  && sqr >= i1 && sqr <= i2){
            		count++;
            	}
            }
            output[i] = count;
         }
        return output;

    }
	
	public static void main(String[] args) {
		
		//[1 20, 1 2000000000, 1 2111111111, 1 2147483647]
		//[4, 44721, 45946, 46340]
		//Time taken: 5

		
		long start = System.currentTimeMillis();
		String s = 1 + " "+Integer.MAX_VALUE;
		
//		String[] str = {"1 20", "1 2000000000", "1 2111111111", s};
		String[] str = {"3 9", "17 24", "1 2111111111", s};
		System.out.println(Arrays.toString(str));
		System.out.println(Arrays.toString(getMinimumUniqueSum(str)));
		
		long time = System.currentTimeMillis() - start;
		System.out.println("Time taken: "+ time);
	}
}
