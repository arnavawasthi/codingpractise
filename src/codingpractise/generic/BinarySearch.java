/**
 * 
 */
package codingpractise.generic;

import java.util.Arrays;

/**
 * This is modified binary search. In addition to finding an element, it also finds the range in which it is present.
 * http://www.programcreek.com/2014/04/leetcode-search-for-a-range-java/
 * @author arnavawasthi
 *
 */
public class BinarySearch {
	int[] source;
	
	public BinarySearch(int[] array){
		source = array;
		System.out.println(Arrays.toString(source));
	}
	
	public int solution(int target){
		System.out.println(String.format(" ********** Finding %d ***********", target));
		int[] range = new int[]{-1, -1};
		return helper(target, 0, source.length-1, range);
	}
	
	private int helper(int target, int l, int h, int[] range){
		if(h < l) return -1;
		System.out.println(String.format("Target: %d, Low: %d, High: %d", target, l, h ));
		int m = l + (h-l)/2;
		if(source[m] == target){
			//finding range for given element "target"
			findRange(l, h, m, range);
			//binary search to find first instance
//			int lowerBound = helper(target, l, m, range);
//			System.out.println(String.format("LowerBound: %d", lowerBound));
//			range[0] = lowerBound;
//			int higherBound = helper(target, m, h, range);
//			range[1] = higherBound;
//			System.out.println(String.format("HigherBound: %d", higherBound));
			System.out.println(String.format("Range: %s", Arrays.toString(range)));
			
			return m;
		}else if(source[m] < target){
			return helper(target, m+1, h, range);
		}else{
			return helper(target, l, m-1, range);
		}
	}
	
	/**
	 * This routine can take O(n) time. Can it be done O(log n)?
	 * @param target
	 * @param l
	 * @param h
	 * @param m
	 */
	
	private void findRange(int l, int h, int foundIndex, int[] range) {
		// TODO Auto-generated method stub
		int min = foundIndex, max= foundIndex;
		int target = source[foundIndex];
		while(min > l){
			if(source[min-1] == target){
				min--;
			}else{
				break;
			}
		}
		
		while(max < h){
			if(source[max+1] == target){
				max++;
			}else{
				break;
			}
		}
		range[0] = min;
		range[1] = max;
//		System.out.println(String.format("Min: %d, Max: %d", min, max));
		
	}

	public static void main(String[] args) {
		int length = 10;
		int[] a = new int[length];
		for(int i = 0; i<length; i++){
			a[i] = i+1;
		}
		BinarySearch bs = new BinarySearch(a);
		for(int i = 0; i<length; i++){
			System.out.println(bs.solution(i+1));
		}
		
		length = 55;
		int[] array = new int[length];
		int counter = 0;
		int arrayLimit = 10;
		for(int i = 0; i<arrayLimit; i++){
			for(int j = 0; j <= i; j++ ){
				array[counter] = i+1;
				counter++;
			}
		}
		BinarySearch bs1 = new BinarySearch(array);
		for(int i = 0; i<arrayLimit; i++){
			System.out.println(bs1.solution(i+1));
		}
		
	}
}
