package codingpractise.generic;
import java.util.Arrays;

/**
 * 
 */

/**
 * In this 
 * http://www.programcreek.com/2012/11/quicksort-array-in-java/
 * @author arnavawasthi
 *
 */
public class QuickSort {
	
	public static void main(String[] args) {
//		int[] x = { 9, 2, 4, 7, 3, 7, 10 };
//		int[] x = {3, 1, 4, 1, 5, 9, 2, 6, 5, 3};
//		int[] x = {6, 5};
		int[] x = {3, 4, 6, 10, 8, 9, 7, 2, 5};
		System.out.println(Arrays.toString(x));
 
		int low = 0;
		int high = x.length - 1;
 
		quickSort(x, low, high);
		System.out.println(Arrays.toString(x));
	}
 
	public static void quickSort(int[] arr, int low, int high) {
		System.out.println("Low: "+low+", High: "+high);
		if (arr == null || arr.length == 0)
			return;
 
		if (low >= high)
			return;
 
		// pick the pivot
		int middle = low + (high - low) / 2;
		int pivot = arr[middle];
 
		
		// make left < pivot and right > pivot
		int i = low, j = high;
		while (i <= j) {
			while (arr[i] < pivot) {
				i++;
			}
 
			while (arr[j] > pivot) {
				j--;
			}
 
			if (i <= j) {
				int temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
				i++;
				j--;
			}
		}
 
		System.out.println(Arrays.toString(arr));
		
		System.out.println("i: "+i+", j: "+j);
//		System.exit(0);
		// recursively sort two sub parts
		if (low < j)
			quickSort(arr, low, j);
 
		if (high > i)
			quickSort(arr, i, high);
	}
}