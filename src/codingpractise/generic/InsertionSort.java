/**
 * 
 */
package codingpractise.generic;

import java.util.Arrays;

/**
 * @author arnavawasthi
 *
 */
public class InsertionSort {
	
	
	public static void solution(int[] a){
		
		for(int i=1; i < a.length; i++){
			for(int j = i; j >= 0; j--){
				if(a[i] < a[j] && (j == 0 || a[i] > a[j-1])){
					insert(a, i, j);
				}
			}
		}
	}
	
	/**
	 * As merge is an important subroutine of mergeSort, insert is an equally important subroutine of insertionSort.
	 * @param a
	 * @param elemIndex
	 * @param targetIndex
	 */
	public static void insert(int[] a, int elemIndex, int targetIndex){
		int elem = a[elemIndex];
		for(int k = elemIndex-1; k >= targetIndex; k--){
			a[k+1] = a[k];
		}
		a[targetIndex] = elem;
	}
	
	public static void main(String[] args) {
		int[] a = {3, 6, 2, 5};
		solution(a);
		System.out.println(Arrays.toString(a));
		
		int[] b = {1,2, 3, 4, 5, 6};
		solution(b);
		System.out.println(Arrays.toString(b));
		
		int[] c = {5, 4, 3, 2, 1, 0};
		solution(c);
		System.out.println(Arrays.toString(c));
	}

}
