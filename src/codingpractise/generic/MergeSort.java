/**
 * 
 */
package codingpractise.generic;

import java.util.Arrays;

/**
 * Better solution for this? In this solution it creates a new copy for sub array. Can it be done using mergeUnused sub-routine.
 * @MergeSort1.java is a better solution. It creates an auxiliary array of size equal to input array. It uses this auxiliary array
 * for keep the temporary sorted portion of array. And copy it to back to main array.
 * 
 * @author arnavawasthi
 *
 */
// 
public class MergeSort {

	
	public static int[] solution(int[] a){
		if(a.length <= 1){
			return a;
		}
		
		int mid = (a.length)/2 - 1;
		
		int[] a1 = solution(getArray(a, 0, mid));
		int[] a2 = solution(getArray(a, mid+1, a.length-1));
		
		return merge(a1, a2);
	}
	
	
	public static int[] merge(int[] a, int[] b){
		int[] r = new int[a.length + b.length];
		
		int i = 0, j = 0, k = 0;
		
		while (i < a.length && j < b.length){
			if(a[i] < b[j]){
				r[k] = a[i];
				i++;
			}else{
				r[k] = b[j];
				j++;
			}
			k++;
		}
		
		while(i < a.length){
			r[k] = a[i];
			k++;
			i++;
		}

		while(j < b.length){
			r[k] = b[j];
			k++;
			j++;
		}
		
		return r;
	}
	
	/**
	 * Incorrect merge because it doesn't check for index overflow of a[] and b[]
	 * @param a
	 * @param b
	 * @return
	 */
	public static int[] merge1(int[] a, int[] b){
		int[] c = new int[a.length + b.length];
		int i = 0, j = 0;
		for(int k = 0; k< c.length; k++){
			if(a[i] < b[j]){
				c[k] = a[i];
				i++;
			}else{
				c[k] = b[j];
				j++;
			}
		}
		return c;
	}
	
	/**
	 * @param a
	 * @param i
	 * @param mid
	 * @return
	 */
	private static int[] getArray(int[] a, int i, int j) {
		int c[] = new int[j-i+1];
		int x = 0, y = i;
		while(y <= j){
			c[x] = a[y];
			x++;
			y++;
		}
		return c;
	}
	
	public static int[] mergeUnused(int[] a, int i1, int j1, int i2, int j2){
		
		int l1 = j1-i1+1;
		int l2 = j2-i2+1;
		int s = l1 + l2;
		int[] r = new int[s];
		
		int i = i1, j = i2, k = 0;
		while (i <= j1 && j <= j2){
			if(a[i] <= a[j]){
				r[k] = a[i];
				i++;
			}else{
				r[k] = a[j];
				j++;
			}
			k++;
		}
		
		while(i < l1){
			r[k] = a[i];
			i++;
			k++;
		}
		
		while(j < l2){
			r[k] = a[j];
			j++;
			k++;
		}
		
		return r;
	}
	
	
	public static void main(String[] args) {
		int[] a = {1, 2, 5, 8, 10, 0, 3, 4};
		System.out.println(Arrays.toString(mergeUnused(a, 0, 4, 5, 7)));
		
		int[] a1 = {1, 2, 5, 9, 10, 15};
		int[] a2 = {0, 4, 6, 8, 12, 15, 19, 20};
		System.out.println(Arrays.toString(merge(a1, a2)));
//		System.out.println(Arrays.toString(merge1(a1, a2)));
		
		int[] b = {10, 6};
		System.out.println(Arrays.toString(mergeUnused(b, 0, 0, 1, 1)));
		System.out.println(Arrays.toString(solution(b)));
		

		int[] c = {6, 10}; //TODO: This case doesn't work
		System.out.println(Arrays.toString(mergeUnused(c, 0, 0, 1, 1)));
		System.out.println(Arrays.toString(solution(c)));
		
		int[] d = {10, 6, 5, 2};
		System.out.println(Arrays.toString(solution(d)));
		System.out.println(Arrays.toString(solution(a)));
	}
}
