/**
 * 
 */
package codingpractise.generic;

/**
 * @author arnavawasthi
 *
 */
//This is the text editor interface. 
//Anything you type or change here will be seen by the other person in real time.

//Least Significant Digit (LSD) Radix Sort

//unsorted: 170, 45, 75, 90, 802, 2, 24, 66
//all <= 999

//first pass: 170, 90, 802, 2, 24, 45, 75, 66

//2nd pass: 802, 2, 24, 45, 66, 170, 75, 90

//3rd pass: 002, 024, 045, 066, 075, 090, 170, 802

//O(n*k)

//n = number of elements in the array
//k = max digits in any number (3 for 999)

//0, 5, 5, 0, 2, 2, 4, 6 

//0 > 2
//1 > 0
//2 > 2
//3 > 0

//[0] -> 170, 90
//[1] -> nothing
//...
//[2] -> 802, 2
//...

import java.util.*;

public class RadixSort {
	public static void solution(int[] arr, int max) {
		int maxPass = String.valueOf(max).length();

		for (int pass = 0; pass < maxPass; pass++) {
			ArrayList<ArrayList<Integer>> digits = new ArrayList<ArrayList<Integer>>();
			for (int i = 0; i < 10; i++) {
				digits.add(new ArrayList<Integer>());
			}

			for (int i = 0; i < arr.length; i++) {
				int digit = getDigit(arr[i], pass);

				// System.out.println(String.format("elem: %d, pass: %d, digit: %d",
				// arr[i], pass, digit));
				digits.get(digit).add(arr[i]);
			}
			// System.out.println(digits);

			int counter = 0;
			for (ArrayList<Integer> list : digits) {
				for (Integer number : list) {
					arr[counter] = number;

					counter++;
				}
			}
			// System.out.println(Arrays.toString(arr));
		}

	}

	static int getDigit(int number, int index) {
		if (String.valueOf(number).length() < index + 1)
			return 0;

		int pass = 0;
		int digit = 0;
		while (pass <= index) {
			digit = number % 10;
			number = number / 10;

			pass++;
		}
		return digit;
	}

	public static void main(String[] args) {

		// System.out.println(getDigit(123, 5));

		int arr[] = { 170, 45, 75, 90, 802, 2, 24, 66 };
		int max = 999;

		// System.out.println(String.valueOf(max).length());

		solution(arr, max);

		System.out.println(Arrays.toString(arr));
	}

}
