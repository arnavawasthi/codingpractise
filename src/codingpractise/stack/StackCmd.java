package codingpractise.stack;
import java.util.*;

//12
//push 4
//pop
//push 3
//push 5
//push 2
//inc 3 1
//pop
//push 1
//inc 2 2
//push 4
//pop
//pop
/**
 * Implement command line stack. Commands will be provided one after another as shown above.
 * First line will be number of stack commands. After each stack command, print the top element 
 * from stack. If stack becomes empty after operation, print EMPTY.
 * 
 * push 4: pushes 4 onto stack and print 4
 * pop: pops and prints what is at top now or EMPTY
 * inc 3 1 (Increments bottom 3 elements by 1)
 * inc 2 3 (Increments bottom 2 elements by 3)
 * 
 * It fails for two HackerRank test cases (not sure, what would be those cases) - May be edge cases:
 * 1. Fewer args than required for a command
 * 2. Fewer commands than given by first line
 *  
 * @author arnavawasthi
 *
 */
public class StackCmd {
	
//	private static int isPresent(Node root, int val){
//		/* For your reference
//		class Node {
//				Node left, right;
//				int data;
//		                Node(int newData) {
//					left = right = null;
//					data = newData;
//				}
//			}
//		*/
//		    
//		    if(root.data == val) return 1;
//		    
//		    int inRight = 0;
//		    int inLeft = 0;
//		    if(root.left != null){
//		        inLeft = isPresent(root.left, val);
//		    }
//		    
//		    if(root.right != null){
//		        inRight = isPresent(root.right, val);
//		    }
//		    
//		    return Math.max(inLeft, inRight);
//		}
	
	
    public static void main(String args[] ) throws Exception {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT */
        
        Scanner s = new Scanner(System.in);
        int l = Integer.parseInt(s.nextLine());
        
        long[] stack = new long[l];
        int counter = 0;
        
        for(int i = 0; i < l; i++){
            String cmd = s.nextLine();
            String[] c = cmd.split(" ");
            
            if(c[0].equals("pop")){
                
            	if(c.length != 1) continue;
            	
            	if(counter == 0) return;
            	
                counter--;
                
            }else if(c[0].equals("push")){
            	if(c.length != 2) continue;
            	
                stack[counter] = Long.parseLong(c[1]);
                counter++;
            }else if(c[0].equals("inc")){
            	if(c.length != 3) continue;
            	
                int x = Integer.parseInt(c[1]);
                int inc = Integer.parseInt(c[2]);
                
                for(int j = 0; j < x; j++){
                    stack[j] += inc;
                }
            }
            
            if(counter == 0){
                System.out.println("EMPTY");
            }else{
                System.out.println(stack[counter-1]);
            }
            
        }
        
       
    }
}