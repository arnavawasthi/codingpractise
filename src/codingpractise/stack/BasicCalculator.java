/**
 * 
 */
package codingpractise.stack;

import java.util.Stack;

/**
 * TODO: Is there a better solution?
 * http://www.programcreek.com/2014/06/leetcode-basic-calculator-java/
 * 
 * @author arnavawasthi
 *
 */
public class BasicCalculator {

	public static int solution(String exp){
	
		char[] expChars = exp.toCharArray();
		int length = expChars.length;
		Stack<String> stack = new Stack<String>();
		
		for(int i=0; i<length; i++){
			char ch = expChars[i];
			if(ch == ' ' || ch == '+') continue;
			
			if(ch >= '0' && ch <= '9'){
				StringBuilder sb = new StringBuilder();
				while(i < length && expChars[i] >= '0' && expChars[i] <= '9'){
					sb.append(expChars[i]);
					i++;
				}
				
				i--;
				if(!stack.isEmpty() && stack.peek().equals("-")){
					String newStr = stack.pop()+sb.toString();
					stack.push(newStr);
				}else{
					stack.push(sb.toString());
				}
				sb = null;
			}else if(ch == ')'){
				int sum = 0;
				while(!stack.peek().equals("(")){
					sum += Integer.parseInt(stack.pop());
				}
				stack.pop();
				if(!stack.isEmpty() && stack.peek().equals("-")){
					stack.pop();
					sum = -sum;
				}
				stack.push(sum+"");
				
			}else{
				stack.push(ch+"");
			}
			
		}
		int sum = 0;
		while(!stack.isEmpty()){
			sum += Integer.parseInt(stack.pop());
		}
			
		return sum;
	}
	
	public static void main(String[] args) {
		System.out.println(solution("10 - 20 - ( 30 + 20 - 10 )"));
		
		System.out.println(solution("10 - 20 - 30 + 20 - 10"));
		System.out.println(solution("30 - 20 - 30 - 10"));
		
		System.out.println(solution("(1)"));
		
		System.out.println(solution("(1-(4-5))"));
	}
}
