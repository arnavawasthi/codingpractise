package codingpractise.stack;

import java.util.*;

/**
 * http://www.programcreek.com/2014/05/leetcode-basic-calculator-ii-java/
 * TODO: Solution given on the site is much shorter and doesn't use stack.
 * @author arnavawasthi
 * 
 */

public class BasicCalculator2 {

	// 3+22*2+2*3-4-4/2
	public static int solution(String exp) {

		char[] charArray = exp.toCharArray();
		Stack<String> stack = new Stack<String>();
		String number = "";
		for (int i = 0; i < charArray.length; i++) {
			char c = charArray[i];
			//This while loop extracts numbers by appending digits one by one
			//one while loop ends, when either a operator or string end is found.
			while (c >= '0' && c <= '9') {
				number += c;
				i++;
				if (i < charArray.length) {
					c = charArray[i];
				}else{
					break;
				}
			}

			//if last inserted value in the stack was a operator, we need to perform calculation for 
			// multiply and division. For subtraction, we need to negate the current value.
			String peekChar = (stack.isEmpty()) ? "" : stack.peek() + "";
			if (peekChar.equals("*") || peekChar.equals("/")) {
				String op = stack.pop();
				int firstValue = Integer.parseInt(stack.pop());
				int secondValue = Integer.parseInt(number);
				
				int output = 0;
				if (op.equals("*")) {
					output = firstValue * secondValue;
				} else {
					output = firstValue / secondValue;
				}
				stack.push(output + "");

			} else if (peekChar.equals("-")) {
				stack.pop();
				stack.push("+");
				stack.push("-" + number);
			} else {
				//in case of addition, just push the number.
				//Addition will be performed, after stack is built.
				stack.push(number);
			}
			//operation of number is done, reset it.
			number = "";
			stack.push(c+"");
			
		}
		
		//This pop is required, because after last number, last digit will also be pushed.
		stack.pop();

		System.out.println(stack);
		while (!stack.isEmpty()) {
			int secondValue = Integer.parseInt(stack.pop());
			//if there is only one value in stack, just return that as output.
			//Task: Guess when such case will arise.
			if (stack.isEmpty())
				return secondValue;

			//Since only operation left is addition, we don't need to check the operator.
			//TODO: ideally we should not even push it to stack. It will reduce runtime space usage.
			String op = stack.pop();
			Integer firstValue = Integer.parseInt(stack.pop());

			int output = firstValue + secondValue;
			stack.push(output + "");
			System.out.println(stack);
		}

		return 0;
	}

	public static void main(String args[]) {
		System.out.println(solution("3+22*2+2*3-4-4/2"));
		
		System.out.println(solution("3+22*2+2*3-4-4/2-543"));

	}
}
