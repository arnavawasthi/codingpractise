/**
 * 
 */
package codingpractise.stack;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Stack;

/**
 * TODO: This question was asked at SigFig as well. Try to implement solution without stack for one type of parenthesis.
 * For multiple types, you can use stack. But keeping all types in if conditions is bad. So try keeping pairs in instance
 * variables.
 * @author arnavawasthi
 *
 */

public class Balanced{
	
/*
 * Complete the function below.
 */

    static String[] braces(String[] values) {
        String[] output = new String[values.length];
        
        for(int index = 0; index < values.length; index++){
            Stack<Character> stack = new Stack<Character>();
            char[] chars = values[index].toCharArray();
            
            boolean balanced = true;
            for(char c: chars){
                if(c == '}' || c == ']' || c == ')'){
                    
                    if(!stack.isEmpty()){
                        char temp = stack.pop();
                        if(!((c == '}' && temp == '{') || ( c == ']' && temp == '[' )|| (c == ')' && temp == '('))){
                            balanced = false;
                            break; 
                        }
                        
                    }else{
                        balanced = false;
                        break;
                    }
                }else{
                    stack.push(c);
                }
            }
            
            if(!stack.isEmpty()){
                balanced = false;
            }
            
            if(balanced){
                output[index] = "YES";
            }else{
                output[index] = "NO";
                     
            }
            
        }
        
        return output;
    }
    
    
    
    /*
     * Complete the function below.
     */

        static int zombieCluster(String[] zombies) {
            ArrayList<HashSet<Integer>> clusters = new ArrayList<HashSet<Integer>>(zombies.length);
            for(String s: zombies){
                clusters.add(null);
            }
            int output = 0;
            for(int i = 0; i < zombies.length; i++){
               char[] arr = zombies[i].toCharArray();
               for(int j = 0; j < arr.length; j++){
                   if(arr[j] == '1'){
                       HashSet<Integer> cluster = clusters.get(i);
                       if(cluster == null){
                           cluster = clusters.get(j);
                       }
                       
                       if(cluster == null){
                           output++;
                           cluster = new HashSet<Integer>();
                           
                       }
                       cluster.add(i);
                       cluster.add(j);
                       clusters.set(i, cluster);
                       clusters.set(j, cluster);
                   }
               }
            }
            return output;
        }


}

