/**
 *
 */
package codingpractise.stack;

import java.util.*;
/**
 * http://www.programcreek.com/2014/04/leetcode-simplify-path-java/
 * @author arnavawasthi
 *
 */

public class SimplfyUnixPath{

  //In this code String.split() is used. It will be O(n) complexity function.
  //And since I have spliting string, I am using O(n) extra space apart from Stack.
  public static String solution(String path){
    String[] segs = path.split("/");
    Stack<String> stack = new Stack<String>();
    for(String seg: segs){
      if(seg.equals("") || seg.equals(".")) continue;
      if(seg.equals("..") && !stack.isEmpty()){
    	  stack.pop();
      }else{
    	  stack.add(seg);
      }


    }
    Stack<String> revStack = new Stack<String>();
    while(!stack.isEmpty()){
    	revStack.add(stack.pop());
    }
    StringBuilder simplePath = new StringBuilder();

    while(!revStack.isEmpty()){
        simplePath.append("/").append(revStack.pop());
    }

    return simplePath.toString();
  }

  public static void main(String[] args){
    System.out.println(solution("//b/d/../../c/..///"));
  }
}
