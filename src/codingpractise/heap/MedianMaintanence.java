/**
 * 
 */
package codingpractise.heap;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * http://www.programcreek.com/2015/01/leetcode-find-median-from-data-stream-java/
 * 
 * Week 6: 
 * Question 2:
 * The goal of this problem is to implement the "Median Maintenance" algorithm
 * (covered in the Week 5 lecture on heap applications). The text file contains
 * a list of the integers from 1 to 10000 in unsorted order; you should treat
 * this as a stream of numbers, arriving one by one. Letting xi denote the ith
 * number of the file, the kth median mk is defined as the median of the numbers
 * x1,…,xk. (So, if k is odd, then mk is ((k+1)/2)th smallest number among
 * x1,…,xk; if k is even, then mk is the (k/2)th smallest number among x1,…,xk.)
 * 
 * In the box below you should type the sum of these 10000 medians, modulo 10000
 * (i.e., only the last 4 digits). That is, you should compute
 * (m1+m2+m3+⋯+m10000)mod10000.
 * 
 * OPTIONAL EXERCISE: Compare the performance achieved by heap-based and
 * search-tree-based implementations of the algorithm. Program will get stream
 * of numbers. For every new input program should compute new Median for all
 * numbers seen so far. Desired complexity: O(log(n))
 * 
 * @author arnavawasthi
 * 
 */
public class MedianMaintanence {

	public static void main(String[] args) throws FileNotFoundException {
		MinHeap mi = new MinHeap(5000);
		MaxHeap mx = new MaxHeap(5000);
		int medianSum = 0;
		List<Integer> medians = new ArrayList<Integer>();
		Scanner s = new Scanner(new File("/Users/arnavawasthi/Downloads/Median.txt"));
		while(s.hasNextLine()){
			int x = Integer.parseInt(s.nextLine());
			//putting first element into MaxHeap
			if(mx.size == 0){
				mx.add(x); 
				medianSum += x;
				medians.add(x);
				continue;
			}
			
			if(mx.getMax() > x){
				mx.add(x);
			}else{
				mi.add(x);
			}
			
			//MaxHeap - MinHeap should not be more than 1.
			if(mx.size - mi.size > 1){
				mi.add(mx.extractMax());
			}
			
			if(mi.size > mx.size){
				mx.add(mi.extractMin());
			}
			medianSum += mx.getMax();
			medians.add(mx.getMax());
//			System.out.println(mx);
//			System.out.println(mi);
//			System.out.println("Median: "+mx.getMax());
//			if(mx.size > 15) break;
			
		}
		s.close();
		System.out.println(medians.size());
		System.out.println(medianSum%10000);
		
		
		ArrayList<Integer>[] lists = new ArrayList[10];
		
		
	}
}
