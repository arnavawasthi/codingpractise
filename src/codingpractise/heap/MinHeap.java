/**
 * 
 */
package codingpractise.heap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * 
 * http://ideone.com/A6ZvUM
 * @author arnavawasthi
 *
 */
public class MinHeap {

	public int[] heap;
	public int size = 0;
	
	public MinHeap(){
		this(5);
	}
	
	public MinHeap(int n){
		heap = new int[n];
	}
	
	public MinHeap(int a[]){
		heapify(a);
		heap = a;
		size = a.length;
	}
	
	public void add(int a){
		heap[size] = a;
		int childIndex = size;
		int parentIndex = (childIndex-1)/2;
		
		while(parentIndex >= 0 && heap[childIndex] < heap[parentIndex]){
			int temp = heap[parentIndex];
			heap[parentIndex] = heap[childIndex];
			heap[childIndex] = temp;
			
			childIndex = parentIndex;
			parentIndex = (childIndex-1)/2;
		}
		
//		System.out.println(Arrays.toString(heap));
		size++;
	}
	
	
	public void delete(){
		throw new RuntimeException("Unsupported operation");
	}
	
	//{16, 14, 10, 8, 7};
	private void heapifyHelper(int[] a, int node){
		if(node > a.length-1) return;
		int lI = 2*node+1;
		int rI = 2*node+2;
//		heapify(a, lI);
//		heapify(a, rI);
		
		int sI = -1;
		if(lI > a.length-1 ) return;
		
		if(rI < a.length && a[rI] < a[lI]){
			sI = rI;
		}else{
			sI = lI;
		}
		
		if(a[sI] < a[node]){
			int temp = a[node];
			a[node] = a[sI];
			a[sI] = temp;
		}
		heapifyHelper(a, sI);
		
		
	}
	
	//bottom-up approach
	public void heapify(int[] a){
		//One optimization is to start from length/2. It means don't process for leaf nodes.
		for(int i = a.length-1; i>=0; i--){
			heapifyHelper(a, i);
		}
	}
	
	//Code looks ugly
	public int extractMin(){
		int min = heap[0];
		heap[0] = heap[size-1];
		heap[size-1] = 0;
		size--;
		//bubble down
		int curIndex = 0;
		int leftIndex = 2*curIndex+1;
		int rightIndex = 2*curIndex+2;
		while((size > leftIndex && heap[curIndex] > heap[leftIndex])  || (size > rightIndex && heap[curIndex] > heap[rightIndex])){
			int swapIndex = -1;
			if(size > rightIndex && heap[leftIndex] > heap[rightIndex]){
				swapIndex = rightIndex;
			}else{
				swapIndex = leftIndex;
			}
			int temp = heap[curIndex];
			heap[curIndex] = heap[swapIndex];
			heap[swapIndex] = temp;
			
			curIndex = swapIndex;
			leftIndex = 2*curIndex+1;
			rightIndex = 2*curIndex+2;
			
		}
		
		return min;
	}
	
	/**
	 * getMin just returns the min without deleting it from the Heap.
	 * @return
	 */
	public int getMin(){
		return heap[0];
	}
	
	/**
	 * Implement an in-place heapsort (Space: 0(1)). It looks like we will take O(n) space first, to create a heap. 
	 * And then extractMin from heap 'n' times and put that into original array. This solution runs in 0(n*log(n)) 
	 * but takes 0(n) extra storage.
	 */
	public void sort(){
		for(int i = heap.length-1; i>=0; i--){
			int min = extractMin();
			heap[i] = min;
		}
		
//		System.out.println(Arrays.toString(heap));
	}
	
	public String toString(){
		List<Integer> sb = new ArrayList<Integer>();
		for(Integer x: heap){
			if(x == 0) break;
			sb.add(x);
		}
		return sb.toString();
	}
	
	public static void main(String[] args) {
		MinHeap mh = new MinHeap();
		mh.add(10);
		mh.add(5);
		mh.add(8);
		mh.add(7);
		System.out.println(mh.extractMin());
		mh.add(4);
		System.out.println(mh.extractMin());
		
		
		//testing heapify
		int a[] = {16, 14, 10, 8, 7};
		mh.heapify(a);
		System.out.println(Arrays.toString(a));
		
		//testing sort (in place)
		System.out.println("Sorting ...");
		int b[] = {10, 5, 13, 11, 9, 7, 18, 16};
		MinHeap h = new MinHeap(b);
		System.out.println(Arrays.toString(b));
		h.sort();
		
	}
}