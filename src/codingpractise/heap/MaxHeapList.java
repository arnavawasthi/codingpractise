/**
 * 
 */
package codingpractise.heap;

import java.util.ArrayList;
import java.util.List;

/**
 * @author arnavawasthi
 * 
 */
public class MaxHeapList {
	List<Integer> heap;

	public MaxHeapList() {
		heap = new ArrayList<Integer>();
	}

	public MaxHeapList(List<Integer> list) {
		heap = list;
		System.out.println(heap);
		buildMaxHeap();
		System.out.println(heap);
	}

	private void buildMaxHeap() {
		Integer size = heap.size();

		for (int i = size / 2; i >= 0; i--) {
			maxHeapify(i);
		}
	}

	public Integer extractMax() {
		Integer max = heap.get(0);
		int lastIndex = heap.size() - 1;
		Integer last = heap.get(lastIndex);

		// Remove of List is problematic. If i use Integer wrapper class for
		// lastIndex, it deletes by value.
		// So I am using primitive "int" for index.
		heap.remove(lastIndex);

		heap.set(0, last);
		maxHeapify(0);

		return max;
	}

	public void maxHeapify(Integer root) {
		Integer left = 2 * root + 1;
		Integer right = 2 * root + 2;
		Integer maxIndex = heap.size() - 1;

		// Already at leaf node
		if (left > maxIndex)
			return;

		Integer rootValue = heap.get(root);
		Integer leftValue = heap.get(left);

		// has only one node (left node)
		if (right > maxIndex) {
			if (leftValue > rootValue) {
				heap.set(root, leftValue);
				heap.set(left, rootValue);
			}

			return;
		}

		Integer rightValue = heap.get(right);

		if (rootValue >= leftValue && rootValue >= rightValue) {
			return;
		}

		if (leftValue > rightValue) {
			heap.set(root, leftValue);
			heap.set(left, rootValue);
			maxHeapify(left);
		} else {
			heap.set(root, rightValue);
			heap.set(right, rootValue);
			maxHeapify(right);
		}

	}

}