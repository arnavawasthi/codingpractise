/**
 * 
 */
package codingpractise.heap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author arnavawasthi
 *
 */
public class MaxHeap {
	
	public int[] heap;
	public int size;
	
	public MaxHeap(){
		this(5);
	}
	
	public MaxHeap(int n){
		heap = new int[n];
		size = 0;
	}
	
	public MaxHeap(int[] array){
		heap = array;
		size = heap.length;
		heapifyArray(heap);
	}
	
	/**
	 * @param heap
	 */
	private void heapifyArray(int[] heap) {
		for(int i = size/2; i >= 0; i--){
			heapify(i);
		}
	}

	/**
	 * @param root
	 */
	private void heapify(int root) {
		int left = root*2 + 1;
		int right = root*2 + 2;
		
		if(left >= size) return;
		
		int rootElem = heap[root];
		int leftElem = heap[left];
		
		if(right < size){
			int rightElem = heap[right];
			if(rootElem < rightElem || rootElem < leftElem){
				if(rightElem > leftElem){
					swap(root, right);
					heapify(right);
				}else{
					swap(root, left);
					heapify(left);
				}
			}else{
				return;
			}
		}else{
			if(rootElem < leftElem){
				swap(root, left);
				heapify(left);
			}else{
				return;
			}
		}
 		
	}

	public int extractMax(){
		int max = heap[0];
		heap[0] = heap[size-1];
		heap[size-1] = 0;
		size--;
		
		int p = 0;
		int l = left(p);
		int r = right(p);
		
		while((l < size && heap[l] > heap[p]) || (r < size && heap[r] > heap[p])){
			int swap = -1;
			if(r < size && heap[r] > heap[l]){
				swap = r;
			}else{
				swap = l;
			}
			
			swap(swap, p);
			p = swap;
			l = left(p);
			r = right(p);
		}
//		System.out.println(Arrays.toString(heap));
		return max;
	}
	
	public void replaceMax(Integer x){
		heap[0] = x;
		heapify(0);
	}
	
	public void add(int a){
		int cur = size;
		heap[cur] = a;
		int p = parent(cur);
		while(p >= 0 && heap[p] < heap[cur]){
			swap(p, cur);
			cur = p;
			p = parent(cur);
		}
		size++;
//		System.out.println(Arrays.toString(heap));
	}
	
	public int getMax(){
		return heap[0];
	}
	
	private int left(int i){
		return 2*i + 1;
	}
	
	private int right(int i){
		return 2*i + 2;
	}
	
	private int parent(int i){
		return (i-1)/2;
	}
	
	private void swap(int i, int j){
		int temp = heap[i];
		heap[i] = heap[j];
		heap[j] = temp;
	}
	
	public String toString(){
		List<Integer> sb = new ArrayList<Integer>();
		for(Integer x: heap){
			if(x == 0) break;
			sb.add(x);
		}
		return sb.toString();
	}
	
	public static void main(String[] args) {
		MaxHeap mx = new MaxHeap(10);
		mx.add(10);
		mx.add(4);
		mx.add(6);
		mx.add(2);
		mx.add(12);
		mx.add(15);
		mx.extractMax();
		mx.extractMax();
		mx.extractMax();
		mx.extractMax();
		mx.extractMax();
		
	}

}
