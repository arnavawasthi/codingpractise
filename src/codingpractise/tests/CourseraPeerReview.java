package codingpractise.tests;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 * 
 */

/**
 * @author arnavawasthi
 *
 */
public class CourseraPeerReview {
	
	private Integer duration;
	private int[] learnerIds;
	private List<Learner> learners;
	private List<Submission> openSubmissions;
	private Map<Integer, List<Submission>> allSubmissions;
	
	private final int submissionTime = 50;
	private final int reviewTime = 20;
	private final int passGrade = 240;
	private final int resubmissionIncremental = 15;
	

	public CourseraPeerReview(Integer duration, List<Learner> learners){
		this.duration = duration;
		this.learners = learners;
		
		learnerIds = new int[learners.size()];
		for(int index = 0; index < learners.size(); index++){
			learnerIds[index] = learners.get(index).learnerId;
		}
		Arrays.sort(learnerIds);
		
		openSubmissions = new ArrayList<Submission>();
		allSubmissions = new HashMap<Integer, List<Submission>>();
	}
	
	public void runSimulation(){
		
		for(int tick = 0; tick < duration; tick++){
			
			List<Submission> currentTickSubmissions = new ArrayList<Submission>();
			
			for(int learnerId: learnerIds){

				List<Submission> submissions = allSubmissions.get(learnerId);
				Submission lastSubmission = null;
				if(submissions != null && submissions.size() > 0){
					lastSubmission = submissions.get(submissions.size()-1);
				}else{
					submissions = new ArrayList<Submission>();
					allSubmissions.put(learnerId, submissions);
				}
				
				//TODO: Won't work. Learners may not have ids from 0 to N
				Learner learner = learners.get(learnerId);
				
//				System.out.println(learner);
				
				if(learner.firstSubmissionStartTime > tick || tick < learner.busyUntil){
					continue;
				}else if(learner.firstSubmissionStartTime == tick){
					workOnSubmission(tick, learner);
					continue;
				}
				
				
				
				if(tick == learner.busyUntil){
					
//					System.out.println("Tick: "+tick);
					if(!learner.submitted){
						lastSubmission.submissionTick = tick;
						currentTickSubmissions.add(lastSubmission);
						learner.submitted = true;
						learner.waitingForReview = true;
						learner.submissions += 1;
						
						
					}else{
						
						Submission reviewSubmission = getLatestSubmission(learner.reviewLearnerId);
						Learner submissionLearner = getLearner(learner.reviewLearnerId);
						

						int score = submissionLearner.submissionTrueGrade + learner.reviewBias;
						if(score < 0) score = 0;
						if(score > 100) score = 100;
						
						reviewSubmission.scoreSum += score;
						
						reviewSubmission.reviewing -= 1;
						reviewSubmission.reviewed += 1;
						reviewSubmission.reviewedBy.add(learnerId);
						
//						System.out.println(String.format("learnerId: %d, tick: %d, reviewerId: %d, score: %d", submissionLearner.learnerId, tick, learnerId, score));
						
						if(reviewSubmission.reviewed == 3){
							reviewSubmission.gradeTick = tick;
							
							if(reviewSubmission.scoreSum < passGrade){
								submissionLearner.failed = true;
								submissionLearner.waitingForReview = false;
								submissionLearner.submissionTrueGrade += resubmissionIncremental;
								
								workOnSubmission(tick, submissionLearner);
							}
							//TODO: remove from open submissions
						}
						
						learner.reviews += 1;
						learner.reviewLearnerId = -1;

						if(learner.submitted && learner.failed){
							workOnSubmission(tick, learner);
						}
					}
					
					
//					workOnReview(tick, learner);
					
					continue;
				}
				
				if(!learner.submitted || (learner.failed && !learner.waitingForReview)){
					workOnSubmission(tick, learner);
					
					continue;
				}
				
//				workOnReview(tick, learner);
				
			}
			
			for(int learnerId: learnerIds){
				Learner learner = getLearner(learnerId);
				workOnReview(tick, learner);
			}
			
			openSubmissions.addAll(currentTickSubmissions);
		}
		
		
		for(Integer learnerId: learnerIds){
			for(Submission submission: allSubmissions.get(learnerId)){
				if(submission.submissionTick > 0)
					submission.print();
			}
		}
		
		
	}

	/**
	 * @param tick
	 * @param learnerId
	 * @param learner
	 */
	private void workOnReview(int tick, Learner learner) {
		if(learner.reviews < 3*learner.submissions && learner.busyUntil <= tick && learner.reviewLearnerId == -1){
			//finding submission to review
			for(Submission openSubmission: openSubmissions){
//				if(learner.learnerId == 4){
//					System.out.println(String.format("tick: %d, reviewingId: %d, reviewed: %d, reviewing: %d, reviewers: %s", tick, learner.reviewLearnerId, openSubmission.reviewed, openSubmission.reviewing, openSubmission.reviewedBy));
//					
//				}
				if(isAvailableForReview(openSubmission, learner.learnerId)){
					
//					if(openSubmission.learnerId == 2){
//						System.out.println(String.format("tick: %d, reviewingId: %d, reviewed: %d, reviewing: %d, reviewers: %s", tick, learner.learnerId, openSubmission.reviewed, openSubmission.reviewing, openSubmission.reviewedBy));
//						
//					}
					learner.reviewLearnerId = openSubmission.learnerId;
					learner.busyUntil = tick + reviewTime;
					
					openSubmission.reviewing += 1;
					
					break;
				}
			}
			
		}
	}

	/**
	 * @param tick
	 * @param learner
	 * @param submissions
	 * @param lastSubmission
	 */
	private void workOnSubmission(int tick, Learner learner) {
		
		if(learner.busyUntil > tick || learner.waitingForReview) return;
		
//		if(learner.learnerId == 0){
//			System.out.println(String.format("tick: %d", tick));
//		}		

		int learnerId = learner.learnerId;
		
		List<Submission> submissions = allSubmissions.get(learnerId);
		Submission lastSubmission = null;
		if(submissions != null && submissions.size() > 0){
			lastSubmission = submissions.get(submissions.size()-1);
		}else{
			submissions = new ArrayList<Submission>();
			allSubmissions.put(learnerId, submissions);
		}
		
		Submission submission = new Submission();
		submission.gradeTick = -1;
		submission.learnerId = learnerId;
		submission.sequenceNumber = (lastSubmission == null)? 0: lastSubmission.sequenceNumber+1;
		submissions.add(submission);
		
		learner.busyUntil = tick + submissionTime;
		learner.submitted = false;
	}

	/**
	 * @param openSubmission
	 * @return
	 */
	private boolean isAvailableForReview(Submission openSubmission, int learnerId) {
		if(openSubmission.learnerId != learnerId 
			&& openSubmission.reviewed + openSubmission.reviewing < 3
			&& !openSubmission.reviewedBy.contains(learnerId))
		{
			
			return true;
		}
		
		return false;
	}

	/**
	 * @param learnerId
	 * @return
	 */
	private Learner getLearner(int learnerId) {
		
		for(Learner learner: learners){
			if(learner.learnerId == learnerId) return learner;
			
		}
		return null;
	}

	/**
	 * @param reviewSubmissionId
	 * @return
	 */
	private Submission getLatestSubmission(int learnerId) {
		
		List<Submission> submissions = allSubmissions.get(learnerId);
		Submission lastSubmission = submissions.get(submissions.size()-1);
		
		return lastSubmission;
	}
	
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
	
		int duration = Integer.parseInt(scanner.nextLine());
		int learnersCount = Integer.parseInt(scanner.nextLine());
		
		List<Learner> learners = new ArrayList<Learner>();
		
		for(int i = 0; i < learnersCount; i++){
			
			String[] learnerDescStr = scanner.nextLine().split(" ");
			
			Learner learner = new Learner();
			learner.learnerId = Integer.parseInt(learnerDescStr[0]);
			learner.firstSubmissionStartTime = Integer.parseInt(learnerDescStr[1]);
			learner.submissionTrueGrade = Integer.parseInt(learnerDescStr[2]);
			learner.reviewBias = Integer.parseInt(learnerDescStr[3]);
			
			
			learners.add(learner);
			
		}
		scanner.close();
		
		CourseraPeerReview solution = new CourseraPeerReview(duration, learners);
		solution.runSimulation();
		
	}
	
}

class Learner{
	int learnerId;
	int firstSubmissionStartTime;
	int submissionTrueGrade;
	int reviewBias;
	
	boolean waitingForReview;
	boolean submitted;
	boolean failed;
	int busyUntil;
	int reviewLearnerId = -1;
	
	int submissions;
	int reviews;
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Learner [learnerId=" + learnerId + ", waitingForReview="
				+ waitingForReview + ", submitted=" + submitted + ", passed="
				+ failed + ", busyUntil=" + busyUntil + "]";
	}
	
	
	
	
}

class Submission{
	
	int learnerId;
	int sequenceNumber;
	int submissionTick;
	int scoreSum;
	int gradeTick;
	
	int reviewed;
	int reviewing;
	
	Set<Integer> reviewedBy = new HashSet<Integer>();
	
	/**
	 * 
	 */
	public void print() {
		System.out.println(learnerId+" "+sequenceNumber+" "+submissionTick+" "+scoreSum+" "+gradeTick);
	}
	
}