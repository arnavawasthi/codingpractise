/**
 * 
 */
package codingpractise.string;

import java.util.ArrayList;
import java.util.List;

/**
 * http://www.programcreek.com/2014/05/leetcode-group-shifted-strings-java/
 * @author arnavawasthi
 *
 */
public class GroupShifters {
	
	public static List<List<String>> solution(String[] strs){
		List<List<String>> grouped = new ArrayList<List<String>>();
		for(int i = 0; i < strs.length; i++){
			if(strs[i].equals("")) continue;
			List<String> group = new ArrayList<String>();
			for(int j = i+1; j < strs.length; j++){
				if(inGroup(strs[i], strs[j])){
					group.add(strs[j]);
					strs[j] = "";
				}
			}
			group.add(strs[i]);
			
			grouped.add(group);
		}
		
		return grouped;
	}
	
	static boolean inGroup(String s, String t){
		
		if(s.length() != t.length()) return false;
		
		char[] sa = s.toCharArray();
		char[] ta = t.toCharArray();
		
		int d = getDiff(sa[0], ta[0]);
		
		for(int i = 1; i < sa.length; i++){
			int di = getDiff(sa[i], ta[i]);
//			System.out.println(String.format("%s - %s = %d", sa[i], ta[i], di));
			
			if(d != di){
				return false;
			}
		}
		
		return true;
	}
	
	private static int getDiff(char a, char b){
		int d = a - b;
		if(d < 0){
			d += 26;
		}
		
		return d;
				
	}
	
	public static void main(String[] args) {
		String[] strs = new String[]{"abc", "az", "bcd", "acef", "a", "ba", "xyz"};
		
		System.out.println(solution(strs));
		
//		System.out.println(inGroup("az", "ba"));
	}

}
