/**
 * 
 */
package codingpractise.string;

/**
 * @author arnavawasthi
 *
 */
public class SayWhatYouSee {

	/*
	 * Complete the function below.
	 */

	    static String[] say_what_you_see(String[] input_strings) {
	        String[] output = new String[input_strings.length];
	        
	        for(int i = 0; i < input_strings.length; i++){
	        
	            char[] chars = input_strings[i].toCharArray();

	            int counter = 0;
	            char c = chars[0];
	            String o = "";
	            for(char x: chars){
	                if(x == c){
	                  counter++; 
	                }else{
	                    o = o+counter+c;
	                    counter = 1;
	                    c = x;
	                }
	            }
	            //This is need for the last digit (or last repeating digit)
	            o = o+counter+c;
	            output[i] = o;
	            o = "";
	    
	        }
	        
	        return output;
	        
	    }


}
