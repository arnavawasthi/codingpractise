/**
 * 
 */
package codingpractise.string;

import java.util.HashMap;
import java.util.Map;

/**
 * http://www.programcreek.com/2014/05/leetcode-valid-anagram-java/
 * @author arnavawasthi
 *
 */
public class Anagram {
	
	public static boolean solution(String s, String t){
		
		if(s.length() != t.length()) return false;
		
		Map<Character, Integer> sm = new HashMap<Character, Integer>();
		char[] sc = s.toCharArray();
		for(char c: sc){
			int count = (sm.containsKey(c))?sm.get(c)+1:1;
			sm.put(c, count);
		}
		
		char[] tc = t.toCharArray();
		
		for(char c: tc){
			if(sm.containsKey(c)){
				int count = sm.get(c) - 1;
				if(count == 0){
					sm.remove(c);
				}else{
					sm.put(c, count);
				}
			}else{
				return false;
			}
			
		}
		
		
		
		return true;
	}
	
	public static void main(String[] args) {
		System.out.println(solution("cat", "tac"));
		System.out.println(solution("cat", "bat"));
		System.out.println(solution("cater", "raket"));
		System.out.println(solution("cater", "racet"));
		
		
	}

}
