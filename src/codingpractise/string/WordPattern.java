/**
 * 
 */
package codingpractise.string;

import java.util.HashMap;
import java.util.Map;

/**
 * https://leetcode.com/problems/word-pattern/
 * http://www.programcreek.com/2014/05/leetcode-word-pattern-java/
 * @author arnavawasthi
 *
 */
public class WordPattern {

	public static boolean solution(String str, String pat){
		char[] patC = pat.toCharArray();
		String[] strS = str.split(" ");
		if(strS.length != patC.length) return false;
		
		Map<Character, String> map = new HashMap<Character, String>();
		for(int i = 0; i < strS.length; i++){
			String s = strS[i];
			Character c = patC[i];
			
			if(map.containsKey(c)){
				if(!s.equals(map.get(c))){
					return false;
				}
			}else{
				map.put(c, s);
			}
		}
		
		
		return true;
	}
	
	public static void main(String[] args) {
		{
			String s = "dog cat cat dog";
			String p = "abba";
			
			System.out.println(solution(s, p));
		}
		
		{
			String s = "dog cat cat fish";
			String p = "abba";
			
			System.out.println(solution(s, p));
		}
	}
}
