/**
 * 
 */
package codingpractise.list;

/**
 * http://www.programcreek.com/2015/03/leetcode-odd-even-linked-list-java/
 * https://leetcode.com/problems/odd-even-linked-list/
 * @author arnavawasthi
 *
 */
public class OddEvenLinkedList {

	public static Node solution(Node root){
		if(root == null) return null;
		
		Node even = root;
		Node odd = root.next;
		Node oddFirst = odd;
		Node evenFirst = even;
		
		/*
		 * if odd.next is null, we have reached the last node of list. So come out of while.
		 */
		while(even != null && odd != null && odd.next != null){

			even.next = odd.next;
			even = odd.next;
			
			odd.next = even.next;
			odd = even.next;
			
		}
		
		even.next = oddFirst;
		
		return evenFirst;
	}
	
	public static void main(String[] args) {
		{
			int[] a = {2, 1, 4, 5, 6, 3, 7};
			MyLinkedList list = new MyLinkedList(a);
			solution(list.root).print();
		}
		
		{
			int[] a = {2, 1, 4, 5, 6, 3};
			MyLinkedList list = new MyLinkedList(a);
			solution(list.root).print();
		}
	}
}
