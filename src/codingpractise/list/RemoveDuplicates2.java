/**
 * 
 */
package codingpractise.list;

/**
 * http://www.programcreek.com/2014/06/leetcode-remove-duplicates-from-sorted-list-ii-java/
 * @author arnavawasthi
 *
 */
public class RemoveDuplicates2 {
	
	public static Node solution(Node root){
		Node result = null;
		Node newList = null;
		
		Node pointer = root;
		int dupData = 0;
		while(pointer != null && pointer.next != null){
			if(pointer.data != dupData && pointer.data != pointer.next.data){
				if(result == null){
					result = pointer;
					newList = pointer;
				}else{
					newList.next = pointer;
					newList = pointer;
				}
			}else{
				dupData = pointer.data;
			}
			
			pointer = pointer.next;
		}
		return result;
	}
	
	/*
	 * By initialzing result/newList and returning result.next, we got rid of if/else inside the if block.
	 */
	public static Node solution1(Node root){
		Node result = new Node();
		Node newList = result;
		
		int dupData = 0;//non existent in the list. You may choose NULL as well.
		Node pointer = root;
		while(pointer != null && pointer.next != null){
			if(dupData != pointer.data && pointer.data != pointer.next.data){
				newList.next = pointer;
				newList = newList.next;
			}else{
				dupData = pointer.data;
			}
			pointer = pointer.next;
		}
		
		
		return result.next;
	}
	
	public static void main(String[] args) {
		{
			int[] a = {1,1,1,1,2,3,4,4,4,5,5,5,6,6,6,7,8,9};
			MyLinkedList list = new MyLinkedList(a);
			solution(list.root).print();
			solution1(list.root).print();
		}
	}

}
