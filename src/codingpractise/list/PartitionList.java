/**
 * 
 */
package codingpractise.list;

/**
 * http://www.programcreek.com/2013/02/leetcode-partition-list-java/
 * @author arnavawasthi
 *
 */
public class PartitionList {

	public static Node solution(Node root, int x){
		Node lowRoot = null, lowList = null, highRoot = null, highList = null;
		Node pivot = null;
		while(root != null){
			if(root.data < x){
				if(lowRoot == null){
					lowRoot = root;
				}else{
					lowList.next = root;
				}
				lowList = root;
			}else if(root.data > x){
				if(highRoot == null){
					highRoot = root;
				}else{
					highList.next = root;
				}
				highList = root;
			}else{
				pivot = root;
			}
			root = root.next;
		}
		
		//Setting last node's next to null (terminating the list)
		lowList.next = null;
		highList.next = null;
		
		lowRoot.print();
		highRoot.print();
		
		lowList.next = pivot;
		pivot.next = highRoot;
		
		return lowRoot;
	}
	
	public static void main(String[] args) {
		{
			int[] a = {4, 2, 5, 1, 7, 2, 3, 1, 4, 9};
			MyLinkedList list = new MyLinkedList(a);
			solution(list.root, 4).print();
		}
	}
}
