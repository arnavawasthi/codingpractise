/**
 * 
 */
package codingpractise.list;

/**
 * @author arnavawasthi
 *
 */
public class MyLinkedList {

	Node root;
	Node end;

	/**
	 * Initialized a blank linked list
	 */
	public MyLinkedList(){
		
	}
	
	/**
	 * Initializes a list with the given array
	 * @param a
	 */
	public MyLinkedList(int[] a){
		root = new Node();
		root.data = a[0];
		end = root;
		
		for(int i = 1; i < a.length; i++){
			Node n = new Node();
			n.data = a[i];
			add(n);
		}
	}
	
	public void add(Node n){
		if(root == null){
			root = n;
			end = n;
			n.next = null;
			return;
		}
		end.next = n;
		end = n;
	}
	
	public void reverse(){
		end = root;

		Node prev = null;
		Node cur = root;
		while(cur != null){
			//Saves the next pointer, otherwise next of current node will be lost
			Node temp = cur.next;
			cur.next = prev;
			prev = cur;
			
			cur = temp;
		}
		root = prev;
	}
	
	public void print(){
		Node root = this.root;
		while (root != null) {
			System.out.print(root.data+" ");
			root = root.next;
		}
		System.out.println();
	}
	

	
	public static void main(String[] args) {
		{
			int a[] = {3, 4, 5, 6};
			MyLinkedList list = new MyLinkedList(a);
			list.print();
			list.reverse();
			list.print();
		}
	}
}

class Node{
	Integer data;
	Node next;
	
	public void print(String str){
		Node root = this;
		int counter = 0;
		System.out.print(str+"");
		while (root != null) {
			System.out.print(root.data+"->");
			root = root.next;
			
			if(++counter > 20) break;
		}
		System.out.println("NULL");
	}
	public void print(){
		print("");
	}
}
