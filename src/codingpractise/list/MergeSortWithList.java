/**
 * 
 */
package codingpractise.list;

/**
 * http://www.programcreek.com/2012/12/leetcode-merge-two-sorted-lists-java/
 * @author arnavawasthi
 *
 */
public class MergeSortWithList {

	public static Node sort(Node root){
		
		if(root.next == null) return root;
		
		Node mid = findMid(root);
		
//		System.out.println(mid.data);
		Node two = mid.next;
		mid.next = null;
		root = sort(root);
		two = sort(two);
//		
		return merge(root, two);
		
	}
	
	private static Node findMid(Node root){
		Node fast = root, slow = root;
		fast = fast.next;
		while(fast != null && fast.next != null){
			fast = fast.next.next;
			slow = slow.next;
		}
		
		return slow;
	}
	
	
	public static Node merge(Node oneRoot, Node twoRoot){
		Node retRoot = null;
		Node retEnd = null; 
		
		while(oneRoot != null && twoRoot != null){
			
			Node add = null;
			if(twoRoot.data < oneRoot.data){
				Node temp = twoRoot.next;
				add = twoRoot;
				twoRoot = temp;
				
			}else{
				Node temp = oneRoot.next;
				add = oneRoot;
				oneRoot = temp;
			}
			
			if(retRoot == null){
				retRoot = add;
				retEnd = retRoot;
			}else{
				
				retEnd.next = add;
				retEnd = add;
			}
		}
		
		if(oneRoot != null) retEnd.next = oneRoot;
		if(twoRoot != null) retEnd.next = twoRoot;
		
//		retRoot.print();
		return retRoot;
	}
	
	
//	public static MyLinkedList merge(MyLinkedList one, MyLinkedList two){
//		Node oneRoot = one.root;
//		Node twoRoot = two.root;
//		Node retRoot = (oneRoot.data < twoRoot.data)?oneRoot:twoRoot;
//		
//		MyLinkedList ret = new MyLinkedList();
//		ret.root = retRoot;
//		ret.end = retRoot;
//		
//		while(oneRoot != null && twoRoot != null){
//			if(twoRoot.data < oneRoot.data){
//				Node temp = twoRoot.next;
//				ret.add(twoRoot);
//				twoRoot = temp;
//				
//			}else{
//				Node temp = oneRoot.next;
//				ret.add(oneRoot);
//				oneRoot = temp;
//			}
//		}
//		
//		if(oneRoot != null) ret.end.next = oneRoot;
//		if(twoRoot != null) ret.end.next = twoRoot;
//		
//		return ret;
//	}
	
	public static void main(String[] args) {
//		{
//			int[] a1 = {2, 4, 10, 40};
//			MyLinkedList one = new MyLinkedList(a1);
//			
//			int[] a2 = {1, 3, 7};
//			MyLinkedList two = new MyLinkedList(a2);
//			
//			MyLinkedList merged = merge(one, two);
//			merged.print();
//			
//		}

		{
			
			int[] a1 = {2, 4, 10, 40};
			MyLinkedList one = new MyLinkedList(a1);
			
			int[] a2 = {1, 3, 7};
			MyLinkedList two = new MyLinkedList(a2);

			Node mergedRoot = merge(one.root, two.root);
			MyLinkedList merged1 = new MyLinkedList();
			merged1.root = mergedRoot;
			merged1.print();
			
		}
		
		{
			int[] a = {3, 1, 5, 13, 8, 2, 4, 10};
			MyLinkedList list = new MyLinkedList(a);
			
			list.root = sort(list.root);
			
			list.print();
		}
	}
}
