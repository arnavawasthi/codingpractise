/**
 * 
 */
package codingpractise.list;

/**
 * http://www.programcreek.com/2013/01/leetcode-remove-duplicates-from-sorted-list/
 * @author arnavawasthi
 *
 */
public class RemoveDuplicates {
	
	public static Node wrongSolution(Node root){
		System.out.println("Wrong solution::");
		Node pointer = root;
		
		while(pointer != null && pointer.next != null){
			
			if(pointer.data == pointer.next.data){
				pointer.next = pointer.next.next;
			}
			//if pointer moves to next always, it will not remove all duplicates of a number.
			pointer = pointer.next;
		}
		
		return root;
	}
	
	public static Node solution(Node root){
		System.out.println("Correct solution::");
		
		Node pointer = root;
		
		while(pointer != null && pointer.next != null){
			
			if(pointer.data == pointer.next.data){
				pointer.next = pointer.next.next;
			}else{
				//Move pointer to next only if, duplicate of current node is not available.
				pointer = pointer.next;
			}
		}
		
		return root;
	}
	
	
	public static Node solution1(Node root){
		
		Node prev = root;
		Node cur = root.next;
		while(cur != null){
			if(prev.data != cur.data){

				prev.next = cur;
				prev = cur;
			}
			cur = cur.next;
			
		}
		
		return root;
	}
	
	public static void main(String[] args) {
		{
			int[] a = {1,1,1,2,3,4,5,5,5,5,6,7,8,8};
			MyLinkedList list = new MyLinkedList(a);
			wrongSolution(list.root).print();
		}
		
		{
			int[] a = {1,1,1,2,3,4,5,5,5,5,6,7,8,8};
			MyLinkedList list = new MyLinkedList(a);
			solution(list.root).print();
			solution1(list.root).print();
		}
	}

}
