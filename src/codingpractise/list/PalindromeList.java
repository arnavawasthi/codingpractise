/**
 * 
 */
package codingpractise.list;

/**
 * http://www.programcreek.com/2014/07/leetcode-palindrome-linked-list-java/
 * @author arnavawasthi
 *
 */
public class PalindromeList {
	
	public static boolean solution(Node root){
		Node rev = reverse(root);
		while(root != null){
			if(root.data != rev.data) return false;
			root = root.next;
			rev = rev.next;
		}
		
		return true;
	}
	
	public static Node reverse(Node root){
		Node prev = null;
		while(root != null){
			Node temp = root.next;
			root.next = prev;
			prev = root;
			root = temp;
		}
		
		return prev;
	}
	
	public static void main(String[] args) {
		{
			int[] a = {1, 2, 3, 4, 5, 6};
			MyLinkedList list = new MyLinkedList(a);
			list.root.print();
//			reverse(list.root).print();
			System.out.println(solution(list.root));
		}
		
		{
			int[] a = {1, 2, 3, 4, 3, 2, 1};
			MyLinkedList list = new MyLinkedList(a);
			list.root.print();
			System.out.println(solution(list.root));
		}
	}

}
